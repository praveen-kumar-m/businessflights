<?php

//die();
//exit;

session_start();
require 'includes/default-functions.php';

$new = new DefaultFun();

if(isset($_GET['term']) && $_GET['term']){	
	$SuggestionTextFlightcut = mb_substr($_GET['term'],0,3,'utf-8');
	$SuggestionTextFlightcut2 = mb_substr($_GET['term'],0,2,'utf-8');
	$SuggestionTextFlightcut1 = mb_substr($_GET['term'],0,1,'utf-8');
	
	$maxresults = 10;
	$maxdupes = 4;
	
	//$locale = array('en-EN','ru-RU');
	$locale = array('en-EN','de-DE','es-ES','fr-FR','ru-RU');
	$locales = join("','",$locale);
	
	$sql = "SELECT * FROM _citiesML WHERE locale IN ('".$locales."') AND (cityName REGEXP '[[:<:]]".$_GET['term']."' or iataCode REGEXP '[[:<:]]".$_GET['term']."') ORDER BY
		CASE WHEN iataCode = '".$SuggestionTextFlightcut."' THEN 1 
		ELSE
			CASE WHEN LEFT(cityName, 3) = '".$SuggestionTextFlightcut."' THEN 2 
				ELSE 
					CASE WHEN LEFT(cityName, 2) = '".$SuggestionTextFlightcut2."' THEN 3
					ELSE
						CASE WHEN LEFT(cityName, 1) = '".$SuggestionTextFlightcut1."' THEN 4
						ELSE 
							CASE WHEN LEFT(cityName, ".strlen($_GET['term']).") = '".$_GET['term']."' THEN 5
							ELSE 6
							END
						END
					END
				END
			END,
		id, cityName limit 0, 20";
	
	$result = $new->getlistofalldata($sql);
	$C = 0;
	$data = array();
	$duparr = array();
	
	foreach($result as $resultVal){ 
		++$C;
		if ($C > $maxresults) break;
		$duparr[$C] = $resultVal['iataCode'];
		if (count(array_keys($duparr, $resultVal['iataCode'], true)) > $maxdupes) continue;
		if ($resultVal['locale'] != 'en-EN') {
			$data[$C]['label'] = $resultVal['cityName'].', '.$resultVal['countryName'].' ('.$resultVal['iataCode'].')';
		} else {
			$data[$C]['label'] = utf8_encode($resultVal['cityName']).', '.$resultVal['countryName'].' ('.$resultVal['iataCode'].')';
		}
		$data[$C]['flag'] = str_replace('http://','//',str_replace('//discountflights','//www.discountflights',$resultVal['flag']));
		$data[$C]['code'] = $resultVal['iataCode'];
	}
	
	echo json_encode($data,JSON_UNESCAPED_UNICODE);
	exit;
	
}

$action = '';
$action = $_GET['action'];

switch($action) {
	case 'ajaxlist':
		echo $new->ajaxFlightList();
		exit;
	break;
}
?>
