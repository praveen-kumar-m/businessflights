<?php
$apikey = 'as901892827258298792949665487652';
$SessionKey = $_GET['SessionKey'];
$OutboundLegId = $_GET['OutboundLegId'];
$InboundLegId = $_GET['InboundLegId'];

$curlURL = 'http://partners.api.skyscanner.net/apiservices/pricing/v1.0/'.$SessionKey.'/booking/'.$OutboundLegId.';'.$InboundLegId.'/?apikey='.$apikey;

$ch = curl_init();
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_URL,$curlURL);
$curlresult = curl_exec($ch);
$encodedresult = json_decode($curlresult, true);
curl_close($ch);

if (is_array($encodedresult)) {
	$i = 0;
	$response = [];
	foreach ($encodedresult['BookingOptions'][0]['BookingItems'] as $deeplinkinfo) {
		$response['Price'][$i] = $deeplinkinfo['Price'];
		$response['Deeplink'][$i] = $deeplinkinfo['Deeplink'];
		$response['CarrierID'][$i] = $encodedresult['Carriers'][$i]['Id'];
		$response['CarrierCode'][$i] = $encodedresult['Carriers'][$i]['Code'];
		$response['CarrierName'][$i] = $encodedresult['Carriers'][$i]['Name'];
		$response['CarrierImageUrl'][$i] = $encodedresult['Carriers'][$i]['ImageUrl'];
		$i++;
	}
}

header('Content-Type: application/json');
echo json_encode($response);
?>