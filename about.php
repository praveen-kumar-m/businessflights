<?php require('header.php');
?>
<div class="footer">
<div class="iwrapper" id="extracontent">
  <h2>About us</h2>
<p>We provide our clients with some of the most innovative, intuitive technologies the travel industry has to offer.  Lessno.com can help you save big on your next trip.</p>
<p>We search hundreds of major and low cost airlines and multiple travel sites for real-time fares, and then present the results to you in a very simple new page results display. Users typically check travel suppliers to get the lowest price. We work with most of the major and low cost domestic and international airlines. And we compare also multiple online travel sites and portals.</p>
<p>However we do not book flights on our site. All our advertisers’ fares deals include and offer consumers flight options based on price, dates of travel and airlines. We work with our partners to supply travelers with the best travel deals to worldwide destinations.   We help you decide which flights are right for you, and show you where you can book those flights for the best available price.</p>
<p>Lessno.com is powered by DiscountFlights.com and is subsidiary of Asian Airfares Group, LLC.</p>
<p>The company is 100% private and based in San Francisco, CA, US.</p>
<p>With innovative industry leading technology, the most extensive coverage, vast selection of worldwide travel suppliers, and a proven business model that supports customers and suppliers alike, DiscountFlights.com is uniquely positioned to deliver to its users a differentiated and superior travel buying experience.</p>
<p>This Site retains all right, title and interest (including all copyright, trademark, patent, trade secrets, and all other intellectual property rights) in the Site. The Site is protected by copyright, trademark, patent, trade secrets, unfair competition, and other laws of worldwide, through the application of local laws or international treaties. Any unauthorized use, reproduction or modification of this Site may violate such laws.</p>
<p><span style="color: #333333;">For investor relations, media and partnership opportunities please contact:</p>
 <p><span style="color: #333333;"><img src="http://www.corporateflights.com/images/email.png" alt="email" width="32" height="32" />info[at]discountflights[dot]com</span></p>
<p>&nbsp;</p></span>
</div>
</div>
<?php require('footer.php');
?>
