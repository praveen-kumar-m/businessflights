<!DOCTYPE HTML>
<html>

<head>
    <title>Traveler - Flights search results 3</title>


    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta name="keywords" content="discount flights, discount airfares" />
    <meta name="description" content="Find discount flights and cheap airline tickets that meet your budget and travel needs. Search for low cost airlines and airfares using our meta search.">
    <meta name="author" content="Tsoy">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
    <!-- /GOOGLE FONTS -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/font-awesome.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="css/discountflights1.css" type="text/css" />


</head>

<body>


    <div class="global-wrap">
        <header>
    <div class="iwrapper">
      <div class="logo column"><img srcset="images/discount-flights-logo.png 1x, images/discount-flights-logo_2x.png 2x" src="images/images/discount-flights-logo.png" alt="Discount Flights Logo" class="logo"></div>
      <div class="utilitynav column">
        <ul>
          <li><a href="javascript:;">EN</a></li>
          <li><a href="javascript:;">USA</a></li>
          <li><a href="javascript:;">$</a></li>
        </ul>
      </div>
    </div>
  </header>
  
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="index.html">Home</a>
                </li>
                <li><a href="#">United States</a>
                </li>
                <li><a href="#">New York (NY)</a>
                </li>
                <li><a href="#">New York City</a>
                </li>
                <li class="active">New York City Flights</li>
            </ul>
            <div class="mfp-with-anim mfp-hide mfp-dialog mfp-search-dialog" id="search-dialog">
                <h3>Search for Flight</h3>
                <form>
                    <div class="tabbable">
                        <ul class="nav nav-pills nav-sm nav-no-br mb10" id="flightChooseTab">
                            <li class="active"><a href="#flight-search-1" data-toggle="tab">Round Trip</a>
                            </li>
                            <li><a href="#flight-search-2" data-toggle="tab">One Way</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="flight-search-1">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-map-marker input-icon input-icon-highlight"></i>
                                            <label>From</label>
                                            <input class="typeahead form-control" placeholder="City, Airport or U.S. Zip Code" type="text" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-map-marker input-icon input-icon-highlight"></i>
                                            <label>To</label>
                                            <input class="typeahead form-control" placeholder="City, Airport or U.S. Zip Code" type="text" />
                                        </div>
                                    </div>
                                </div>
                                <div class="input-daterange" data-date-format="MM d, D">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-highlight"></i>
                                                <label>Departing</label>
                                                <input class="form-control" name="start" type="text" />
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-highlight"></i>
                                                <label>Returning</label>
                                                <input class="form-control" name="end" type="text" />
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group form-group-lg form-group-select-plus">
                                                <label>Passengers</label>
                                                <div class="btn-group btn-group-select-num" data-toggle="buttons">
                                                    <label class="btn btn-primary active">
                                                        <input type="radio" name="options" />1</label>
                                                    <label class="btn btn-primary">
                                                        <input type="radio" name="options" />2</label>
                                                    <label class="btn btn-primary">
                                                        <input type="radio" name="options" />3</label>
                                                    <label class="btn btn-primary">
                                                        <input type="radio" name="options" />4</label>
                                                    <label class="btn btn-primary">
                                                        <input type="radio" name="options" />5</label>
                                                    <label class="btn btn-primary">
                                                        <input type="radio" name="options" />5+</label>
                                                </div>
                                                <select class="form-control hidden">
                                                    <option>1</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                    <option selected="selected">6</option>
                                                    <option>7</option>
                                                    <option>8</option>
                                                    <option>9</option>
                                                    <option>10</option>
                                                    <option>11</option>
                                                    <option>12</option>
                                                    <option>13</option>
                                                    <option>14</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="flight-search-2">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-map-marker input-icon input-icon-highlight"></i>
                                            <label>From</label>
                                            <input class="typeahead form-control" placeholder="City, Airport or U.S. Zip Code" type="text" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-map-marker input-icon input-icon-highlight"></i>
                                            <label>To</label>
                                            <input class="typeahead form-control" placeholder="City, Airport or U.S. Zip Code" type="text" />
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group form-group-lg form-group-icon-left"><i class="fa fa-calendar input-icon input-icon-hightlight"></i>
                                            <label>Departing</label>
                                            <input class="date-pick form-control" data-date-format="MM d, D" type="text" />
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group form-group-lg form-group-select-plus">
                                            <label>Passengers</label>
                                            <div class="btn-group btn-group-select-num" data-toggle="buttons">
                                                <label class="btn btn-primary active">
                                                    <input type="radio" name="options" />1</label>
                                                <label class="btn btn-primary">
                                                    <input type="radio" name="options" />2</label>
                                                <label class="btn btn-primary">
                                                    <input type="radio" name="options" />3</label>
                                                <label class="btn btn-primary">
                                                    <input type="radio" name="options" />4</label>
                                                <label class="btn btn-primary">
                                                    <input type="radio" name="options" />5</label>
                                                <label class="btn btn-primary">
                                                    <input type="radio" name="options" />5+</label>
                                            </div>
                                            <select class="form-control hidden">
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                                <option selected="selected">6</option>
                                                <option>7</option>
                                                <option>8</option>
                                                <option>9</option>
                                                <option>10</option>
                                                <option>11</option>
                                                <option>12</option>
                                                <option>13</option>
                                                <option>14</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-primary btn-lg" type="submit">Search for Flights</button>
                </form>
            </div>
            <h3 class="booking-title">12 Flights from London to New York on Mar 22 for 1 adult <small><a class="popup-text" href="#search-dialog" data-effect="mfp-zoom-out">Change search</a></small></h3>
            <div class="row">
                <div class="col-md-3">
                    <aside class="booking-filters booking-filters-white">
                        <h3>Filter By:</h3>
                        <ul class="list booking-filters-list">
                            <li>
                                <h5 class="booking-filters-title">Stops <small>Price from</small></h5>
                                <div class="checkbox">
                                    <label>
                                        <input class="i-check" type="checkbox" />Non-stop<span class="pull-right">$215</span>
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input class="i-check" type="checkbox" />1 Stop<span class="pull-right">$154</span>
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input class="i-check" type="checkbox" />2+ Stops<span class="pull-right">$197</span>
                                    </label>
                                </div>
                            </li>
                            <li>
                                <h5 class="booking-filters-title">Price </h5>
                                <input type="text" id="price-slider">
                            </li>
                            <li>
                                <h5 class="booking-filters-title">Flight Class <small>Price from</small></h5>
                                <div class="checkbox">
                                    <label>
                                        <input class="i-check" type="checkbox" />Economy<span class="pull-right">$154</span>
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input class="i-check" type="checkbox" />Business<span class="pull-right">$316</span>
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input class="i-check" type="checkbox" />First<span class="pull-right">$450</span>
                                    </label>
                                </div>
                            </li>
                            <li>
                                <h5 class="booking-filters-title">Airlines <small>Price from</small></h5>
                                <div class="checkbox">
                                    <label>
                                        <input class="i-check" type="checkbox" />Lufthansa<span class="pull-right">$215</span>
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input class="i-check" type="checkbox" />American Airlines<span class="pull-right">$350</span>
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input class="i-check" type="checkbox" />Airfrance<span class="pull-right">$154</span>
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input class="i-check" type="checkbox" />Croatia Airlines<span class="pull-right">$197</span>
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input class="i-check" type="checkbox" />Delta<span class="pull-right">$264</span>
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input class="i-check" type="checkbox" />Air Canada<span class="pull-right">$445</span>
                                    </label>
                                </div>
                            </li>
                            <li>
                                <h5 class="booking-filters-title">Departure Time</h5>
                                <div class="checkbox">
                                    <label>
                                        <input class="i-check" type="checkbox" />Morning (5:00a - 11:59a)</label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input class="i-check" type="checkbox" />Afternoon (12:00p - 5:59p)</label>
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input class="i-check" type="checkbox" />Evening (6:00p - 11:59p)</label>
                                </div>
                            </li>
                        </ul>
                    </aside>
                </div>
				
                <div class="col-md-7" style="padding:0">
                    <div class="nav-drop booking-sort">
                        <h5 class="booking-sort-title"><a href="#">Sort: Sort: Price (low to high)<i class="fa fa-angle-down"></i><i class="fa fa-angle-up"></i></a></h5>
                        <ul class="nav-drop-menu">
                            <li><a href="#">Price (high to low)</a>
                            </li>
                            <li><a href="#">Duration</a>
                            </li>
                            <li><a href="#">Stops</a>
                            </li>
                            <li><a href="#">Arrival</a>
                            </li>
                            <li><a href="#">Departure</a>
                            </li>
                        </ul>
                    </div>
                    <ul class="booking-list">
                        <li>
                            <div class="booking-item-container">
                                <div class="booking-item">
                                    <div class="row border">
                                        <div class="col-md-2">
                                            <div class="booking-item-airline-logo">
                                                <img src="img/lufthansa.jpg" alt="Image Alternative text" title="Image Title" />
                                                <p>Lufthansa</p>
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="booking-item-flight-details">
                                                <div class="booking-item-departure"><!-- <i class="fa fa-plane"></i> -->
                                                    <h2>JKF</h2>
                                                    <p class="booking-item-date">Sun, Mar 22</p>
                                                    <p class="booking-item-destination">London, England,<br> United Kingdom (LHR)</p>
                                                </div>
												<span><img src="images/right-arrow (4).png"></span>
                                                <div class="booking-item-arrival"><!-- <i class="fa fa-plane fa-flip-vertical"></i> -->
                                                    <h2>LHR</h2>
                                                    <p class="booking-item-date">Sat, Mar 23</p>
                                                    <p class="booking-item-destination">New York, NY,<br> United States (JFK)</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <h5>22h 50m</h5>
                                            <p>1 stop</p>
											<a href="#">[+] More Details</a>
                                        </div>
                                  <div class="booking-item-details">
								    <div class="row depart">
									   <div class="col-md-6">
									      <h5><b>DEPART</b> fri, 24 mar 2017</h5>
										  
									   </div>
									   <div class="col-md-6">
											<span>[-]Less Details</span>
									   </div>
									</div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <ul class="list">
                                                <li><b>Aeroflot</b></li>
                                                <li><b>SU2575</b></li>
											</ul>	
                                        </div>
										<div class="col-md-5">
                                            <ul class="list">
                                                <li><b>10:25&nbsp;&nbsp;&nbsp;</b>  JKF New York John Kennedy</li>
												<li><b>10:25&nbsp;&nbsp;&nbsp; </b> AUH Sbu dhabi international</li>
											</ul>	
                                        </div>
										<div class="col-md-4 text-right">
                                            <ul class="list list-inline">
                                                <li><img src="images/FlightList_03.png" alt="img"></li>
											    <li>12h 45</li>
											</ul>	
                                        </div>
                                    </div>
									<div class="row connect-airport">
									   <div class="col-md-6">
									      <p>Connect in airport</p>
									   </div>
									   <div class="col-md-6 text-right">
									      <img src="images/bullets_07.png" alt="img">&nbsp;&nbsp; <span>1h 30</span>
									   </div>
                                    </div>	
									<div class="row">
                                        <div class="col-md-3">
                                            <ul class="list">
                                                <li><b>Aeroflot</b></li>
                                                <li><b>SU2575</b></li>
											</ul>	
                                        </div>
										<div class="col-md-5">
                                            <ul class="list">
                                                <li><b>10:25&nbsp;&nbsp;&nbsp; </b> JKF New York John Kennedy</li>
												<li><b>10:25&nbsp;&nbsp;&nbsp;</b>  AUH Sbu dhabi international</li>
											</ul>	
                                        </div>
										<div class="col-md-4 text-right">
                                            <ul class="list list-inline">
                                                <li><img src="images/FlightList_03.png" alt="img"></li>
											    <li>12h 45</li>
											</ul>	
                                        </div>
                                    </div>
									 <div class="row arrive">
                                        <div class="col-md-4">
                                           
                                        </div>
										<div class="col-md-4">
                                            <ul class="list">
                                                <li>Arrives Mon, 27 mar 2017</li>
											</ul>	
                                        </div>
										<div class="col-md-4 text-left">
                                            <ul class="list list-inline">
											    <li>Journey Duration: 42h 05</li>
											</ul>	
                                        </div>
                                    </div>
                                </div>
                                    </div>
									 <div class="row airlines">
                                        <div class="col-md-2">
                                            <div class="booking-item-airline-logo">
                                                <img src="img/american-airlines.jpg" alt="Image Alternative text" title="Image Title" />
                                                <p>American Airlines</p>
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="booking-item-flight-details">
                                                <div class="booking-item-departure"><!-- <i class="fa fa-plane"></i> -->
                                                    <h2>LHR</h2>
                                                    <p class="booking-item-date">Sun, Mar 22</p>
                                                    <p class="booking-item-destination">London, England,<br> United Kingdom (LHR)</p>
                                                </div>
												<span><img src="images/right-arrow (4).png"></span>
                                                <div class="booking-item-arrival"><!-- <i class="fa fa-plane fa-flip-vertical"></i> -->
                                                    <h2>JKF</h2>
                                                    <p class="booking-item-date">Sat, Mar 23</p>
                                                    <p class="booking-item-destination">New York, NY,<br> United States (JFK)</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <h5>22h 50m</h5>
                                            <p>1 stop</p>
											<a href="#">[+] More Details</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
						</ul>
					</div>	
					<div class="col-md-2 book-price"><span class="booking-item-price">$187</span><span>/person</span>
                        <p class="booking-item-flight-class">Class: Economy</p><a class="btn btn-primary" href="#">Select</a>
                    </div>	
					
					
					 <div class="col-md-7" style="padding:0">
                    <div class="nav-drop booking-sort">
                    </div>
                    <ul class="booking-list">
                        <li>
                            <div class="booking-item-container">
                                <div class="booking-item booking-item1">
                                    <div class="row border">
                                        <div class="col-md-2">
                                            <div class="booking-item-airline-logo">
                                                <img src="img/lufthansa.jpg" alt="Image Alternative text" title="Image Title" />
                                                <p>Lufthansa</p>
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="booking-item-flight-details">
                                                <div class="booking-item-departure"><!-- <i class="fa fa-plane"></i> -->
                                                    <h2>JKF</h2>
                                                    <p class="booking-item-date">Sun, Mar 22</p>
                                                    <p class="booking-item-destination">London, England,<br> United Kingdom (LHR)</p>
                                                </div>
												<span><img src="images/right-arrow (4).png"></span>
                                                <div class="booking-item-arrival"><!-- <i class="fa fa-plane fa-flip-vertical"></i> -->
                                                    <h2>LHR</h2>
                                                    <p class="booking-item-date">Sat, Mar 23</p>
                                                    <p class="booking-item-destination">New York, NY,<br> United States (JFK)</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <h5>22h 50m</h5>
                                            <p>1 stop</p>
											<a href="#">[+] More Details</a>
                                        </div>
                                    <div class="booking-item-details">
									<div class="row depart">
									   <div class="col-md-6">
									       <h5><b>DEPART</b> fri, 24 mar 2017</h5>
									   </div>
									   <div class="col-md-6">
									       <span>[-]Less Details</span>
									   </div>
									</div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <ul class="list">
                                                <li><b>Aeroflot</b></li>
                                                <li><b>SU2575</b></li>
											</ul>
                                        </div>
										<div class="col-md-5">
                                            <ul class="list">
                                                <li><b>10:25&nbsp;&nbsp;&nbsp;</b>  JKF New York John Kennedy</li>
												<li><b>10:25&nbsp;&nbsp;&nbsp;</b>  AUH Sbu dhabi international</li>
											</ul>	
                                        </div>
										<div class="col-md-4 text-right">
                                            <ul class="list list-inline text-right">
                                                <li><img src="images/FlightList_03.png" alt="img"></li>
											    <li>12h 45</li>
											</ul>	
                                        </div>
                                    </div>
									<div class="row connect-airport">
									   <div class="col-md-6">
									      <p>Connect in airport</p>
									   </div>
									   <div class="col-md-6 text-right">
									      <img src="images/bullets_07.png" alt="img">&nbsp;&nbsp; <span>1h 30</span>
									   </div>
                                    </div>	
									<div class="row">
                                        <div class="col-md-3">
                                            <ul class="list">
                                                <li><b>Aeroflot</b></li>
                                                <li><b>SU2575</b></li>
											</ul>	
                                        </div>
										<div class="col-md-5">
                                            <ul class="list">
                                                <li><b>10:25&nbsp;&nbsp;&nbsp;</b>  JKF New York John Kennedy</li>
												<li><b>10:25&nbsp;&nbsp;&nbsp;</b>  AUH Sbu dhabi international</li>
											</ul>	
                                        </div>
										<div class="col-md-4 text-left">
                                            <ul class="list list-inline">
                                                <li><img src="images/FlightList_03.png" alt="img"></li>
											    <li>12h 45</li>
											</ul>	
                                        </div>
                                    </div>
									 <div class="row arrive">
                                        <div class="col-md-4">
                                           
                                        </div>
										<div class="col-md-4">
                                            <ul class="list">
                                                <li>Arrives Mon, 27 mar 2017</li>
											</ul>	
                                        </div>
										<div class="col-md-4 text-left">
                                            <ul class="list list-inline">
											    <li>Journey Duration: 42h 05</li>
											</ul>	
                                        </div>
                                    </div>
                                </div>
                                    </div>
									 <div class="row airlines">
                                        <div class="col-md-2">
                                            <div class="booking-item-airline-logo">
                                                <img src="img/american-airlines.jpg" alt="Image Alternative text" title="Image Title" />
                                                <p>American Airlines</p>
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="booking-item-flight-details">
                                                <div class="booking-item-departure"><!-- <i class="fa fa-plane"></i> -->
                                                    <h2>LHR</h2>
                                                    <p class="booking-item-date">Sun, Mar 22</p>
                                                    <p class="booking-item-destination">London, England,<br> United Kingdom (LHR)</p>
                                                </div>
												<span><img src="images/right-arrow (4).png"></span>
                                                <div class="booking-item-arrival"><!-- <i class="fa fa-plane fa-flip-vertical"></i> -->
                                                    <h2>JKF</h2>
                                                    <p class="booking-item-date">Sat, Mar 23</p>
                                                    <p class="booking-item-destination">New York, NY,<br> United States (JFK)</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <h5>22h 50m</h5>
                                            <p>1 stop</p>
											<a href="#">[+] More Details</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
					</ul>	
					</div>	
					<div class="col-md-2 book-price1"><span class="booking-item-price">$187</span><span>/person</span>
                        <p class="booking-item-flight-class">Class: Economy</p><a class="btn btn-primary" href="#">Select</a>
                     </div>	
					</div>	
                    </div>		
					</div>		
            </div>
        
        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/slimmenu.js"></script>
        <script src="js/bootstrap-datepicker.js"></script>
        <script src="js/bootstrap-timepicker.js"></script>
        <script src="js/nicescroll.js"></script>
        <script src="js/dropit.js"></script>
        <script src="js/ionrangeslider.js"></script>
        <script src="js/icheck.js"></script>
        <script src="js/fotorama.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script src="js/typeahead.js"></script>
        <script src="js/card-payment.js"></script>
        <script src="js/magnific.js"></script>
        <script src="js/owl-carousel.js"></script>
        <script src="js/fitvids.js"></script>
        <script src="js/tweet.js"></script>
        <script src="js/countdown.js"></script>
        <script src="js/gridrotator.js"></script>
        <script src="js/custom.js"></script>
    </div>
</body>

</html>


