<?php

/*define('DB_NAME', 'discountflights_plform');
define('DB_HOST', 'localhost');
define('DB_USER', 'root');
define('DB_PASSWORD', '');*/

define('DB_NAME', 'corpori3_discountflight');
define('DB_HOST', 'localhost');
define('DB_USER', 'corpori3_corpora');
define('DB_PASSWORD', 'gti=Eqh+O}F,');

require 'flight-db-functions.php';

class DefaultFun {
	
	use FlightDbFunctions;
	
	public $db;
	
	function __construct() {
		
		//$this->db = new PDO('mysql:dbname='.DB_NAME.';host='.DB_HOST.'',DB_USER, DB_PASSWORD);
		$this->db = new PDO('mysql:dbname='.DB_NAME.';host='.DB_HOST.'',DB_USER, DB_PASSWORD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
		
	}
	
	public function getlistofalldata($sql){
		
		$valueArray = array();
		$pstmt = $this->db->prepare($sql);
		$pstmt->execute($valueArray);
		$pstmt->setFetchMode(PDO::FETCH_ASSOC);
		$rows = $pstmt->fetchAll();
		return $rows;

	}
	
	function get_client_ip() {
		$ipaddress = '';
		if (isset($_SERVER['HTTP_CLIENT_IP']))
			$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
		else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
			$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
		else if(isset($_SERVER['HTTP_X_FORWARDED']))
			$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
		else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
			$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
		else if(isset($_SERVER['HTTP_FORWARDED']))
			$ipaddress = $_SERVER['HTTP_FORWARDED'];
		else if(isset($_SERVER['REMOTE_ADDR']))
			$ipaddress = $_SERVER['REMOTE_ADDR'];
		else
			$ipaddress = 'UNKNOWN';
		return $ipaddress;
	}
	
	function DeleteCacheFiles() {
		$path = __DIR__.'/../response/';
		if ($handle = opendir($path)) {
		
			while (false !== ($file = readdir($handle))) { 
				$filelastmodified = filemtime($path . $file);
				//24 hours in a day * 3600 seconds per hour
				if((time() - $filelastmodified) > 24*3600)
				{
				   if(file_exists($path . $file)):
				        unlink($path . $file);
				   endif;
				}
			}
			
			closedir($handle); 
		}
	}
}
