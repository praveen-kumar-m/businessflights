<?php

require 'api/flight-api-functions.php';

trait FlightDbFunctions {
	
	use FlightApiFuntions;
	
	public function ajaxFlightList(){
		$html = '';
		//s$this->DeleteCacheFiles();
		ob_start();
		require ''.__DIR__.'/../ajaxlist.php';
		$html = ob_get_contents();
		ob_end_clean();
		return $html;
	}

}
