<?php
//echo 'https://'.$_SERVER["HTTP_HOST"].$_SERVER['REQUEST_URI'];
if (!isset($_SERVER['HTTPS'])) header('Location: https://'.$_SERVER["HTTP_HOST"].$_SERVER['REQUEST_URI']);
require 'includes/default-functions.php';
define('HOMEURL', 'https://127.0.0.1/businessflights/');
?>
<!DOCTYPE html>
<html>
<head>

<title>Business & Corporate Flights for Business Travel</title>
<?php
if($Is_Page == 'PageIdlist'){
echo '<meta name="description" content=" Find discount flights from: '.$from.' to: '.$to.'  and save money on airfares to every destination in the world at DiscountFlights.com ">';
}else{
  echo '<meta name="description" content="Cheap Airline Tickets - Searching for Cheap Flights?‎ Find the cheapest flights by comparing hundreds of airlines and travel sites in just one search. ">';

}
?>
<!-- favcion -->
<link rel="apple-touch-icon" sizes="180x180" href="images/icon/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="images/icon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="192x192" href="images/icon/android-chrome-192x192.png">
<link rel="icon" type="image/png" sizes="16x16" href="images/icon/favicon-16x16.png">
<link rel="manifest" href="images/icon/site.webmanifest">
<link rel="mask-icon" href="images/icon/safari-pinned-tab.svg" color="#760bc2">
<meta name="msapplication-TileColor" content="#2d89ef">
<meta name="theme-color" content="#ffffff">
<!-- /favicon --> 
<meta name="description" content="Looking for business flights booking? Get special negotiated flights & airfares for business travel by booking direct.">
<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<meta name="keywords" content="discount flights, discount airfares" />
<meta name="author" content="cheap-airlines.com">
<meta name="msvalidate.01" content="742A39FA3BCA62E9AA09A1349046129E" />
<meta name="viewport"  content="width=device-width, initial-scale=1.0, user-scalable=no">
<!-- GOOGLE FONTS -->
<link href='//fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
<!-- /GOOGLE FONTS -->
<link rel="stylesheet" type="text/css" href="css/rs_searchbox.css"> 
<link rel="stylesheet" type="text/css" href="css/loader.css">
<link rel="stylesheet" type="text/css" href="css/rs_calendar_style.css">
 
<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
<?php
if($Is_Page == 'PageIdlist'){
echo '<link rel="stylesheet" href="css/bootstrap.css">';
}
else{
echo '<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" />';
}
?>
<link rel="stylesheet" href="css/bootstrap-datepicker.min.css" type="text/css" />
<link rel="stylesheet" href="css/reset.css" type="text/css" />
<link rel="stylesheet" href="css/font-awesome.css?v=<?=rand()?>">
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
 
  ga('create', 'UA-11871909-29', 'auto');
  ga('send', 'pageview');
 
</script>

<?php
if($Is_Page ==  'PageIdlist'):
echo '<link rel="stylesheet" href="css/styles.css" type="text/css" />';
endif;
?>
<link rel="stylesheet" href="css/discountflights.css" type="text/css" />
<style>
#air_one_way input {
  height:40px;
  width:100%;
}
#air_one_way .row {
  margin-bottom:10px;
}
.ui-autocomplete {
  overflow-y: auto;
  position:absolute;
}
.ui-helper-hidden-accessible {
    border: 0;
    clip: rect(0 0 0 0);
    height: 1px;
    margin: -1px;
    overflow: hidden;
    padding: 0;
    position: absolute;
    width: 1px;
}
.ui-autocomplete-loading { background:url('images/loader.gif') no-repeat right center }
</style>
<link rel="stylesheet" type="text/css" href="css/restyle.css">
<link rel="stylesheet" href="css/header.css" type="text/css" />
<link rel="stylesheet" href="css/usp.css" type="text/css" />
</head>
<body  class="<?php if($Is_Page == 'PageIdlist'){ echo 'PageIdlist'; }?>">
  <header class="relative primary-menu">
      <div class="clearfix primary-menu-wrap">
          <section class="flight-logo-block left relative">
              <a href="<?php echo HOMEURL; ?>" class="clearfix">
                  <div class="logo">
                      <img src="images/hotels_logo.svg" />
                  </div>
                  <div class="mobile-logo">
                      <img src="images/hotels_logo_symbol.svg" />
                  </div>
              </a>
          </section>
          <section class="hotel-logo-block right">
              <a href="https://businesshotels.com/" target="_blank" class="clearfix ">
                  <div class="logo ">
                      <img src="images/flights_logo.svg" />
                  </div>
                  <div class="flight-mobile-logo mobile-logo ">
                      <img src="images/flights_logo_symbol.svg " />
                  </div>
              </a>
          </section>
      </div>
      <nav class="index-menu">
          <div class="mobile-menu clearfix">
              <div class="left ">
                  <h3>BusinessFlights</h3>
              </div>
              <div class="right ">
                <div class="mobile-menu-toggle">
                              <span></span>
                              <span></span>
                              <span></span>
                              <span></span>
                          </div>
              </div>
          </div>
          <ul class="index-menu-wrapper">
              <li>
                  <a href="about.php">About</a>
              </li>
              <li>
                  <a href="terms.php">Terms</a>
              </li>
              <li>
                  <a href="privacy.php">Privacy</a>
              </li>
              <li class="currency-container-wrap">
                  <a href="javascript:void(0);" class="currency-header"><img class="flag-block" src="images/usa.png"/></a>
                  <ul class="currency-container">
                                <li class="active">
                                  <a href="javascript:void(0);">
                                    <span class="symbol"><img src="flags/US.png" /></span>
                                    <span>United States</span>
                                  </a>
                                </li>
                                <li>
                                  <a href="javascript:void(0);">
                                    <span class="symbol"><img src="flags/IN.png" /></span>
                                    <span>India</span>
                                  </a>
                                </li>
                                <li>
                                  <a href="javascript:void(0);">
                                    <span class="symbol"><img src="flags/AU.png" /></span>
                                    <span>Australia</span>
                                  </a>
                                </li>
                                <li>
                                  <a href="javascript:void(0);">
                                    <span class="symbol"><img src="flags/CA.png" /></span>
                                    <span>Canada</span>
                                  </a>
                                </li>
                                <li>
                                  <a href="javascript:void(0);">
                                    <span class="symbol"><img src="flags/GB.png" /></span>
                                    <span>United Kingdom</span>
                                  </a>
                                </li>
                                <li>
                                  <a href="javascript:void(0);">
                                    <span class="symbol"><img src="flags/RU.png" /></span>
                                    <span>Russia</span>
                                  </a>
                                </li>
                            </ul>
              </li>
          </ul>
      </nav>
  </header>