 
<html>

<head>
    <!-- Meta Data -->
    <meta charset="utf-8" />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Travelling for business? We offer world's best business hotels and direct booking for business travelers along with great corporate hotel rates.">
    <!-- Title -->
    <title>Business Hotels: Business & Corporate Hotels for Business Travel</title>
    <!-- favcion -->
    <link rel="apple-touch-icon" sizes="180x180" href="img/icon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/icon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="192x192" href="img/icon/android-chrome-192x192.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/icon/favicon-16x16.png">
    <link rel="manifest" href="img/icon/site.webmanifest">
    <link rel="mask-icon" href="img/icon/safari-pinned-tab.svg" color="#760bc2">
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="theme-color" content="#ffffff">
    <!-- Animate on Scroll -->
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <!-- Custom CSS -->
    <link rel="stylesheet" type="text/css" href="css/list-style.css">
    <link rel="stylesheet" type="text/css" href="css/list-responsive.css">
    <!-- Jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <!-- Custom JS -->
    <script src="js/script.js"></script>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:100,300,400,500,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
    <!-- Auto Complete -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!-- Datepicker -->
    <script type="text/javascript" src="js/datepicker/moment.min.js"></script>
    <script type="text/javascript" src="js/datepicker/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="js/datepicker/daterangepicker.css" />
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-11871909-37"></script>
</head>

<body>
    <nav class="primary-menu">
        <header class="relative">
            <div class="container clearfix">
                <section class="flight-logo-block left relative">
                    <a href="<?php echo HOMEURL; ?>" class="clearfix">
                        <div class="logo">
                            <img src="img/flights_logo.svg" />
                        </div>
                        <div class="mobile-logo">
                            <img src="img/hotels_logo_symbol.svg" />
                        </div>
                    </a>
                </section>
                <section class="hotel-logo-block right">
                    <a href="https://www.busineshotels.com/" target="_blank" class="clearfix ">
                        <div class="logo ">
                            <img src="img/hotels_logo.svg " />
                        </div>
                        <div class="flight-mobile-logo mobile-logo ">
                            <img src="img/flights_logo_symbol.svg " />
                        </div>
                    </a>
                </section>
            </div>
            <img src="img/menu_bottom_bar.png " class="menu-bottom-bar " />
        </header>
    </nav>
