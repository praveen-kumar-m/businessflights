<?php
$Is_Page = 'indexlist';
 require('header.php');
?>
<script>
   var _ctq = _ctq || [];
  _ctq.push(['newCompareToStrip', 'aflight']);
  _ctq.push(['aflight.setPublisherID', 118]);
  _ctq.push(['aflight.setFormID', 'rs_air_forms']);
  _ctq.push(['aflight.setDepartureDateID', 'rs_chk_in']);
  _ctq.push(['aflight.setReturnDateID', 'rs_chk_out']);
  _ctq.push(['aflight.setNumTravelersID', 'rs_adults']);
  _ctq.push(['aflight.setOneWayID', 'one-way']);
  _ctq.push(['aflight.setAirportFromID', 'rs_from']);
  _ctq.push(['aflight.setAirportToID', 'rs_to']);              
  _ctq.push(['aflight.setAnchorID', 'ct-anchor']);
  _ctq.push(['aflight.setReferralURL', 'http://<?=$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"]?>']);
  _ctq.push(['aflight.optMaxChecked', '3']);                                     
  _ctq.push(['aflight.optMaxAdvertisers', '5']); 
  _ctq.push(['aflight.optCascadeWindows', 'true']);
  _ctq.push(['aflight.optPopUnder', 'true']);
  _ctq.push(['aflight.draw']);
	
  
	(function() {
    var ct = document.createElement('script');
    ct.type = 'text/javascript';
    ct.async = true;
    ct.src = ('https:' == document.location.protocol ? 'https://' : 'http://')
           + 'static.clicktripz.com/scripts/js/ct.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(ct, s);
		
		var head = document.head, link = document.createElement('link');
		link.type = 'text/css';
  	link.rel = 'stylesheet';
  	link.href = 'http://www.discountflights.com/css/ct.css?v='+Math.random();
		head.appendChild(link);
		
	})();
  
</script>
<div class="containertp">
  <div class="searchWrapper">
    <div id="rs_multi" class="rs_searchbox">
      <form name="air" class="air rs_air_form" id = "rs_air_forms" action="dynamic-flight-result.php" method="GET">
        <div class="rs_air_options"> <span class="rs_air_option rs_air_highlight">
          <input type="radio" class="round-trip" name="air-radio" id="round-trip" checked>
          <label for="round-trip">Round Trip</label>
          </span> <span class="rs_air_option">
          <input type="radio" class="one-way" name="air-radio" id="one-way">
          <label for="one-way">One Way</label>
          </span>
          <!--<span class="rs_air_option"><input type="radio" class="multi-city" name="air-radio" id="multi-city"><label for="multi-city">Multi City</label></span>-->
        </div>
        <div class="clear"></div>
        <div id="air_round_trip">
          <div class="row">
            <div class="col-sm-6 input-daterange">
              <input name="from" class="from autosuggest rs_from" id = "rs_from" placeholder = 'From (ie. NYC)' onClick='$(this).val("");' autocomplete="off">
              <input type="hidden" name="oricode" id = "rs_o_code" value="">
              <div id="suggesstion-box"></div>
            </div>
            <div class="col-sm-6">
              <input name="to" class="to autosuggest rs_to" placeholder ='To (ie. LON)' id = "rs_to" onClick='$(this).val("");' autocomplete="off">
              <input type="hidden" name="descode" id ="rs_d_code" value="">
            </div>
          </div>
          <div class="row">
            <div class="col-sm-4 DepartBlk">
              <input name="outdate" class="rs_chk_in tb-input" id="rs_chk_in" placeholder ="Depart" readonly>
            </div>
            <div class="col-sm-4 DateOutBlock">
              <input name="indate" class="rs_chk_out tb-input" id = "rs_chk_out" placeholder = "Return" readonly>
            </div>
            <div class="col-sm-4 travelersBlk">
            	<div class="travelers">
                	<span>Travelers 1, Economy</span>
                    <img src="images/down-arrow.png">
                </div>
            </div>
          </div>
          <div class="row PaxBlock" style="display:none;">
            <div class="col-md-3 col-sm-6">
              <label>Adults (16+) </label>
              <select name="adults" class="rs_adults_input pax paxcal" id="rs_adults">
                <option selected="" value="1">1 Adult</option>
                <option value="2">2 Adults</option>
                <option value="3">3 Adults</option>
                <option value="4">4 Adults</option>
                <option value="5">5 Adults</option>
                <option value="6">6 Adults</option>
              </select>
            </div>
            <div class="col-md-3 col-sm-6">
              <label>Children (2-16)</label>
              <select name="children" class="rs_child_input pax paxcal">
                <option value="0">0 Children</option>
                <option value="1">1 Children</option>
                <option value="2">2 Children</option>
                <option value="3">3 Children</option>
                <option value="4">4 Children</option>
                <option value="5">5 Children</option>
              </select>
            </div>
            <div class="col-md-3 col-sm-6">
              <label>Infants (0-2)</label>
              <select name="infants" class="rs_infant_input pax paxcal">
                <option value="0">0 Infants</option>
                <option value="1">1 Infants</option>
                <option value="2">2 Infants</option>
                <option value="3">3 Infants</option>
                <option value="4">4 Infants</option>
                <option value="5">5 Infants</option>
              </select>
            </div>
            <div class="col-md-3 col-sm-6">
              <label>Cabin Class</label>
              <select name="cabinclass" class="rs_select_skin_activated rs_select_box rs_cabin_box paxcal">
                <option selected="" value="Economy">Economy</option>
                <option value="PremiumEconomy">Premium Economy</option>
                <option value="Business">Business</option>
                <option value="First">First</option>
              </select>
            </div>
          </div>
          <div class="rs_button_row newbtn">
             <input type="hidden" name="rs_language" id = "rs_language"  value="en-US">
            <input type="hidden" name="rs_currency"  id = "rs_currency" value="USD">
            <input type="button" class="rs_search" value="Find">
            <i class="fa fa-search plf-search" aria-hidden="true"  ></i>
            <i class="fa-li fa plf-load fa-spinner fa-spin" style="display:none"></i>
            </input>
          </div>
          <div class="clear"></div>
        </div>
        <input type="hidden" name="trip" id="trip" value="2">
      </form>
    </div>
  </div>
  <div class="anchorwrapper">
		<h3 class="hdr">Compare prices with:</h3>
  	<div id="ct-anchor"></div>
  </div>
</div>
<?php require('footer.php');
?>


