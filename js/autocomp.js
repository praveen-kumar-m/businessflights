"use strict";

var checkorigin;
var itemname;

$(function () {
	$(document).on('blur', '#rs_from, #rs_to', function () {
		checkorigin = $(this).val();
		itemname = $(this).attr('name');
		if (checkorigin.indexOf("(") > -1 && checkorigin.indexOf(")") > -1) {
			if(checkorigin.match(/\((.*?)\)/)[1].length !== 3) {
				getiatamatch(checkorigin, itemname);
			}
		} else {
			getiatamatch(checkorigin, itemname);
		}
	});	
	
	var movedWhildAutocomplete = false;
	$(document)
		.on('touchstart', '.ui-autocomplete li.ui-menu-item', function(){
			$(this).trigger('mouseenter');
			movedWhildAutocomplete = false;
		})
		.on('touchmove', '.ui-autocomplete li.ui-menu-item', function(){
			movedWhildAutocomplete = true;
		})
		.on('touchend', '.ui-autocomplete li.ui-menu-item', function(evt){
			if (!movedWhildAutocomplete) {
				var $el = $(this);
				if ($el.is(':visible') && $el.hasClass('ui-state-focus')) {
						evt.preventDefault();
						$el.trigger('click');
				}
			}
		movedWhildAutocomplete = false;
	});

	$(document).on('click','.loadlinks',function(){
		var obid = $(this).attr('data-obid');
		var ibid = $(this).attr('data-ibid');
		var sid = $(this).attr('data-sid');
		loadlinks(obid,ibid,sid);
	});

	$(document).on('click', '.booking-item', function() {
		if($(this).parents('.flightrow').find('.loadlinks').length) {
			var obid = $(this).parents('.flightrow').find('.loadlinks').attr('data-obid');
			var ibid = $(this).parents('.flightrow').find('.loadlinks').attr('data-ibid');
			var sid = $(this).parents('.flightrow').find('.loadlinks').attr('data-sid');
			loadlinks(obid,ibid,sid);
		}
	});
	
	$(document).on('click','.bookindividual, a[data-test="booksingle"]',function(e){
		e.stopPropagation();
	});
});

function getiatamatch(checkorigin, itemname) {
	$.ajax({
  	url: '/ajax.php?term='+checkorigin,
  	context: document.body
	}).done(function(result) {
		var jsonresult = JSON.parse(result);
		$('input[name="'+itemname+'"]').val(jsonresult[1].label);	
		switch(itemname) {
			case 'from':
				$('input[name="oricode"]').val(jsonresult[1].code);	
				break;
			case 'to':
				$('input[name="descode"]').val(jsonresult[1].code);	
				break;
		}
	});
}

function loadlinks(obid,ibid,sid) {
	var targetelement = 'fetchingadls-'+obid+'-'+ibid;
	if (typeof obid !== undefined && typeof ibid !== undefined && typeof sid !== undefined && $('.'+targetelement).hasClass('initial')) {
		$.ajax({
			url: '/getmultideeplinks.php?SessionKey='+sid+'6&OutboundLegId='+obid+'&InboundLegId='+ibid,
			context: document.body
		}).done(function(result) {
			renderseparatedls(result,targetelement);
		});
	}
}

function renderseparatedls(response,targetelement) {
	if (typeof response !== undefined && typeof targetelement !== undefined) {
		$('.'+targetelement).html('');
		var i = 0;
		for (i=0; i < response.Deeplink.length; i++) {
			var flightappendix = 'Outbound';
			if (i > 0) flightappendix = 'Inbound';
			$('.'+targetelement).append('<li style="margin: 10px 0;"><span class="agentimage"><img style="width:auto; height:55px;" src="'+response.CarrierImageUrl[i]+'" alt="'+response.CarrierName[i]+'"></span><span class="agentname">'+response.CarrierName[i]+'</span><span class="agentprice">AU$ '+response.Price[i]+'</span><a target="_blank" class="btn bookbt btn-primary bookindividual" href="'+response.Deeplink[i]+'" style="width: auto;">Book '+flightappendix+' Flight</a></li>');	
		}
		$('.'+targetelement).removeClass('initial');
	}
}
