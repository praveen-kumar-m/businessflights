"use strict";

var checkorigin;
var itemname;

$(function () {
	$(document).on('blur', '#rs_from, #rs_to', function () {
		checkorigin = $(this).val();
		itemname = $(this).attr('name');
		if (checkorigin.indexOf("(") > -1 && checkorigin.indexOf(")") > -1) {
			if(checkorigin.match(/\((.*?)\)/)[1].length !== 3) {
				getiatamatch(checkorigin, itemname);
			}
		} else {
			getiatamatch(checkorigin, itemname);
		}
	});	
	
	$(document).on('touchend','.ui-autocomplete .ui-menu-item a',function(){
		$(this).trigger('click');
	});
});

function getiatamatch(checkorigin, itemname) {
	$.ajax({
  	url: '/ajax.php?term='+checkorigin,
  	context: document.body
	}).done(function(result) {
		var jsonresult = JSON.parse(result);
		$('input[name="'+itemname+'"]').val(jsonresult[1].label);	
		switch(itemname) {
			case 'from':
				$('input[name="oricode"]').val(jsonresult[1].code);	
				break;
			case 'to':
				$('input[name="descode"]').val(jsonresult[1].code);	
				break;
		}
	});
}