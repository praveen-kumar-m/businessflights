$(function () {
  $('input[name="air-radio"]').change(function () {
    $('.rs_air_option').toggleClass('rs_air_highlight');
    if ($('#round-trip').is(':checked')) {
     // $('#air_round_trip').show();
      //$('#air_one_way').hide();
	  $('.DateOutBlock').show();
	  $('#trip').val(2);
	  $('.travelersBlk').removeClass('col-sm-6');
	  $('.travelersBlk').addClass('col-sm-4');
	  $('.DepartBlk').removeClass('col-sm-6');
	  $('.DepartBlk').addClass('col-sm-4');
    } else {
     // $('#air_round_trip').hide();
      //$('#air_one_way').show();
	  $('#trip').val(1);
	  $('.travelersBlk').removeClass('col-sm-4');
	  $('.travelersBlk').addClass('col-sm-6');
	  $('.DepartBlk').removeClass('col-sm-4');
	  $('.DepartBlk').addClass('col-sm-6');
	  $('.DateOutBlock').hide();
    }
  });
  
  var toggleId = true;
  
  $(document).on('click', '.travelers', function() {
	  if(toggleId) {
		  $('.travelers img').attr('src','images/up-arrow.png');
	  } else {
		  $('.travelers img').attr('src','images/down-arrow.png');
	  }
	  toggleId = !toggleId;
	  $('.PaxBlock').toggle(500);
  });
	
	$('footer').css('margin-top',$(window).height()+'px');
});

function travellerCal() {
	var adults	= 0;
	var child 	= 0;
	var infant	= 0;
	var total	= 0;
	
	if($('.rs_adults_input :selected').val() > 0) {
		adults = $('.rs_adults_input :selected').val();
	}
	
	if($('.rs_child_input :selected').val() > 0) {
		child = $('.rs_child_input :selected').val();
	}
	
	if($('.rs_infant_input :selected').val() > 0) {
		infant = $('.rs_infant_input :selected').val();
	}
	
	total = parseInt(adults) + parseInt(child) + parseInt(infant);
	
	$('.travelers span').text('Travelers '+total+', '+$('.rs_select_skin_activated :selected').text());
}

$(document).ready(function(){
	
	$('.flagclose').click(function(){
  $('.flagopen').hide();
  
  });
  $('.flag_opens').click(function(){
  $('.flagopen').show();
  
  });
	
	var ftrip = $('#trip').val();
	
	if(ftrip == 1){
		$('.ftripers').trigger('click');
	}
	
	$('.utilitynavs').click(function(){ 
		 $(".coun_cur_list").toggle();
	});
	
	$(document).on('change', '.paxcal', function() {
		travellerCal();
	});
	
	$('.rs_search').click(function(){
		
		var trip = $('#trip').val();
		
		var error  = 0;
		if($('#rs_from').val() == ''){
				$('#rs_from').css("border-color", "#DF0000");
				$('.error').addClass('tooltip-inner');
			    $('.error').show();
				error = 1;
			} else {
				$('#rs_from').css("border-color", "");
			} if($('#rs_to').val() == '') {
				$('#rs_to').css("border-color", "#DF0000");
				$('.error').addClass('tooltip-inner');
			    $('.error').show();
				
				error = 1;
			}else{
				$('#rs_to').css("border-color", "");
			}if($('#rs_chk_in').val() == ''){
				$('#rs_chk_in').css("border-color", "#DF0000");
				$('.error').addClass('tooltip-inner');
			    $('.error').show();
				
				error = 1;
			}else{
				$('#rs_chk_in').css("border-color", "");
			}if($('#rs_chk_out').val() == ''){
				
				if(trip == 1) {
					$('#rs_chk_out').css("border-color", "#DF0000");
					$('.error').addClass('tooltip-inner');
					$('.error').show();
					error = 1;
				} else {
					$('#rs_chk_out').css("border-color", "");
				}
			}else{
				$('#rs_chk_out').css("border-color", "");
			}
			if(error == 0) {
					$('.plf-load').show();    
					$('.plf-search').hide();
					$('#rs_air_forms').submit();

			}
			
	});
	/*$('#rs_searchs').click(function(){
		var error  = 0;
		if($('.rs_from').val() == ''){
			
				$('.rs_from').css("border-color", "#DF0000");
				$('.error').addClass('tooltip-inner');
			    $('.error').show();
				error = 1;
			} else {
				$('.rs_from').css("border-color", "");
			} if($('.rs_to').val() == '') {
				$('.rs_to').css("border-color", "#DF0000");
				$('.error').addClass('tooltip-inner');
			    $('.error').show();
				error = 1;
			}else{
				$('.rs_to').css("border-color", "");
			}if($('.rs_chk_in').val() == ''){
				$('.rs_chk_in').css("border-color", "#DF0000");
				$('.error').addClass('tooltip-inner');
			    $('.error').show();
				error = 1;
			}else{
				$('.rs_chk_in').css("border-color", "");
			}
			if(error == 0) {
				$('#rs_air_forms').submit();
			}
			
	});*/
	/* mobile date picker start*/
	/*$('.rs_mobi_in').datepicker({
		format: 'dd/mm/yy',
		maxViewMode: 0,
		startDate: "dateToday",
		autoclose: true
	});
	
	$('.rs_mobi_out').datepicker({
		format: 'dd/mm/yy',
		maxViewMode: 0,
		startDate: "dateToday",
		autoclose: true
	});
	$('.rs_mobi_in').datepicker().on('changeDate', function (ev) {
		var newDate = new Date(ev.date);
		var month = (newDate.getMonth()+1);
		var date = (newDate.getDate()+1);
		$('.rs_mobi_chk_day').val(date);
	});*/
	/* mobile date picker end*/
	
	$('.rs_chk_in').datepicker({
		format: 'dd/mm/yy',
		maxViewMode: 0,
		startDate: 'dateToday',
		autoclose: true
	});
	
	$('.rs_chk_out').datepicker({
		format: 'dd/mm/yy',
		maxViewMode: 0,
		startDate: "dateToday",
		autoclose: true
	});
	
	$('.rs_chk_in').datepicker().on('changeDate', function (ev) {
		var newDate = new Date(ev.date);
		//newDate.setDate(newDate.getDate() + 2)
		$('.rs_chk_out').datepicker('setStartDate', ev.date);
		$('.rs_chk_out').datepicker('setDate', newDate);
		$('.rs_chk_out').focus();
	});


 	$( ".rs_from" ).autocomplete({
		minLength: 2,
		autoFocus: true,
        source: function(request, response) {
            $.getJSON(
                "ajax.php",
                { term:request.term, extraParams:$('#changes_language').text() }, 
                response
            );
        },
		create: function () {
            $(this).data('ui-autocomplete')._renderItem = function (ul, item) {
                return $('<li>')
                    .append('<a><img src="' +item.flag + '" alt="flag"> ' + item.label + '</a>')
                    .appendTo(ul);
            };
        },
		select: function (event, ui) {
        $("#rs_o_code").val(ui.item.code);
        
    } 
    });
	
	$( ".rs_to" ).autocomplete({
		minLength: 2,
		autoFocus: true,
       source: function(request, response) {
            $.getJSON(
                "ajax.php",
                { term:request.term, extraParams:$('#changes_language').text() }, 
                response
            );
        },
		create: function () {
            $(this).data('ui-autocomplete')._renderItem = function (ul, item) {
                return $('<li>')
                    .append('<a><img src="' +item.flag + '" alt="flag"> ' + item.label + '</a>')
                    .appendTo(ul);
            };
        },
		select: function (event, ui) {
        $("#rs_d_code").val(ui.item.code);
        
    } 
    });
	/*$( ".rs_froms" ).autocomplete({
		minLength: 3,
        source: 'ajax1.php',
		create: function () {
			alert(item);
            $(this).data('ui-autocomplete')._renderItem = function (ul, item) {
                return $('<li>')
                    .append('<a><img src="' +item.flag + '" alt="flag"> ' + item.label + '</a>')
                    .appendTo(ul);
            };
        },
		onselect: function (event, ui) {
        $("#rs_o_code").val(ui.item.code);
        
    },
	open: function(event, ui) {
		$('.ui-autocomplete').off('menufocus hover mouseover mouseenter');
	}
    });
	
	$( ".rs_tos" ).autocomplete({
		minLength: 3,
        source: 'ajax1.php',
		create: function () {
            $(this).data('ui-autocomplete')._renderItem = function (ul, item) {
                return $('<li>')
                    .append('<a><img src="' +item.flag + '" alt="flag"> ' + item.label + '</a>')
                    .appendTo(ul);
            };
        },
		onselect: function (event, ui) {
        $("#rs_d_code").val(ui.item.code);
        
    },
	open: function(event, ui) {
		$('.ui-autocomplete').off('menufocus hover mouseover mouseenter');
	}
		
		
    });*/

});
