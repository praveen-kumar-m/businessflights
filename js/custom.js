"use strict";

// Added By Santhosh on 10-01-2017

$(document).on('click', '.collapsefilter, .mbe-filter-close1', function(){
	$('.fiterbox').toggle(500);
});

$(document).on('click', '.mbe-filter-close', function(){
	$('.bs-example-modal-sm').hide();
	$('.modal-backdrop').hide();
});

$(document).on('click', '.flagopen', function(){
	$('.bs-example-modal-sm').show();
	$('.modal-backdrop').show();
});

$(document).on('click', '.filterclose', function(){
	$('.fiterbox').hide();
});

$(document).on('click', '.flagclose', function(){
	$('.bs-example-modal-sm').removeClass('in');
	$('.modal-backdrop').removeClass('fade in');
	
	$('.bs-example-modal-sm').attr('aria-hidden','false');
	
});

$(document).on('click', '.flagopen', function(){

	$('.bs-example-modal-sm').show();
	
});


$(document).on('click', '.booking-sort', function() {
	$('.listtitle').toggle();
});

function LoadList(){
	
	var list_url = 'ajax.php?action=ajaxlist&uniqueId='+$('#uniqueId').val()+'&loadmore=0';
	
	$.ajax({
		async: true,
		url: list_url,
		success: function(res){
			if($.trim(res).toLowerCase().indexOf("pending") >= 0) {
				if ($.trim(res).toLowerCase().indexOf('value="0"') >= 0) {
					$('.booking-list').append(res);
				} else {
					$('.booking-list').hide();
					$('.booking-list').html(res).fadeIn();	
				}
				if(Number($('.flightcount').val()) > 0) {
					$('.ListLoading').hide();
					$('.loading-image').show();
					$('.TotalFlightCount').html($('.flightcount').val()+' '+'<img style="width: 20px;" src="images/loading.gif">');
					var currentflightcount = $('.flightcount').val();
					//console.log('57: updated flight value to '+currentflightcount);
				}
				LoadList();
			} else if($.trim(res).toLowerCase().indexOf("noresults") >= 0) {
				$('.ListLoading').hide();
				$('.loading-image').hide();
				$('.NoResulstAlert').show();
				$('.page-selection').hide();
				$('.TotalFlightCount').html(0);
				if($('#testvalues').val()) {
					$('.booking-list').append(res);
				}
			} else {
				$('.ListLoading').hide();
				$('.NoResulstAlert').hide();
				$('.loading-image').hide();
				setTimeout(function(){
					$('.booking-list').html('');
					$('.booking-list').append(res);
					var AjaxAirlines = $('.AjaxAirlines').html();
					$('.AjaxAirlines').remove();
					$('.FilterAirlines').html(AjaxAirlines);
					$('.TotalFlightCount').html($('.flightcount').val());
					var currentflightcount2 = $('.flightcount').val();
					//console.log('81: updated flight value to '+currentflightcount2);
					if($('.flightcount').val() > 20) {
						$('#page-selection').show();
					}
				}, 100);
			}
			/*setTimeout(function(){
				lazyload();
			}, 150);*/
			$('.page-selection, .fiterbox').css('opacity','1');
		}
	});
	
}

function listsorting(sorttype, sortorder, loadmore) {
	
	loadmore = (typeof loadmore === "undefined") ? "0" : loadmore;
	
	if(loadmore == 0) {
		//$('.loading-image').show();
	}
	
	$('.TotalFlightCount').html('<img style="width: 20px;" src="images/loading.gif">');
	
	var sorting = '&sorttype='+sorttype+'&sortorder='+sortorder;
	
	var filter = '';
	
	var stops = ''; //Stop
	
	$('.stop').each(function(){
		if(this.checked){
			if(stops) {
				stops += ','+$(this).val();
			} else {
				stops += $(this).val();
			}
		}
	});
	
	var time = '';
	
	$('.timefilter').each(function(){
		if(this.checked){
			if(time) {
				time += ','+$(this).val();
			} else {
				time += $(this).val();
			}
		}
	});
	
	var Airline = '';
	var Airline_check = true;
	
	$('.AirlinesName').each(function(){
		if(!this.checked){
			if(Airline) {
				if($(this).val()) {
					Airline += ';'+$(this).val();
				}
			} else {
				if($(this).val()) {
					Airline += $(this).val();
				}
			}
		} else {
			Airline_check = false;
		}
	});
	
	if(Airline_check != false) {
		Airline = '';
	}
	
	filter = '&stops='+stops+'&outbounddeparttime='+time+'&excludecarriers='+Airline;
	
	var list_url = 'ajax.php?action=ajaxlist&uniqueId='+$('#uniqueId').val()+sorting+filter+'&loadmore='+loadmore;
	
	//filters change
	$.ajax({
		url: list_url,
		success: function(res){
			if($.trim(res).toLowerCase().indexOf("pending") >= 0) {
				listsorting(sorttype, sortorder, loadmore);
			} else if($.trim(res).toLowerCase().indexOf("noresults") >= 0) {
				$('.ListLoading').hide();
				if(parseInt($.trim(loadmore)) == parseInt(0)){
					$('.booking-list').html('');
					$('.TotalFlightCount').html(0);
				}
				$('.NoResulstAlert').show();
				$('#page-selection').hide();
				$('.TotalFlightCount').html($('.flightcount').val());
			}  else {
				$('.NoResulstAlert').hide();
				setTimeout(function(){
					if(parseInt($.trim(loadmore)) == parseInt(0)){
						$('.booking-list').html('');
					}
					$('.booking-list').append(res);
					$('.AjaxAirlines').remove();
					$('.TotalFlightCount').html($('.flightcount').val());
					if($('.flightcount').val() > 20) {
						$('#page-selection').show();
					}
					$('.ListLoading').hide();
				}, 100);
				$('.booking-list').show();
			}
			$('.ListPageResults').css('opacity','1');
			//lazyload();
			$('.page-selection, .fiterbox').css('opacity','1');
			$('.loading-image').hide();
		}
	});
}

/*function lazyload() {
	$('.booking-list img').imgLazyLoad({
		container: window,
		effect: 'fadeIn',
		speed: 700,
		delay: 600,
		callback: function(){
			$( this ).css( 'opacity', .99 );
		}
	});
	$('.page-selection, .fiterbox').css('opacity','1');
}*/

function UpdateSortNames() {
	$('.sortName').html('Airline');
	$('.sortFrom').html('From');
	$('.sortTo').html('To');
	$('.sortDuration').html('Duration');
	$('.sortPrice').html('Price');
}

$(document).ready(function() {
	
	$('.LadGif').show();
	
	$('.page-selection, .fiterbox').css('opacity','0.5');
	
	setTimeout(function(){
			LoadList();
	},1000);
	
	var sortorder = '';
	
	var sorttype = '';
	
	var outbounddeparttime = 'desc';
	
	$(document).on('click', '.sortTo', function(){
		UpdateSortNames();
		$('.ListLoading').show();
		$('#page-selection, .booking-list, .NoResulstAlert').hide();
		$('.fiterbox').css('opacity','0.5');
			if(outbounddeparttime == 'desc') {
				outbounddeparttime = 'asc';
			} else {
				outbounddeparttime = 'desc';
			}
			$(this).html('To <i class="fa fa-sort-'+outbounddeparttime+'" aria-hidden="true"></i>');
			listsorting('outbounddeparttime',outbounddeparttime);
			sorttype = 'outbounddeparttime';
			sortorder = outbounddeparttime;
	});
	
	var outboundarrivetime = 'desc';
	
	$(document).on('click', '.sortFrom', function(){
		UpdateSortNames();
		$('.ListLoading').show();
		$('#page-selection, .booking-list, .NoResulstAlert').hide();
		$('.fiterbox').css('opacity','0.5');
			if(outboundarrivetime == 'desc') {
				outboundarrivetime = 'asc';
			} else {
				outboundarrivetime = 'desc';
			}
			$(this).html('From <i class="fa fa-sort-'+outboundarrivetime+'" aria-hidden="true"></i>');
			listsorting('outboundarrivetime',outboundarrivetime);
			sorttype = 'outboundarrivetime';
			sortorder = outboundarrivetime;
	});
	
	var priceSort = 'desc';
	
	$(document).on('click', '.sortPrice', function(){
		UpdateSortNames();
		$('.ListLoading').show();
		$('#page-selection, .booking-list, .NoResulstAlert').hide();
		$('.fiterbox').css('opacity','0.5');
			if(priceSort == 'desc') {
				priceSort = 'asc';
			} else {
				priceSort = 'desc';
			}
			$(this).html('Price <i class="fa fa-sort-'+priceSort+'" aria-hidden="true"></i>');
			listsorting('price',priceSort);
			sorttype = 'price';
			sortorder = priceSort;
	});
	
	$(document).on('click', '.bubblecheap', function(){
		$('.ListLoading').show();
		$('#page-selection, .booking-list, .NoResulstAlert').hide();
		$('.fiterbox').css('opacity','0.5');
		$(this).html('Price <i class="fa fa-sort-'+priceSort+'" aria-hidden="true"></i>');
		listsorting('price','asc');
		sorttype = 'price';
		sortorder = priceSort;
	});
	
	var durationSort = 'desc';
	
	$(document).on('click', '.sortDuration', function(){
		UpdateSortNames();
		$('.ListLoading').show();
		$('#page-selection, .booking-list, .NoResulstAlert').hide();
		$('.fiterbox').css('opacity','0.5');
			if(durationSort == 'desc') {
				durationSort = 'asc';
			} else {
				durationSort = 'desc';
			}
			$(this).html('Duration <i class="fa fa-sort-'+durationSort+'" aria-hidden="true"></i>');
			listsorting('duration',durationSort);
			sorttype = 'duration';
			sortorder = durationSort;
	});
	
	$(document).on('click', '.bubbleshort', function(){
		$('.ListLoading').show();
		$('#page-selection, .booking-list, .NoResulstAlert').hide();
		$('.fiterbox').css('opacity','0.5');
		$(this).html('Duration <i class="fa fa-sort-'+durationSort+'" aria-hidden="true"></i>');
		listsorting('duration','asc');
		sorttype = 'duration';
		sortorder = durationSort;
	});
	
	var Sortname = 'desc';
	
	$(document).on('click', '.sortName', function(){
		UpdateSortNames();
		$('.ListLoading').show();
		$('#page-selection, .booking-list, .NoResulstAlert').hide();
		$('.fiterbox').css('opacity','0.5');
			if(Sortname == 'desc') {
				Sortname = 'asc';
			} else {
				Sortname = 'desc';
			}
			$(this).html('Airline <i class="fa fa-sort-'+Sortname+'" aria-hidden="true"></i>');
			listsorting('carrier',Sortname);
			sorttype = 'carrier';
			sortorder = Sortname;
	});
	
	//Pagination
	
	$(document).on('click', '#page-selection', function() {
		$('.ListLoading').show();
		$('#page-selection, .NoResulstAlert').hide();
		$('.fiterbox').css('opacity','0.5');
		//setTimeout(function(){
			/*var length = $(".FlightList:visible").length + 1
			var end = $(".FlightList").length;
			var i = 0;
			var count = 0;
			for(i=length;i<=end;i++){
				++count;
				$('.booking-list li:nth-child(' + i + ')').show(500);
				if(count == 20){
					break;
				}
			}*/
			var end = $(".FlightList").length;
			listsorting(sorttype, sortorder, end);
			//$('.ListLoading').hide();
			//$('.ListPageResults').css('opacity','1');
		//}, 500);
	});
	
	$(document).on('click', '.stop', function(){
		$('.ListLoading').show();
		$('#page-selection, .booking-list, .NoResulstAlert').hide();
		$('.fiterbox').css('opacity','0.5');
		listsorting(sorttype, sortorder);
		if($('.fiterbox').hasClass('mobileview')) {
			$('.mbe-filter-close1').trigger('click');
		}
	});
	
	$(document).on('click', '.AirlinesName', function(){
		$('.ListLoading').show();
		$('#page-selection, .booking-list, .NoResulstAlert').hide();
		$('.fiterbox').css('opacity','0.5');
		listsorting(sorttype, sortorder);
		if($('.fiterbox').hasClass('mobileview')) {
			$('.mbe-filter-close1').trigger('click');
		}
	});
	
	$(document).on('click', '.timefilter', function(){
		$('.ListLoading').show();
		$('#page-selection, .booking-list, .NoResulstAlert').hide();
		$('.fiterbox').css('opacity','0.5');
		listsorting(sorttype, sortorder);
		if($('.fiterbox').hasClass('mobileview')) {
			$('.mbe-filter-close1').trigger('click');
		}
	});
	
	/*$('input').on('ifClicked', function (event){
		$('.booking-list, .page-selection').css('opacity','0.5');
		//$(this).closest("input").attr('checked', true);
		if($(this).closest(".checkbox").hasClass('stop')){
			if($(this).closest(".checkbox").attr('data-check') == 'check') {
				$(this).closest(".checkbox").attr('data-check', 'uncheck');
			} else {
				$(this).closest(".checkbox").attr('data-check', 'check');
			}
		}
		if($(this).closest(".checkbox").hasClass('timefilter')){
			if($(this).closest(".checkbox").attr('data-check') == 'check') {
				$(this).closest(".checkbox").attr('data-check', 'uncheck');
			} else {
				$(this).closest(".checkbox").attr('data-check', 'check');
			}
		}
		if($(this).closest(".checkbox").hasClass('filterAirline')){
			if($(this).closest(".checkbox").attr('data-check') == 'check') {
				$(this).closest(".checkbox").attr('data-check', 'uncheck');
			} else {
				$(this).closest(".checkbox").attr('data-check', 'check');
			}
		}
		listsorting(sorttype, sortorder);
	});*/
	
});

// End

$('ul.slimmenu').slimmenu({
    resizeWidth: '992',
    collapserTitle: 'Main Menu',
    animSpeed: 250,
    indentChildren: true,
    childrenIndenter: ''
});


// Countdown
$('.countdown').each(function() {
    var count = $(this);
    $(this).countdown({
        zeroCallback: function(options) {
            var newDate = new Date(),
                newDate = newDate.setHours(newDate.getHours() + 130);

            $(count).attr("data-countdown", newDate);
            $(count).countdown({
                unixFormat: true
            });
        }
    });
});


$('.btn').button();

$("[rel='tooltip']").tooltip();

$('.form-group').each(function() {
    var self = $(this),
        input = self.find('input');

    input.focus(function() {
        self.addClass('form-group-focus');
    })

    input.blur(function() {
        if (input.val()) {
            self.addClass('form-group-filled');
        } else {
            self.removeClass('form-group-filled');
        }
        self.removeClass('form-group-focus');
    });
});

$('.typeahead').typeahead({
    hint: true,
    highlight: true,
    minLength: 3,
    limit: 8
}, {
    source: function(q, cb) {
        return $.ajax({
            dataType: 'json',
            type: 'get',
            url: 'http://gd.geobytes.com/AutoCompleteCity?callback=?&q=' + q,
            chache: false,
            success: function(data) {
                var result = [];
                $.each(data, function(index, val) {
                    result.push({
                        value: val
                    });
                });
                cb(result);
            }
        });
    }
});


$('input.date-pick, .input-daterange, .date-pick-inline').datepicker({
    todayHighlight: true
});



$('input.date-pick, .input-daterange input[name="start"]').datepicker('setDate', 'today');
$('.input-daterange input[name="end"]').datepicker('setDate', '+7d');

$('input.time-pick').timepicker({
    minuteStep: 15,
    showInpunts: false
})

$('input.date-pick-years').datepicker({
    startView: 2
});




$('.booking-item-price-calc .checkbox label').click(function() {
    var checkbox = $(this).find('input'),
        // checked = $(checkboxDiv).hasClass('checked'),
        checked = $(checkbox).prop('checked'),
        price = parseInt($(this).find('span.pull-right').html().replace('$', '')),
        eqPrice = $('#car-equipment-total'),
        tPrice = $('#car-total'),
        eqPriceInt = parseInt(eqPrice.attr('data-value')),
        tPriceInt = parseInt(tPrice.attr('data-value')),
        value,
        animateInt = function(val, el, plus) {
            value = function() {
                if (plus) {
                    return el.attr('data-value', val + price);
                } else {
                    return el.attr('data-value', val - price);
                }
            };
            return $({
                val: val
            }).animate({
                val: parseInt(value().attr('data-value'))
            }, {
                duration: 500,
                easing: 'swing',
                step: function() {
                    if (plus) {
                        el.text(Math.ceil(this.val));
                    } else {
                        el.text(Math.floor(this.val));
                    }
                }
            });
        };
    if (!checked) {
        animateInt(eqPriceInt, eqPrice, true);
        animateInt(tPriceInt, tPrice, true);
    } else {
        animateInt(eqPriceInt, eqPrice, false);
        animateInt(tPriceInt, tPrice, false);
    }
});


$('div.bg-parallax').each(function() {
    var $obj = $(this);
    if($(window).width() > 992 ){
        $(window).scroll(function() {
            var animSpeed;
            if ($obj.hasClass('bg-blur')) {
                animSpeed = 10;
            } else {
                animSpeed = 15;
            }
            var yPos = -($(window).scrollTop() / animSpeed);
            var bgpos = '50% ' + yPos + 'px';
            $obj.css('background-position', bgpos);

        });
    }
});



//$('.nav-drop').dropit();

/*$("#price-slider").ionRangeSlider({
    min: 0,
    max: 1000,
    type: 'double',
    prefix: "$",
    // maxPostfix: "+",
    prettify: false,
    hasGrid: true,
	onFinish: function (data) {
       // $('.')
    },
});
*/
/*$('.i-check, .i-radio').iCheck({
    checkboxClass: 'i-check',
    radioClass: 'i-radio'
});*/



$('.booking-item-review-expand').click(function(event) {
    var parent = $(this).parent('.booking-item-review-content');
    if (parent.hasClass('expanded')) {
        parent.removeClass('expanded');
    } else {
        parent.addClass('expanded');
    }
});


$('.stats-list-select > li > .booking-item-rating-stars > li').each(function() {
    var list = $(this).parent(),
        listItems = list.children(),
        itemIndex = $(this).index();

    $(this).hover(function() {
        for (var i = 0; i < listItems.length; i++) {
            if (i <= itemIndex) {
                $(listItems[i]).addClass('hovered');
            } else {
                break;
            }
        };
        $(this).click(function() {
            for (var i = 0; i < listItems.length; i++) {
                if (i <= itemIndex) {
                    $(listItems[i]).addClass('selected');
                } else {
                    $(listItems[i]).removeClass('selected');
                }
            };
        });
    }, function() {
        listItems.removeClass('hovered');
    });
});

$(document).on('click', '.alldeal', function(){
	$(this).closest('.flightrow').find('.booking-item').trigger('click');
});

/*$(document).on('click', '.booking-item', function() {
	if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        $(this).parent().removeClass('active');
		$(this).find('.flightmore').html('Show Details');
    } else {
		$(this).find('.flightmore').html('Hide Details');
        $(this).addClass('active');
        $(this).parent().addClass('active');
        $(this).delay(1500).queue(function() {
            $(this).addClass('viewed')
        });
    }
});
*/

$(document).on('click', '.moreresult-dialog', function() {
	var rel=$(this).attr('data-rel');
	var childid = $(this).children('.Flightsearchnew').attr('id');
	//console.log(childid);
	var a = $(this).children('.Flightsearchnew').clone().prop('id','cloneddiv');
	$("body").append(a);
	$('.newview'+rel).show();
	//$('.moreresult-dialog .newview'+rel).hide();
	$("body").css({"overflow":"visible"});
	$("body").addClass("addbodycls");
	$("body").css({"height":$("body").height()});
	$(this).removeClass('moreresult-dialog');
});

$(document).on('click', '.closenew', function() {
	var rel=$(this).attr('data-rel');
	$('.newview'+rel).parent('.flightrow').addClass('moreresult-dialog');
	$('.newview'+rel).css({"display":"none"});
	$("body").removeClass("addbodycls");
	$("body").css({"overflow":"visible"});
	$("body").css({"height":"auto"});
	//if($('#cloneddiv').length > 0) $('#cloneddiv').remove();
  $('#cloneddiv.Flightsearchnew').remove();
});

/*$('.booking-item-container').children('.booking-item').click(function(event) {
	alert('alldeal');
    if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        $(this).parent().removeClass('active');
		$(this).find('.flightmore').html('Show Details');
    } else {
		$(this).find('.flightmore').html('Hide Details');
        $(this).addClass('active');
        $(this).parent().addClass('active');
        $(this).delay(1500).queue(function() {
            $(this).addClass('viewed')
        });
    }
});
*/

// Lighbox text
$('.popup-text').magnificPopup({
    removalDelay: 500,
    closeBtnInside: true,
    callbacks: {
        beforeOpen: function() {
            this.st.mainClass = this.st.el.attr('data-effect');
        }
    },
    midClick: true
});

// Lightbox iframe
$('.popup-iframe').magnificPopup({
    dispableOn: 700,
    type: 'iframe',
    removalDelay: 160,
    mainClass: 'mfp-fade',
    preloader: false
});



$('.form-group-select-plus').each(function() {
    var self = $(this),
        btnGroup = self.find('.btn-group').first(),
        select = self.find('select');
    btnGroup.children('label').last().click(function() {
        btnGroup.addClass('hidden');
        select.removeClass('hidden');
    });
});
// Responsive videos

var tid = setInterval(tagline_vertical_slide, 2500);

// vertical slide
function tagline_vertical_slide() {
    var curr = $("#tagline ul li.active");
    curr.removeClass("active").addClass("vs-out");
    setTimeout(function() {
        curr.removeClass("vs-out");
    }, 500);

    var nextTag = curr.next('li');
    if (!nextTag.length) {
        nextTag = $("#tagline ul li").first();
    }
    nextTag.addClass("active");
}

function abortTimer() { // to be called when you want to stop the timer
    clearInterval(tid);
}