$(document).ready(function(){

    var windowSize = $(window).width();

    // mobile menu toggle 
    $(".mobile-menu-toggle").click(function() {   
        $(".currency-header").removeClass('toggle');
    	$(this).toggleClass('open');
    	if ($("body").hasClass("overflow")) {
    		$("body").removeClass("overflow");
    	}  else {
    		$("body").addClass("overflow");
    	}
    	$(".index-menu .index-menu-wrapper").slideToggle();
    	$(".index-menu .index-menu-wrapper li").each( function(){
    		if ($(this).hasClass("show")) {
	    		$(this).removeClass("show");
	    	}  else {
	    		$(this).addClass("show");
	    	}
    	});
	});

    $(window).scroll(function(){
        if (windowSize >= 577 && windowSize <= 991) {
            $(".currency-container").removeClass('toggle');
            $(".currency-container").hide();
        }
    });

    $(document).on("click", function (e) {
        if (e.target.class == "currency-container" || $(e.target).parents(".currency-container-wrap").length) {
            //$(".guest-dropdown-block").show();
        } else {
            $(".currency-container").removeClass('toggle');
            $(".currency-container").hide();
        }
    });

    $(".currency-header").on("click", function(){
        if (windowSize <= 768) {
            $(this).toggleClass('toggle');
        }
        if ($(this).parent().find(".currency-container").hasClass('toggle')) {
            $(this).parent().find(".currency-container").removeClass('toggle');
        } else {
            $(this).parent().find(".currency-container").addClass('toggle');
        }
    });

    $(".currency-container li").on("click", function(){
        $(".currency-container").removeClass('toggle');
        $(".currency-container").hide();
        if (windowSize <= 768) {
            $(".currency-header").removeClass('toggle');
        }
    });

});