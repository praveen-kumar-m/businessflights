! function(a) {
    function b(a, b) {
        b = b || {};
        var c = b.sprintf || [],
            d = a;
        return e(i) && (translation = h[i], translation.hasOwnProperty(a) && (d = translation[a])), l(d, c)
    }

    function c(a, b, c) {
        c = c || {};
        var d = c.count,
            f = c.sprintf || [],
            g = a,
            k = j[i],
            m = k(d);
        return 0 === f.length && f.push(d), 1 == m && (g = b), e(i) && (translation = h[i], translation.hasOwnProperty(a) && (results = translation[a], Array.isArray(results) && m < results.length && (g = results[k(d)]))), l(g, f)
    }

    function d(a, b, c) {
        h[a] = b, c === !0 && f(a)
    }

    function e(a) {
        return h.hasOwnProperty(a)
    }

    function f(a) {
        i = a
    }
    var g = {},
        h = {},
        i = "en-us",
        j = {
            "en-us": function(a) {
                return Number(1 != a)
            },
            "en-gb": function(a) {
                return Number(1 != a)
            },
            "es-us": function(a) {
                return Number(1 != a)
            },
            "fr-fr": function(a) {
                return Number(a > 1)
            }
        },
        k = function() {
            function a(a) {
                return Object.prototype.toString.call(a).slice(8, -1).toLowerCase()
            }

            function b(a, b) {
                for (var c = []; b > 0; c[--b] = a);
                return c.join("")
            }
            var c = function() {
                return c.cache.hasOwnProperty(arguments[0]) || (c.cache[arguments[0]] = c.parse(arguments[0])), c.format.call(null, c.cache[arguments[0]], arguments)
            };
            return c.format = function(c, d) {
                var e, f, g, h, i, j, l, m = 1,
                    n = c.length,
                    o = "",
                    p = [];
                for (f = 0; n > f; f++)
                    if (o = a(c[f]), "string" === o) p.push(c[f]);
                    else if ("array" === o) {
                    if (h = c[f], h[2])
                        for (e = d[m], g = 0; g < h[2].length; g++) {
                            if (!e.hasOwnProperty(h[2][g])) throw k('[sprintf] property "%s" does not exist', h[2][g]);
                            e = e[h[2][g]]
                        } else e = h[1] ? d[h[1]] : d[m++];
                    if (/[^s]/.test(h[8]) && "number" != a(e)) throw k("[sprintf] expecting number but found %s", a(e));
                    switch (h[8]) {
                        case "b":
                            e = e.toString(2);
                            break;
                        case "c":
                            e = String.fromCharCode(e);
                            break;
                        case "d":
                            e = parseInt(e, 10);
                            break;
                        case "e":
                            e = h[7] ? e.toExponential(h[7]) : e.toExponential();
                            break;
                        case "f":
                            e = h[7] ? parseFloat(e).toFixed(h[7]) : parseFloat(e);
                            break;
                        case "o":
                            e = e.toString(8);
                            break;
                        case "s":
                            e = (e = String(e)) && h[7] ? e.substring(0, h[7]) : e;
                            break;
                        case "u":
                            e = Math.abs(e);
                            break;
                        case "x":
                            e = e.toString(16);
                            break;
                        case "X":
                            e = e.toString(16).toUpperCase()
                    }
                    e = /[def]/.test(h[8]) && h[3] && e >= 0 ? "+" + e : e, j = h[4] ? "0" == h[4] ? "0" : h[4].charAt(1) : " ", l = h[6] - String(e).length, i = h[6] ? b(j, l) : "", p.push(h[5] ? e + i : i + e)
                }
                return p.join("")
            }, c.cache = {}, c.parse = function(a) {
                for (var b = a, c = [], d = [], e = 0; b;) {
                    if (null !== (c = /^[^\x25]+/.exec(b))) d.push(c[0]);
                    else if (null !== (c = /^\x25{2}/.exec(b))) d.push("%");
                    else {
                        if (null === (c = /^\x25(?:([1-9]\d*)\$|\(([^\)]+)\))?(\+)?(0|'[^$])?(-)?(\d+)?(?:\.(\d+))?([b-fosuxX])/.exec(b))) throw "[sprintf] huh?";
                        if (c[2]) {
                            e |= 1;
                            var f = [],
                                g = c[2],
                                h = [];
                            if (null === (h = /^([a-z_][a-z_\d]*)/i.exec(g))) throw "[sprintf] huh?";
                            for (f.push(h[1]);
                                "" !== (g = g.substring(h[0].length));)
                                if (null !== (h = /^\.([a-z_][a-z_\d]*)/i.exec(g))) f.push(h[1]);
                                else {
                                    if (null === (h = /^\[(\d+)\]/.exec(g))) throw "[sprintf] huh?";
                                    f.push(h[1])
                                }
                            c[2] = f
                        } else e |= 2;
                        if (3 === e) throw "[sprintf] mixing positional and named placeholders is not (yet) supported";
                        d.push(c)
                    }
                    b = b.substring(c[0].length)
                }
                return d
            }, c
        }(),
        l = function(a, b) {
            b.unshift(a);
            try {
                return k.apply(null, b)
            } catch (c) {
                return console.error(c), b
            }
        };
    g._t = b, g._n = c, g.set_lang = f, g.add_lang = d, g.has_lang = e, g.sprintf = k, g.vsprintf = l, a.i18n = a.i18n || g
}(jQuery, window, document);
var uuid = null;
! function(a, b, c, d) {
    function e() {
        for (var a, b = 3, d = c.createElement("div"), e = d.getElementsByTagName("i"); d.innerHTML = "<!--[if gt IE " + ++b + "]><i></i><![endif]-->", e[0];) return b > 4 ? b : a
    }

    function f() {
        var a, b = (new Date).getTime();
        return uuid = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function(c) {
            return a = (b + 16 * Math.random()) % 16 | 0, b = Math.floor(b / 16), ("x" == c ? a : 7 & a | 8).toString(16)
        })
    }

    function g(b) {
        var c = {
            v: "1",
            tid: "UA-1825499-106",
            cid: uuid
        };
        return i.arguments && i.arguments[1].logging === !1 || a.fn.searchbox.get(!0, !0) && a.fn.searchbox.get(!0, !0).options.logging === !1 ? !1 : b.el && "object" == typeof b.el && "undefined" == typeof JSON ? !1 : (b.el && "object" == typeof b.el && (b.el = JSON.stringify(b.el)), void a.ajax({
            type: "POST",
            url: ("https:" == location.protocol ? "https://ssl." : "http://www.") + "google-analytics.com/collect",
            data: a.extend(!0, {}, c, b)
        }))
    }

    function h(a, c) {
        c = "undefined" == c ? !0 : c, "undefined" == typeof b.console || e() || console.log("object" == typeof a ? a : c ? "Searchbox Error: " + a : a)
    }
    var i = function(a, b, d) {
        return this.options = b, this.$el = a, this.init(), "function" == typeof d && d && d.call(this), g({
            t: "event",
            ec: "searchbox load",
            ea: this.options.refid + ": " + location.href,
            el: {
                version: this.config.version,
                version_date: this.config.version_date
            }
        }), g({
            an: this.config.version_date,
            av: this.config.version,
            aid: "searchbox",
            t: "screenview",
            cd: this.options.refid,
            dt: c.location.hostname
        }), this
    };
    i.prototype = {
        dates: {
            now: new Date,
            now_est: new Date((new Date).getTime() + 6e4 * (new Date).getTimezoneOffset() + -18e6),
            today: new Date((new Date).getFullYear(), (new Date).getMonth(), (new Date).getDate()),
            yesterday: new Date((new Date).getFullYear(), (new Date).getMonth(), (new Date).getDate() - 1),
            max_date: new Date((new Date).getFullYear(), (new Date).getMonth(), (new Date).getDate() + 329),
            allow_previous: !1,
            days_in_months: [31, 29 == new Date((new Date).getFullYear(), 1, 29).getDate() ? 29 : 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
            months: [a.i18n._t("January"), a.i18n._t("February"), a.i18n._t("March"), a.i18n._t("April"), a.i18n._t("May"), a.i18n._t("June"), a.i18n._t("July"), a.i18n._t("August"), a.i18n._t("September"), a.i18n._t("October"), a.i18n._t("November"), a.i18n._t("December")],
            days: [a.i18n._t("Sunday"), a.i18n._t("Monday"), a.i18n._t("Tuesday"), a.i18n._t("Wednesday"), a.i18n._t("Thursday"), a.i18n._t("Friday"), a.i18n._t("Saturday")]
        },
        init: function() {
            var a;
            this.init_check("hotel"), this.init_check("car"), this.init_check("air"), this.init_check("vp"), this.ua = b.navigator.userAgent, this.options.hotel.currentDate = this.options.car.currentDate = this.options.air.currentDate = this.options.vp.currentDate = this.dates.now, this.config = {
                protocol: "file:" == b.location.protocol ? "http:" : "",
                version: "2.37.00",
                version_date: "12/15/2016",
                mobile: !1,
                search_logging: !1
            }, this.config.refdata_url = this.init_getURL("refdata"), this.config.autosuggest_url = this.init_getURL("autosuggest"), /iPhone|iPod|iPad|Android|BlackBerry|BB10|RIM Tablet|Windows Phone|SymbianOS|Kindle|Silk/.test(this.ua) && (this.options.hotel.autosuggest.geolocation = !0, this.options.car.autosuggest.geolocation = !0, this.options.vp.autosuggest.geolocation = !0, this.options.air.autosuggest.geolocation = !0, this.config.mobile = !0), this.plugins = {}, parseFloat(this.dates.now_est.getHours() + "." + this.dates.now_est.getMinutes()) <= 5 && (this.dates.allow_previous = !0), this.options.skip_ref ? (this.options.vcid = this.options.vcid || 28948, this.options.cname = this.init_getURL("cname"), this.init_bind("hotel"), this.init_bind("car"), this.init_bind("air"), this.init_bind("vp")) : (a = new Date, this.init_ref(), g({
                t: "timing",
                utc: "searchbox getRefData",
                utv: "load",
                utl: this.options.refid + ": " + location.href,
                utt: new Date - a
            })), this.init_plugin(), this.options.hotel.express_deals && (this.options.hotel.autosuggest.hotels = !1, this.options.hotel.autosuggest.pois = !1, this.options.hotel.autosuggest.regions = !1, this.options.hotel.autosuggest.airports = !1, this.options.hotel.calendar.required = !0)
        },
        init_plugin: function() {
            var b, c = this,
                d = a(".rs_searchbox_plugin", this.options.hotel.$parent),
                e = '<input name="searchbox_plugin" class="rs_searchbox_plugin" type="hidden" />';
            if (2486 == parseInt(this.options.refid)) this.options.hotel.autosuggest.generate = !1, this.options.hotel.autosuggest.geolocation = !1, b = "p_2486";
            else if (6758 == parseInt(this.options.refid) && "6758_venues" == this.options.hotel.autosuggest.plugin) this.options.hotel.autosuggest.generate = !1, this.options.hotel.autosuggest.geolocation = !1, b = "p_6758", this.check_add(d, this.options.hotel.$parent, e, "6758_venues");
            else if (6912 == parseInt(this.options.refid) && "6912_events" == this.options.hotel.autosuggest.plugin) this.options.hotel.autosuggest.generate = !1, this.options.hotel.autosuggest.geolocation = !1, b = "p_6912", this.check_add(d, this.options.hotel.$parent, e, "6912_events");
            else if (2547 == parseInt(this.options.refid) && "2547_colleges" == this.options.hotel.autosuggest.plugin) this.options.hotel.autosuggest.generate = !1, this.options.hotel.autosuggest.geolocation = !1, b = "p_2547", this.check_add(d, this.options.hotel.$parent, e, "2547_colleges");
            else if (6032 == parseInt(this.options.refid) && "6032_hotels" == this.options.hotel.autosuggest.plugin) this.options.hotel.autosuggest.generate = !1, this.options.hotel.autosuggest.geolocation = !1, b = "p_6032", this.check_add(d, this.options.hotel.$parent, e, "6032_hotels");
            else if (6614 == parseInt(this.options.refid) && "6614_cities" == this.options.hotel.autosuggest.plugin) this.options.hotel.autosuggest.generate = !1, this.options.hotel.autosuggest.geolocation = !1, b = "p_6614", this.check_add(d, this.options.hotel.$parent, e, "6614_cities");
            else if (7564 == parseInt(this.options.refid) && "7564_cities" == this.options.hotel.autosuggest.plugin) this.options.hotel.autosuggest.generate = !1, this.options.hotel.autosuggest.geolocation = !1, b = "p_7564", this.check_add(d, this.options.hotel.$parent, e, "7564_cities");
            else if ("object" == typeof ref && ref.site_options && ref.site_options.intent_media_sca && "" !== ref.site_options.intent_media_sca.value && "object" == typeof rs_global && rs_global.intent_media)
                if (this.options.hotel.autosuggest.set_callback = rs_global.intent_media.runIntentMediaCallBack, "function" == typeof this.options.hotel.post_check) {
                    var f = this.options.hotel.post_check;
                    this.options.hotel.post_check = function(a, b, c) {
                        return f(a, b, c), rs_global.intent_media.addSCACall(a, b, c), !0
                    }
                } else this.options.hotel.post_check = rs_global.intent_media.addSCACall;
                "object" == typeof ref && ref.site_options && "object" == typeof ref.site_options.aaa_redirect && "1" === ref.site_options.aaa_redirect.value && (this.plugins.aaa_redirect = !0), b && a.ajax({
                url: this.init_getURL("plugin").replace("{name}", b),
                crossDomain: !0,
                dataType: "script",
                success: function() {
                    c.options.hotel.autosuggest.callback = a.fn.searchbox.autosuggest_plugin
                },
                error: function() {
                    h('Could not load plugin "' + b + '".'), g({
                        t: "event",
                        ec: "searchbox error",
                        ea: c.options.refid + ": " + location.href,
                        el: 'Could not load plugin "' + b + '".'
                    })
                }
            })
        },
        init_check: function(b) {
            var c, d, e = [],
                f = this.options[b];
            if (f.enabled)
                for (c in f.elements) f.elements.hasOwnProperty(c) && ("form" == c ? (d = f.elements[c].charAt(0), "#" != d && "." != d && 0 === a("form[name=" + f.elements[c] + "]", this.$el).length || 0 === a(f.elements[c], this.$el).length && ("#" == d || "." == d) ? e.push("The specified form for " + b + " does not exist.") : f.$parent = "#" == d || "." == d ? a(f.elements[c], this.$el) : a("form[name=" + f.elements[c] + "]", this.$el)) : 0 === a(f.elements[c], f.$parent).length && "required" != c && ("chk_in" == c || "chk_out" == c ? 0 === a(f.elements.month_in, f.$parent).length && 0 === a(f.elements.month_out, f.$parent).length && 0 === a(f.elements.day_in, f.$parent).length && 0 === a(f.elements.day_out, f.$parent).length && f.elements.required && e.push("The specified div for " + b + ' "' + c + '" does not exist.') : "month_in" == c || "month_out" == c || "day_in" == c || "day_out" == c ? 0 === a(f.elements.chk_in, f.$parent).length && 0 === a(f.elements.chk_out, f.$parent).length && f.elements.required && e.push("The specified div for " + b + ' "' + c + '" does not exist.') : "round_trip" == c && !f.elements.round_trip || "one_way" == c && !f.elements.one_way || "multi_dest" == c && !f.elements.multi_dest || "infant_seating" == c || f.elements.required && e.push("The specified div for " + b + ' "' + c + '" does not exist.')));
            return e.length > 0 && f.elements.required ? (g({
                t: "event",
                ec: "searchbox error",
                ea: this.options.refid + ": " + location.href,
                el: e.join(" ")
            }), h(e.join(" ")), !1) : !0
        },
        init_getURL: function(a) {
            var b = "";
            return b = "local" == this.options.environment && -1 !== c.location.hostname.indexOf("rezserver.ppn.dockerhost") ? "//rezserver.ppn.dockerhost" : "local" == this.options.environment && -1 !== c.location.hostname.indexOf("rezserver3.ppn.local") ? "//" + c.location.hostname : "local" == this.options.environment ? "//rezserver3.ppn.local" : "qa" == this.options.environment ? "//qa.rezserver.com" : -1 !== c.location.hostname.indexOf("a401.corp") ? "//www.a401.corp.rezserver.com" : -1 !== c.location.hostname.indexOf("b402.corp") ? "//www.b402.corp.rezserver.com" : -1 !== c.location.hostname.indexOf("a403.corp") ? "//www.a403.corp.rezserver.com" : -1 !== c.location.hostname.indexOf("a001.corp") ? "//secure.a001.corp.rezserver.com" : -1 !== c.location.hostname.indexOf("a002.corp") ? "//secure.a002.corp.rezserver.com" : -1 !== c.location.hostname.indexOf("a003.corp") ? "//secure.a003.corp.rezserver.com" : -1 !== c.location.hostname.indexOf("p1.corp") ? "//www.p1.corp.rezserver.com" : "//secure.rezserver.com", "refdata" == a ? b = this.config.protocol + b + "/api/getRefData" : "autosuggest" == a ? b = this.config.protocol + b + "/api/autosuggest/" : "cname" == a ? b = this.options.backend || "" == this.options.cname && "prod" == this.options.environment ? "" : ("http" == this.options.protocol || "https" == this.options.protocol ? this.options.protocol + ":" : this.config.protocol) + "//" + this.options.cname.replace(/(^http:\/\/www\.)|(^https:\/\/www\.)|(^http:\/\/)|(^https:\/\/)|(^\/\/)/, "") : "plugin" == a ? b += "local" == this.options.environment ? "/shared/js/searchbox/plugins/{name}.js" : "/public/js/searchbox/plugins/{name}.min.js" : "trk" == a && (b += "/search_log.php"), b
        },
        init_ref: function() {
            var b = this,
                c = {
                    refid: this.options.refid,
                    jsoncallback: "?"
                };
            a.getJSON(this.config.refdata_url + this.build_query(c), function(a) {
                a.error ? (h("RefData " + a.error), g({
                    t: "event",
                    ec: "searchbox error",
                    ea: b.options.refid + ": " + location.href,
                    el: "RefData Error: " + JSON.stringify(a)
                })) : (b.options.hotel.active = "1" == a.products.hotel.product_on_off, b.options.car.active = "1" == a.products.car.product_on_off, b.options.air.active = "1" == a.products.air.product_on_off, b.options.vp.active = "1" == a.products.vp.product_on_off, b.options.hotel.version = a.products.hotel.version, b.options.car.version = a.products.car.version, b.options.air.version = a.products.air.version, b.options.vp.version = a.products.vp.version, b.options.filter_city = a.filter_city || !1, b.options.filter_country = a.filter_country || !1, b.options.filter_state = a.filter_state || !1, b.options.cname = "//secure.rezserver.com" != b.options.cname ? b.options.cname : ("http" == b.options.protocol || "https" == b.options.protocol ? b.options.protocol + ":" : b.config.protocol) + "//" + (a.domain_mask || b.options.cname).replace(/(^http:\/\/www\.)|(^https:\/\/www\.)|(^http:\/\/)|(^https:\/\/)|(^\/\/)/, ""), b.options.accountid = parseInt(a.accountid), b.options.group_booking = a.group_booking_url && a.group_booking_url.length > 10 ? a.group_booking_url : !1, b.options.allow_group_booking = !("0" != a.group_booking_flag && "1" != a.group_booking_flag), b.options.vcid = a.vcid && "0" != a.vcid ? a.vcid : 28948, b.options.pet_friendly = a.site_options && a.site_options.pet_friendly && "1" == a.site_options.pet_friendly.value || 1 == b.options.pet_friendly ? !0 : !1, "object" == typeof a.site_options && ("object" == typeof a.site_options.force_https_protocol && "1" == a.site_options.force_https_protocol.value && (b.options.protocol = "https", "object" == typeof a.site_options.secure_domain_mask && "1" != a.site_options.secure_domain_mask.value && (b.options.cname = "//secure.rezserver.com"), b.options.cname = b.options.protocol + "://" + b.options.cname.replace(/(^http:\/\/www\.)|(^https:\/\/www\.)|(^http:\/\/)|(^https:\/\/)|(^\/\/)/, "")), "object" == typeof a.site_options.aaa_redirect && "1" === a.site_options.aaa_redirect.value && (b.plugins.aaa_redirect = !0)), "" == b.options.cname && (b.options.cname = b.init_getURL("cname")), b.init_bind("hotel"), b.init_bind("car"), b.init_bind("air"), b.init_bind("vp"))
            }).fail(function() {
                h("Could not retrieve RefData."), g({
                    t: "event",
                    ec: "searchbox error",
                    ea: b.options.refid + ": " + location.href,
                    el: "Could not retrieve RefData."
                })
            })
        },
        init_bind: function(b) {
            var c, e, f, i, j = this.options[b];
            if (j.enabled && j.active) {
                for (c = j.elements.search.replace(/\s+/g, "").split(","), e = j.elements.chk_in.replace(/\s+/g, "").split(","), f = j.elements.chk_out.replace(/\s+/g, "").split(","), /^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}/.test(a("input[name=" + j.names.check_in + "]", j.$parent).val()) !== !1 && (j.currentDate = new Date(a("input[name=" + j.names.check_in + "]", j.$parent).val()), j.chk_in = a("input[name=" + j.names.check_in + "]", j.$parent).val()), j.names.check_in1 && /^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}/.test(a("input[name=" + j.names.check_in1 + "]", j.$parent).val()) !== !1 && (j.currentDate = new Date(a("input[name=" + j.names.check_in1 + "]", j.$parent).val()), j.chk_in = a("input[name=" + j.names.check_in1 + "]", j.$parent).val()), /^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}/.test(a("input[name=" + j.names.check_out + "]", j.$parent).val()) !== !1 && (j.currentDate = new Date(a("input[name=" + j.names.check_out + "]", j.$parent).val()), j.chk_out = a("input[name=" + j.names.check_out + "]", j.$parent).val()), !j.calendar.today || j.chk_in || j.chk_out || this.auto_set(b), i = 0; i < c.length; i++) a(c[i], j.$parent).on("click.searchbox", {
                    _self: this,
                    product: b
                }, function(b) {
                    return b.data._self.check_form(a(this), b.data.product)
                });
                for (i = 0; i < e.length; i++) a(e[i], j.$parent).on("click.searchbox", {
                    _self: this,
                    product: b
                }, function(b) {
                    b.data._self.make_cal(a(this), b.data.product, "chk_in")
                });
                for (i = 0; i < f.length; i++) a(f[i], j.$parent).on("click.searchbox", {
                    _self: this,
                    product: b
                }, function(b) {
                    b.data._self.make_cal(a(this), b.data.product, "chk_out")
                });
                if (j.elements.geo && a(j.elements.geo, j.$parent).on("click.searchbox", {
                        _self: this,
                        product: b
                    }, function(b) {
                        b.data._self.get_location(a("input[name=" + ("hotel" == b.data.product ? "query" : "car" == b.data.product ? "rs_pu_city" : "rs_o_city") + "]", b.data._self.options[b.data.product].$parent), b.data.product, a(this))
                    }), (j.autosuggest.enabled || j.autocomplete) && (a(j.elements["hotel" == b ? "autosuggest" : "from"], j.$parent).prop("onclick", null).on("keyup.searchbox", {
                        _self: this,
                        product: b
                    }, function(b) {
                        b.data._self.autosuggest(a(this), b)
                    }).on("keydown.searchbox", {
                        _self: this,
                        product: b
                    }, function(b) {
                        b.data._self.autosuggest(a(this), b)
                    }).on("click.searchbox", {
                        _self: this,
                        product: b
                    }, function(b) {
                        var c, e = a(this),
                            f = this.name.match(/rs_o_city([2-5])/i);
                        b.data._self.options[b.data.product].autosuggest.geolocation ? (b.stopPropagation(), b.data._self.create_geo(e, b.data.product)) : b.data._self.create_recent(e, b.data.product), e.select(), b.data._self.config.mobile && b.data._self.options[b.data.product].autosuggest.mobile_scroll && a("body, html").animate({
                            scrollTop: a(this).offset().top
                        }, 300, function() {
                            e.val() == (b.data._self.options[b.data.product].autosuggest.default_label || b.data._self.options[b.data.product].autosuggest.from_default_label) && e.val("").blur(function() {
                                "" == a(this).val() && a(this).val(b.data._self.options[b.data.product].autosuggest.default_label || b.data._self.options[b.data.product].autosuggest.from_default_label).change()
                            })
                        }), b.data._self.close_calendar(a(".rs_cal", b.data._self.options[b.data.product].$parent), b.data.product), f && "air" === b.data.product && (f = f[1], c = "d" + (f - 1) + "_set", (0 == b.data._self.options.air[c] || b.data._self.options.air[c] == d) && (b.data._self.options.air[c] = !0, ("" == a(this).val() || a(this).val() == b.data._self.options.air.autosuggest.from_default_label) && a(this).val(a(b.data._self.options.air.elements.multi_dest + " input[name=rs_d_city" + (f - 1) + "]", b.data._self.options.air.$parent).val()).change().select(), "" != a(".rs_d_aircode" + (f - 1), b.data._self.options.air.elements.multi_dest).val() && b.data._self.check_add(a(".rs_o" + f + "_aircode", b.data._self.options.air.elements.multi_dest), a(b.data._self.options.air.elements.multi_dest, b.data._self.options.air.$parent), '<input name="rs_o_aircode' + f + '" class="rs_o' + f + '_aircode" type="hidden" />', a(".rs_d" + (f - 1) + "_aircode", b.data._self.options.air.elements.multi_dest).val()))), b.stopImmediatePropagation()
                    }), ("car" == b || "air" == b || "vp" == b) && a(j.elements.to, j.$parent).prop("onclick", null).on("keyup.searchbox", {
                        _self: this,
                        product: b
                    }, function(b) {
                        b.data._self.autosuggest(a(this), b)
                    }).on("keydown.searchbox", {
                        _self: this,
                        product: b
                    }, function(b) {
                        b.data._self.autosuggest(a(this), b)
                    }).on("click.searchbox", {
                        _self: this,
                        product: b
                    }, function(b) {
                        var c = a(this);
                        b.data._self.options[b.data.product].autosuggest.geolocation ? (b.stopPropagation(), b.data._self.create_geo(a(this), b.data.product)) : b.data._self.create_recent(a(this), b.data.product), c.select(), b.data._self.config.mobile && b.data._self.options[b.data.product].autosuggest.mobile_scroll && a("body, html").animate({
                            scrollTop: a(this).offset().top
                        }, 300, function() {
                            c.val() == b.data._self.options[b.data.product].autosuggest.to_default_label && c.val("").blur(function() {
                                "" == a(this).val() && a(this).val(b.data._self.options[b.data.product].autosuggest.to_default_label).change()
                            })
                        }), b.data._self.close_calendar(a(".rs_cal", b.data._self.options[b.data.product].$parent), b.data.product), b.stopImmediatePropagation()
                    })), "hotel" != b && "vp" != b || !j.generate_rooms || this.generate_rooms(b), "hotel" != b && "air" != b && "vp" != b || !j.generate_guests || this.generate_guests(b), "hotel" == b && 0 !== a("select[name=" + j.names.rooms + "]", j.$parent).length && 0 !== a("select[name=" + j.names.adults + "]", j.$parent).length && a(j.elements.rooms, j.$parent).on("change.searchbox", {
                        _self: this,
                        product: b
                    }, function(a) {
                        a.data._self.generate_guests(a.data.product)
                    }), "car" == b && j.calendar.generate_time && (this.generate_time(b, "time_in"), this.generate_time(b, "time_out")), "air" == b)
                    for (i = 1; 5 >= i; i++) j.elements["month_in" + i] && j.elements["day_in" + i] && (this.generate_months(b, "month_in" + i), this.generate_months(b, "month_out" + i));
                "vp" == b && (a(j.elements.children, j.$parent).on("change.searchbox", {
                    _self: this
                }, function(b) {
                    b.data._self.generate_children(a(this), "vp")
                }), a("select", j.elements.children_ages).on("change.searchbox", {
                    _self: this
                }, function(a) {
                    a.data._self.generate_infants("vp")
                }), j.elements.geo && a(j.elements.geo, j.$parent).on("click.searchbox", {
                    _self: this,
                    product: b
                }, function() {
                    this.get_location(a("input[name=query_from]", j.$parent), b, a("<div></div>"))
                })), j.elements.month_in && j.elements.month_out && j.elements.day_in && j.elements.day_out && (this.generate_months(b, "month_in"), this.generate_months(b, "month_out"))
            } else j.enabled && !j.active && (h('The "' + b + '" product is not enabled. It must be enabled before you can use it in the searchbox.'), g({
                t: "event",
                ec: "searchbox error",
                ea: this.options.refid + ": " + location.href,
                el: 'The "' + b + '" product is not enabled.'
            }))
        },
        build_query: function(a) {
            var b, c = "";
            for (b in a) c += "&" + b + "=" + a[b];
            return "?" + c.substr(1)
        },
        get_location: function(c, d, e) {
            var f = this;
            "geolocation" in navigator ? (this.reset_form(c, d), c.val(a.i18n._t("Please wait...")).change().prop("disabled", !0).css("background", "#fff url(" + ("file:" == b.location.protocol ? "http:" : "") + "'//media.rezserver.com/img/loader.gif') no-repeat right 10px center"), navigator.geolocation.getCurrentPosition(function(a) {
                f.set_location(c, d, a, e)
            }, function(a) {
                f.error_location(c, d, a)
            }, {
                enableHighAccuracy: !0,
                timeout: 6e4
            })) : (g({
                t: "event",
                ec: "searchbox error",
                ea: this.options.refid + ": " + location.href,
                el: "Geo location not supported or not allowed."
            }), this.check_alert(d, "Sorry, your web browser does not support this feature. Please enter a location instead."))
        },
        check_add: function(b, c, d, e, f) {
            var i;
            return b && c && d ? (f || (f = "append"), 0 === b.length ? (i = a(d), e && i.val(e), "append" == f ? c.append(i) : "after" == f && c.after(i)) : e && b.val(e), !0) : (h("Missing required variable for check_add()."), g({
                t: "event",
                ec: "searchbox error",
                ea: location.href,
                el: "Missing required variable for check_add()."
            }), !1)
        },
        check_remove: function(a, b) {
            return a ? (0 !== a.length && (b ? a.val("") : a.remove()), !0) : (h("Missing required variable for check_remove()."), g({
                t: "event",
                ec: "searchbox error",
                ea: location.href,
                el: "Missing required variable for check_remove()."
            }), !1)
        },
        reset_form: function(b, c) {
            var d, e = this.options[c];
            if ("hotel" == c) this.check_remove(a(".rs_cityid", e.$parent)), this.check_remove(a(".rs_airid", e.$parent)), this.check_remove(a(".rs_regionid", e.$parent)), this.check_remove(a(".rs_poiid", e.$parent)), this.check_remove(a(".rs_hotelid", e.$parent)), this.check_remove(a(".rs_latitude", e.$parent)), this.check_remove(a(".rs_longitude", e.$parent));
            else if ("car" == c) b.hasClass(e.elements.from.substr(1)) || b[0].id == e.elements.from.substr(1) ? (this.check_remove(a(".rs_pu_cityid", e.$parent)), this.check_remove(a(".rs_pu_airport", e.$parent)), this.check_remove(a(".rs_pickup_counter", e.$parent))) : (b.hasClass(e.elements.to.substr(1)) || b[0].id == e.elements.to.substr(1)) && (this.check_remove(a(".rs_do_cityid", e.$parent)), this.check_remove(a(".rs_do_airport", e.$parent)));
            else if ("air" == c)
                for (d = 0; 5 >= d; d++) b.attr("name") == "rs_o_city" + (0 == d ? "" : d) ? this.check_remove(a(".rs_o" + (0 == d ? "" : d) + "_aircode", e.$parent)) : b.attr("name") == "rs_d_city" + (0 == d ? "" : d) && this.check_remove(a(".rs_d" + (0 == d ? "" : d) + "_aircode", e.$parent));
            else "vp" == c && (b.hasClass(e.elements.from.substr(1)) || b[0] && b[0].id == e.elements.from.substr(1) ? (this.check_remove(a(".rs_o_cityid", e.$parent)), this.check_remove(a(".rs_o_aircode", e.$parent))) : (b.hasClass(e.elements.to.substr(1)) || b[0] && b[0].id == e.elements.to.substr(1)) && (this.check_remove(a(".rs_d_cityid", e.$parent)), this.check_remove(a(".rs_d_aircode", e.$parent))))
        },
        kill_request: function(a) {
            var b = this.options[a];
            null != b.request && (b.request.abort(), b.request = null)
        },
        check_alert: function(b, c, d) {
            var e = this.options[b];
            e.ignore_alerts || alert(a.i18n._t(c)), d || g({
                t: "event",
                ec: "searchbox validation error",
                ea: this.options.refid + ": " + location.href,
                el: b + ": " + c
            })
        },
        string_to_date: function(a, b) {
            var c = a.split("/"),
                b = b ? b.split("/") : !1;
            return "mm" == c[0] || null == c[0] || "" == c[0] ? this.dates.today : b ? new Date("[Y]" == b[0] ? c[0] : "[Y]" == b[1] ? c[1] : c[2], ("[m]" == b[0] ? c[0] : "[m]" == b[1] ? c[1] : c[2]) - 1, "[d]" == b[0] ? c[0] : "[d]" == b[1] ? c[1] : c[2]) : new Date(c[2], c[0] - 1, c[1])
        },
        do_pop: function(a, c) {
            if (this.check_pop() && 1e3 == parseInt(this.options.accountid) && -1 === [2999, 1420, 1001, 1385, 2060].indexOf(parseInt(this.options.refid)) && this.options.vcid && "-1" != this.options.vcid) {
                var d, e = "http://www.hotelsbycity.com/hotels/pop_up.php?key=" + a + "&vcid=" + this.options.vcid + "&skip_tig=true" + c,
                    f = "resizable=1,scrollbars=1,menubar=1,location=1,toolbar=1,titlebar=1,width=680,height=630, top=50, left=50";
                d = b.open(e, "HBCPop", f), d.blur()
            }
        },
        check_pop: function() {
            var a = this.read_cookie("lastHBCPop");
            return this.config.mobile ? !1 : null == a ? (this.create_cookie("lastHBCPop", this.dates.now.getTime(), 0), !0) : parseInt(a) + 864e3 > parseInt(this.dates.now.getTime()) ? !1 : (this.create_cookie("lastHBCPop", this.dates.now.getTime(), 0), !0)
        },
        create_cookie: function(a, b, d) {
            var e, f = "";
            d && (e = new Date, e.setTime(e.getTime() + 24 * d * 60 * 60 * 1e3), f = "; expires=" + e.toGMTString()), c.cookie = a + "=" + b + f + "; path=/"
        },
        read_cookie: function(a) {
            var b, d, e = a + "=",
                f = c.cookie.split(";");
            for (b = 0; b < f.length; b++) {
                for (d = f[b];
                    " " == d.charAt(0);) d = d.substring(1, d.length);
                if (0 == d.indexOf(e)) return d.substring(e.length, d.length)
            }
            return null
        },
        generate_rooms: function(b) {
            var c, d, e = "",
                f = this.options.allow_group_booking ? 10 : 5,
                g = this.options[b];
            if (g.select_name)
                for (c = 1; f > c; c++) d = 9 == c ? "+" : "", e += '<option value="' + c + '"' + (c == g.default_rooms ? " selected" : "") + ">" + a.i18n._n("1 Room", "%s Rooms", {
                    count: c,
                    sprintf: [c + d]
                }) + "</option>";
            else
                for (c = 1; f > c; c++) e += '<option value="' + c + '"' + (c == g.default_rooms ? " selected" : "") + ">" + c + (9 == c ? "+" : "") + "</option>";
            a(g.elements.rooms, g.$parent).append(e).trigger("change")
        },
        generate_guests: function(b) {
            var c, d = this.options[b],
                e = a("select[name=" + d.names.rooms + "]", d.$parent).val(),
                f = 4 * e,
                g = a("select[name=" + d.names.adults + "]", d.$parent),
                h = a("select[name=" + d.names.children + "]", d.$parent),
                i = "";
            if ("air" == b ? (e = 0, f = 8) : "vp" == b && (e = 1, f = 8), 0 !== g.length && ("hotel" === b || "air" === b || "vp" === b)) {
                if (d.select_name)
                    for (c = parseInt(e); f >= c; c++) i += '<option value="' + c + '"' + (c === d.default_guests ? " selected" : "") + ">" + (0 === h.length ? a.i18n._n("1 Guest", "%d Guests", {
                        count: c
                    }) : a.i18n._n("1 Adult", "%d Adults", {
                        count: c
                    })) + "</option>";
                else
                    for (c = parseInt(e); f >= c; c++) i += '<option value="' + c + '"' + (c === d.default_guests ? " selected" : "") + ">" + c + "</option>";
                g.empty().append(i).trigger("change")
            }
            if (0 !== h.length) {
                if (i = "", "air" == b && (e = 1), d.select_name)
                    for (c = 0; f - e >= c; c++) i += '<option value="' + c + '"' + (c === d.default_children ? " selected" : "") + ">" + a.i18n._n("1 Child", "%d Children", {
                        count: c
                    }) + "</option>";
                else
                    for (c = 0; f - e >= c; c++) i += '<option value="' + c + '"' + (c === d.default_children ? " selected" : "") + ">" + c + "</option>";
                h.empty().append(i).trigger("change")
            }
        },
        generate_time: function(b, c) {
            var d = this.options[b],
                e = "",
                f = 0,
                g = "00",
                h = null;
            for ("0" == d.calendar.pickup_time.charAt(0) && (d.calendar.pickup_time = d.calendar.pickup_time.substr(1)), "0" == d.calendar.dropoff_time.charAt(0) && (d.calendar.dropoff_time = d.calendar.dropoff_time.substr(1)); 24 > f;) h = new Date, h.setHours(f, g, 0, 0), e += "time_in" == c && d.calendar.pickup_time == f + ":" + g || "time_out" == c && d.calendar.dropoff_time == f + ":" + g ? '<option value="' + f + ":" + g + '" selected="selected">' : '<option value="' + f + ":" + g + '">', e += this.format_date(b, h, d.calendar.time_format), e += "</option>", "00" == g ? g = "30" : (g = "00", f++);
            a(d.elements[c], d.$parent).append(e).trigger("change").on("change.searchbox", {
                _self: this,
                product: b,
                time: c
            }, function(b) {
                b.data._self.set_car_time(a(this), b.data.product, b.data.time)
            })
        },
        generate_children: function(b, c) {
            var d, e = this.options[c],
                f = b.val(),
                g = "<span>" + a.i18n._t("Children's Ages:") + "</span>",
                h = "<select class='childrens_ages'>";
            if (a(e.elements.children_ages, e.$parent).html(""), a(e.elements.infant_seating, e.$parent).html(""), f > 0) {
                for (d = 17; d > 0; d--) h += 1 == d ? '<option value="0"><2</option>' : '<option value="' + d + '">' + d + "</option>";
                for (h += "</select>", d = 1; f >= d; d++) g += h;
                a(e.elements.children_ages, e.$parent).append(g), a("select", e.elements.children_ages).on("change.searchbox", {
                    _self: this
                }, function(a) {
                    a.data._self.generate_infants("vp")
                })
            }
        },
        generate_infants: function(b) {
            var c, d = this.options[b],
                e = 0,
                f = "",
                g = ["th", "st", "nd", "rd"];
            if (a("select", d.elements.children_ages).each(function() {
                    0 == a(this).val() && e++
                }), a(d.elements.infant_seating, d.$parent).html(""), e > 0) {
                for (this.check_add(a(d.elements.infant_seating, d.$parent), a(d.elements.children_ages), "<div id=" + d.elements.infant_seating + "></div>"), c = 1; e >= c; c++) f += '<div class="infant_seat_container">', f += "<span>" + (c + (g[(c % 100 - 20) % 10] || g[c % 100] || g[0])) + " Infant Seating:</span>", f += '<div class="infant_seat">', f += '<div class="infant_seat_radio_container">', f += '<input type="radio" id="rs_infant_seat_lap' + c + '" name="rs_infant' + c + '" value="lap" />', f += '<label for="rs_infant_seat_lap' + c + '">' + a.i18n._t("Lap Infant") + "</label>", f += "</div>", f += '<div class="infant_seat_radio_container">', f += '<input type="radio" id="rs_infant_seat' + c + '" name="rs_infant' + c + '" value="seat" checked />', f += '<label for="rs_infant_seat' + c + '">' + a.i18n._t("Purchase Seat") + "</label>", f += "</div>", f += "</div>", f += "</div>";
                a(d.elements.infant_seating, d.$parent).html(f)
            }
        },
        generate_months: function(b, c) {
            var d = this.options[b],
                e = "<option>" + a.i18n._t("Month") + '</option><optgroup disabled="disabled" style="margin-top: 1em;"></optgroup>',
                f = (this.dates.today.getFullYear(), new Date(this.dates.today.getFullYear(), this.dates.today.getMonth(), 1)),
                g = !1;
            for (("month_out" == c || "month_in2" == c || "month_in3" == c || "month_in4" == c || "month_in5" == c) && (f = new Date(d.currentDate.getFullYear(), d.currentDate.getMonth(), 1)), !d.calendar.today || "month_in" != c && "month_out" != c && "month_in1" != c || (g = !0); f <= this.dates.max_date;) e += "<option " + (g ? 'selected="selected" ' : "") + 'data-month="' + f.getMonth() + '" data-year="' + f.getFullYear() + '" value="' + f.getMonth() + '">' + this.dates.months[f.getMonth()] + "</option>", f.setMonth(f.getMonth() + 1), g = !1;
            this.generate_days(!1, b, c), a(d.elements[c], d.$parent).html(e), !d.calendar.today || "month_in" != c && "month_out" != c && "month_in1" != c || this.generate_days(a(d.elements[c], d.$parent), b, c), a(d.elements[c], d.$parent).on("change.searchbox", {
                _self: this,
                product: b,
                cal: c
            }, function(b) {
                if (!a("option:selected", a(this)).data("year") && !a("option:selected", a(this)).data("month")) {
                    var e = b.data.cal.replace(/([^0-9])/g, "");
                    b.data._self.check_remove(a("input[name=" + ("month_in" == c || "" != e ? d.names.check_in : d.names.check_out) + e + "]", d.$parent), !0)
                }
                b.data._self.generate_days(a("option:selected", a(this)), b.data.product, b.data.cal)
            })
        },
        generate_days: function(b, c, e, f) {
            var g, h, i, j = this.options[c],
                k = "",
                l = e.replace(/([^0-9])/g, ""),
                e = "month_in" == e ? "day_in" : "month_out" == e ? "day_out" : -1 !== e.indexOf("month_in") ? "day_in" + l : e;
            if (!b || !b.data("year") && !b.data("month")) return k += "<option>" + a.i18n._t("Day") + "</option>", a(j.elements[e], j.$parent).html(k), !0;
            for (i = new Date(b.data("year"), b.data("month"), 1), g = 1; g <= this.dates.days_in_months[b.data("month")]; g++) i.setDate(g), h = new Date("day_in2" == e ? j.chk_in1 || j.currentDate : "day_in3" == e ? j.chk_in2 || j.chk_in1 || j.currentDate : "day_in4" == e ? j.chk_in3 || j.chk_in2 || j.chk_in1 || j.currentDate : "day_in5" == e ? j.chk_in4 || j.chk_in3 || j.chk_in2 || j.chk_in1 || j.currentDate : j.chk_in), "day_in" == e || "day_in1" == e || ("day_in2" == e || "day_in3" == e || "day_in4" == e || "day_in5" == e) && j.calendar.allow_same && h && i > h ? i <= this.dates.max_date && i >= this.dates.today && (k += '<option data-day="' + g + '">' + g + "</option>") : (i <= this.dates.max_date && i > h || i <= this.dates.max_date && i > this.dates.today && j.chk_in == d && "day_out" == e || j.calendar.allow_same && i <= this.dates.max_date && i >= h) && (k += g == h.getDate() + 1 && j.calendar.next_day && h.getMonth() == i.getMonth() ? '<option data-day="' + g + '" selected="selected">' + g + "</option>" : '<option data-day="' + g + '">' + g + "</option>");

            a(j.elements[e], j.$parent).html(k), f && 0 != f || ("air" == c ? this.set_date(a(j.elements[e], a("" != l ? b.parents().is(j.elements.multi_dest) ? j.elements.multi_dest : j.elements.one_way : j.elements.round_trip)), c, e, !0) : this.set_date(a(j.elements[e], j.$parent), c, e, !0)), a(j.elements[e], j.$parent).on("change.searchbox", {
                _self: this
            }, function(b) {
                b.data._self.set_date(a(this), c, e, !0)
            })
        },
        autosuggest: function(b, c) {
            var d = c.data.product,
                e = this.options[d],
                f = e.query = b.val().replace(/([^0-9a-zA-Z\s\,\.\'\(\\&\#\-\_\/)])/g, "");
            if (16 != c.keyCode && this.kill_request(d), "keydown" == c.type) switch (c.keyCode) {
                case 13:
                    if (0 == a(".rs_suggest", e.$parent).is(":visible")) return this.check_form(b, d), !0;
                    c.preventDefault(), this.reset_form(b, d), this.save_selection(b, a(".rs_suggest .rs_suggest_active", e.$parent), d), a(".rs_suggest", e.$parent).hide();
                    break;
                case 9:
                    1 == a(".rs_suggest", e.$parent).is(":visible") && (c.preventDefault(), this.move_selection("down", d));
                    break;
                case 8:
                    f.length <= 3 && this.reset_form(b, d);
                    break;
                case 38:
                    c.preventDefault();
                    break;
                case 40:
                    c.preventDefault()
            } else if ("keyup" == c.type) switch (c.keyCode) {
                case 13:
                    c.preventDefault();
                    break;
                case 38:
                    c.preventDefault(), this.move_selection("up", d);
                    break;
                case 40:
                    c.preventDefault(), this.move_selection("down", d);
                    break;
                case 27:
                    this.reset_form(b, d);
                    break;
                case 9:
                    c.preventDefault();
                    break;
                case 16:
                    c.preventDefault();
                    break;
                default:
                    f.length >= 3 && (c.keyCode >= 65 && c.keyCode <= 90 || c.keyCode >= 48 && c.keyCode <= 57 || c.keyCode >= 96 && c.keyCode <= 105 || 8 == c.keyCode || 32 == c.keyCode || /(Android.[0-9]*)+.*(Chrome\/[0-9]*)/.test(this.ua)) ? this.query_solr(b, d, f) : f.length < 3 && this.reset_form(b, d)
            }
        },
        query_solr: function(e, f, i, j) {
            var k = this.options[f],
                l = null,
                m = this;
            j = j || !1, e.css("background", "#fff url(" + ("file:" == b.location.protocol ? "http:" : "") + "'//media.rezserver.com/img/loader.gif') no-repeat right 10px center"), l = {
                format: "jsonp",
                product: f,
                refid: this.options.refid,
                query: encodeURIComponent(i),
                cities: k.autosuggest.cities || "false",
                airports: k.autosuggest.airports || "false",
                hotels: k.autosuggest.hotels || "false",
                regions: k.autosuggest.regions || "false",
                pois: k.autosuggest.pois || "false",
                plugin: k.autosuggest.plugin || "",
                pet_friendly: this.options.pet_friendly || "false",
                path: f,
                jsoncallback: "?"
            }, g({
                t: "event",
                ec: "searchbox query",
                ea: this.options.refid + " - " + f + ": " + location.href,
                el: i
            }), k.search_timout && (clearTimeout(k.search_timout), k.search_timout = null), k.search_timout = setTimeout(function() {
                m.s_add(f, e.prop("name"), i), clearTimeout(k.search_timout), k.search_timout = null
            }, 2e3), k.request = a.getJSON(this.init_getURL("autosuggest") + this.build_query(l), function(b) {
                var l, n, o = 0;
                if (b = b.getSolr, b.error && 1208 == b.error.status_code) return g({
                    t: "event",
                    ec: "searchbox error",
                    ea: m.options.refid + ": " + location.href,
                    el: JSON.stringify(b)
                }), g({
                    t: "timing",
                    utc: "searchbox getSolr",
                    utv: "load",
                    utl: m.options.refid + ": " + location.href,
                    utt: Math.round(1e3 * b.time)
                }), h("No results."), !1;
                if (b.results && 500 == b.results.status_code && !j) g({
                    t: "event",
                    ec: "searchbox error",
                    ea: m.options.refid + ": " + location.href,
                    el: "500 error - retrying"
                }), h("500 error - retrying"), m.query_solr(e, f, i, !0);
                else {
                    if (!b.results || !b.results.result) return g({
                        t: "event",
                        ec: "searchbox error",
                        ea: m.options.refid + ": " + location.href,
                        el: JSON.stringify(b)
                    }), !1;
                    if (b = b.results, g({
                            t: "timing",
                            utc: "searchbox getSolr",
                            utv: "load",
                            utl: m.options.refid + ": " + location.href,
                            utt: Math.round(1e3 * b.time)
                        }), j && (g({
                            t: "event",
                            ec: "searchbox error",
                            ea: m.options.refid + ": " + location.href,
                            el: "500 error - retrying was successful"
                        }), h("500 error - retrying was successful")), m.plugins.aaa_redirect && "hotel" === f && b.query.match(/hawaii/i)) {
                        n = {
                            city_0: {
                                city: "Hawaii",
                                cityid_ppn: "899999999",
                                country: "United States",
                                country_code: "US",
                                state: "Hawaii",
                                state_code: "HI",
                                type: "city",
                                hotel_count: 0,
                                pet_count: 0
                            }
                        };
                        for (l in b.result.city) n["city_" + (o + 1)] = b.result.city["city_" + o], o++;
                        b.result.city = n
                    }
                    if (k.autosuggest.generate && m.generate_autosuggest(e, b, f), k.autosuggest.callback) {
                        var p = k.autosuggest.callback;
                        "function" == typeof p ? p(e, b, m, f) : h('Callback function "' + k.autosuggest.callback + '" for autosuggest does not exist.')
                    }
                    if ("" != k.autosuggest.first && k.autocomplete) {
                        if (i == k.previous) return !1;
                        k.previous = i, 8 != event.keyCode && k.autosuggest.first !== d && (a(e, k.$parent).val(i + k.autosuggest.first.substring(k.previous.length)), m.create_selection(e, f))
                    }
                    k.clickListen || (k.clickListen = !0, a(c).click({
                        options: k
                    }, function(b) {
                        0 == a(b.target).parents().is(".rs_suggest") && a(".rs_suggest", b.data.options.$parent).hide()
                    })), a(".rs_suggest li", k.$parent).each(function() {
                        a(this).on("mouseover.searchbox", {
                            options: k
                        }, function(b) {
                            a(".rs_suggest li", b.data.options.$parent).each(function() {
                                a(this).removeClass("rs_suggest_active")
                            }), a(this).addClass("rs_suggest_active")
                        }).on("click.searchbox", {
                            $el: e,
                            _self: m,
                            product: f
                        }, function(a) {
                            m.save_selection(a.data.$el, this, a.data.product)
                        })
                    })
                }
            }).complete(function() {
                e.css("background", "#fff")
            }).fail(function(a, b, c) {
                "abort" != b && g({
                    t: "event",
                    ec: "searchbox error",
                    ea: m.options.refid + ": " + location.href,
                    el: b + ", " + c
                })
            })
        },
        create_geo: function(b, d) {
            var e = "",
                f = this.options[d],
                g = this;
            return b && 0 == b.next().hasClass("rs_suggest") && 0 !== a(".rs_suggest", f.$parent).length && a(".rs_suggest", f.$parent).remove(), (0 === a(".rs_suggest_geo", f.$parent).length && b || !b) && (e += '<div class="rs_suggest_geo">', e += "<ul>", e += b ? '<li data-geo="true" class="rs_suggest_active">' : '<li data-geo="true">', e += '<div class="rs_suggest_left">', e += f.autosuggest.icon_location, e += a.i18n._t("Use my Location"), e += "</div>", e += "</li>", b && (e += this.create_recent(!1, d)), e += "</ul>", e += "</div>"), b ? (this.check_add(a(".rs_suggest", f.$parent), a(b, f.$parent), '<div class="rs_suggest"/>', !1, "after"), f.clickListen || (f.clickListen = !0, a(c).click({
                _self: this,
                product: d,
                $el: b
            }, function(c) {
                0 != a(c.target).parents().is(".rs_suggest") || a(c.target).is(b) || a(".rs_suggest", c.data._self.options[c.data.product].$parent).hide()
            })), e && 0 === a(".rs_suggest .rs_suggest_absolute", f.$parent).length ? (e = '<div class="rs_suggest_absolute">' + e + "</div>", a(".rs_suggest", f.$parent).append(e)) : e && a(".rs_suggest .rs_suggest_absolute", f.$parent).prepend(e), a(".rs_suggest", f.$parent).show(), a(".rs_suggest li", f.$parent).each(function() {
                a(this).on("mouseover.searchbox", {
                    options: f
                }, function(b) {
                    a(".rs_suggest li", b.data.options.$parent).each(function() {
                        a(this).removeClass("rs_suggest_active")
                    }), a(this).addClass("rs_suggest_active")
                }).on("click.searchbox", {
                    $el: b,
                    _self: g,
                    product: d
                }, function(a) {
                    g.save_selection(a.data.$el, this, a.data.product)
                })
            }), void 0) : e
        },
        create_recent: function(b, d) {
            var e, f = this.options[d],
                g = "rsRecentSearch_" + d,
                h = this.read_cookie(g) || !1,
                i = "",
                j = "",
                k = this;
            return h ? (b && "" === b.val() && a(".rs_suggest", f.$parent).remove(), b && 0 == b.next().hasClass("rs_suggest") && 0 !== a(".rs_suggest", f.$parent).length && a(".rs_suggest", f.$parent).remove(), (0 === a(".rs_suggest_recent", f.$parent).length && b || !b) && (h = JSON.parse(h), i += '<div class="rs_suggest_recent">', i += "<ul>", h.forEach(function(a) {
                e = !1, i += "<li";
                for (var b in a) i += " data-" + b + '="' + a[b] + '"', "airid" === b ? (j = f.autosuggest.icon_airports, e = a[b]) : "cityid" === b ? j = f.autosuggest.icon_cities : "regionid" === b && (j = f.autosuggest.icon_regions);
                i += ">", i += '<div class="rs_suggest_left">', i += j, i += a.name, i += "</div>", e && (i += '<div class="rs_suggest_right">' + e + "</div>"), i += "</li>"
            }), i += "</ul>", i += "</div>"), b ? (this.check_add(a(".rs_suggest", f.$parent), a(b, f.$parent), '<div class="rs_suggest"/>', !1, "after"), f.clickListen || (f.clickListen = !0, a(c).click({
                _self: this,
                product: d,
                $el: b
            }, function(c) {
                0 != a(c.target).parents().is(".rs_suggest") || a(c.target).is(b) || a(".rs_suggest", c.data._self.options[c.data.product].$parent).hide()
            })), i && 0 === a(".rs_suggest .rs_suggest_absolute", f.$parent).length && (i = '<div class="rs_suggest_absolute">' + i + "</div>", a(".rs_suggest", f.$parent).append(i)), a(".rs_suggest", f.$parent).show(), a(".rs_suggest li", f.$parent).each(function() {
                a(this).on("mouseover.searchbox", {
                    options: f
                }, function(b) {
                    a(".rs_suggest li", b.data.options.$parent).each(function() {
                        a(this).removeClass("rs_suggest_active")
                    }), a(this).addClass("rs_suggest_active")
                }).on("click.searchbox", {
                    $el: b,
                    _self: k,
                    product: d
                }, function(a) {
                    k.reset_form(a.data.$el, a.data.product), k.save_selection(a.data.$el, this, a.data.product)
                })
            }), void 0) : i) : ""
        },
        set_location: function(b, c, d, e) {
            var f, h, i = this,
                j = this.options[c],
                k = {
                    format: "jsonp",
                    product: c,
                    refid: this.options.refid,
                    latitude: d.coords.latitude,
                    longitude: d.coords.longitude,
                    cities: "true",
                    airports: "true",
                    jsoncallback: "?"
                };
            a.getJSON(this.init_getURL("autosuggest") + this.build_query(k), function(k) {
                k = k.getSolr.error && 1240 == k.getSolr.error.status_code ? !1 : k.getSolr.results.result, f = k && k.city && k.city.city_0["geodist()"] || !1, h = k && k.airport && k.airport.airport_0["geodist()"] || !1, f && (h > f || !h) && 25 > f ? (e.data("cityid", "air" == c && "2" == j.version ? k.city.city_0.cityid_t : k.city.city_0.cityid_ppn), e.data("name", k.city.city_0.city + ", " + k.city.city_0.state_code), i.save_selection(b, e, c), b.prop("disabled", !1).css("background", "#fff"), g({
                    t: "event",
                    ec: "searchbox setGeo",
                    ea: i.options.refid + ": " + location.href,
                    el: {
                        product: c,
                        type: "city",
                        name: k.city.city_0.city,
                        id: "air" == c && "2" == j.version ? k.city.city_0.cityid_t : k.city.city_0.cityid_ppn,
                        distance: f
                    }
                })) : h && (f > h || !f) && 25 > h ? (e.data("airid", k.airport.airport_0.iata), e.data("name", k.airport.airport_0.airport), i.save_selection(b, e, c), b.prop("disabled", !1).css("background", "#fff"), g({
                    t: "event",
                    ec: "searchbox setGeo",
                    ea: i.options.refid + ": " + location.href,
                    el: {
                        product: c,
                        type: "airport",
                        name: k.airport.airport_0.airport,
                        iata: k.airport.airport_0.iata,
                        distance: h
                    }
                })) : "hotel" == c ? (i.check_add(a(".rs_latitude", j.$parent), j.$parent, '<input name="' + j.names.latitude + '" class="rs_latitude" type="hidden" />', d.coords.latitude), i.check_add(a(".rs_longitude", j.$parent), j.$parent, '<input name="' + j.names.longitude + '" class="rs_longitude" type="hidden" />', d.coords.longitude), b.val("Your Location").prop("disabled", !1).css("background", "#fff"), g({
                    t: "event",
                    ec: "searchbox setGeo",
                    ea: i.options.refid + ": " + location.href,
                    el: {
                        product: c,
                        type: "geo",
                        lat: d.coords.latitude,
                        lng: d.coords.longitude
                    }
                })) : i.error_location(b, c, {
                    code: 2
                })
            }).error(function() {
                i.error_location(b, c, {
                    code: 2
                })
            })
        },
        error_location: function(b, c, d) {
            alert(1 == d.code ? a.i18n._t("Sorry, you must allow us to get your location to use this feature.") : a.i18n._t("Sorry, we were unable to find your location. Please enter a location instead.")), g({
                t: "event",
                ec: "searchbox error",
                ea: this.options.refid + ": " + location.href,
                el: {
                    msg: "Geo Error: " + (1 == d.code ? "Permission Error" : "Not Found"),
                    product: c
                }
            }), b.val("").prop("disabled", !1).css("background", "#fff")
        },
        generate_autosuggest: function(b, c, d) {
            var e, f, g, h, i, j, k, l, m, n, o, p, q, r = this.options[d],
                s = [],
                t = "",
                u = "";
            0 == b.next().hasClass("rs_suggest") && 0 !== a(".rs_suggest", r.$parent).length && a(".rs_suggest", r.$parent).remove(), this.check_add(a(".rs_suggest", r.$parent), b, '<div class="rs_suggest"/>', !1, "after"), r.autosuggest.first = "", s.start = '<div class="rs_suggest_absolute">', s.cities = "", s.regions = "", s.airports = "", s.pois = "", s.hotels = "", s.end = "";
            for (e in c.result) {
                f = 0, g = "", "airport" == e ? (s.airports += '<div class="rs_suggest_air"><ul>', g = "airports") : "city" == e ? (s.cities += '<div class="rs_suggest_city"><ul>', g = "cities") : "region" == e ? (s.regions += '<div class="rs_suggest_region"><ul>', g = "regions") : "poi" == e ? (s.pois += '<div class="rs_suggest_poi"><ul>', g = "pois") : "hotel" == e && (s.hotels += '<div class="rs_suggest_hotel"><ul>', g = "hotels");
                for (h in c.result[e]) f < r.autosuggest["num_" + g] && (i = c.result[e][h], q = i.airport || i.hotel_name || i.region_name || i.poi_name || i.city || (i.address && i.address.city_name ? i.address.city_name : !1), p = i.iata || i.hotelid_ppn || i.poiid_ppn || i.area_id || ("air" == d && "3" == r.version ? i.cityid_ppn : "air" == d ? i.cityid_t : i.car_city_ppn_id || i.cityid_ppn) || !1, k = i.state_code || (i.address && i.address.state_code ? i.address.state_code : !1), l = i.country_code || (i.address && i.address.country_code ? i.address.country_code : !1), m = i.address && i.address.address_line_one ? i.address.address_line_one : i.address || !1, n = i.country || !1, j = i.city || !1, o = this.options.pet_friendly ? Number(String(i.pet_count)) : Number(String(i.hotel_count) || String(i.rank)) || 0, s[g] += "<li ", "airports" == g ? (s[g] += 'data-airid="' + p + '" data-name="' + ("air" == d && j && p ? j + " (" + p + ")" : q) + '">', s[g] += '<div class="rs_suggest_left">' + r.autosuggest.icon_airports + q + "</div>", s[g] += '<div class="rs_suggest_right">' + p + "</div>", s[g] += '<div class="rs_suggest_full rs_suggest_city_name">' + (j ? j + ", " : "") + (k ? k : l || "") + (k && !j && l ? ", " + l : "") + "</div>") : "hotels" == g ? (s[g] += 'data-hotelid="' + p + '" data-name="' + q + '">', s[g] += '<div class="rs_suggest_full">' + r.autosuggest.icon_hotels + q + "</div>", s[g] += '<div class="rs_suggest_full rs_suggest_city_name">' + (m ? m + ", " : "") + (q ? q + " " : "") + (k ? k + ", " : "") + (l ? l : "") + "</div>") : "pois" == g ? (s[g] += 'data-poiid="' + p + '" data-name="' + q + '">', s[g] += '<div class="rs_suggest_full">' + r.autosuggest.icon_pois + q + "</div>", s[g] += '<div class="rs_suggest_full rs_suggest_city_name">' + (m ? m + ", " : "") + (q ? q + " " : "") + (k ? k + ", " : "") + (n ? n : "") + "</div>") : "regions" == g ? (s[g] += 'data-regionid="' + p + '" data-name="' + q + '">', s[g] += '<div class="rs_suggest_left">' + r.autosuggest.icon_regions + q + "</div>", s[g] += '<div class="rs_suggest_right">Nearby Hotels</div>') : "cities" == g && (s[g] += 'data-cityid="' + p + '" data-name="' + (k ? q + ", " + k : q + ", " + n) + '" data-country="' + n + '" data-country_code="' + l + '">', s[g] += '<div class="rs_suggest_left">' + r.autosuggest.icon_cities + (k ? q + ", " + k : q + ", " + n) + "</div>", s[g] += '<div class="rs_suggest_city_name">' + n + "</div>", s[g] += '<div class="rs_suggest_right">' + ("hotel" == d ? 0 === o ? '<div class="rs_sub_count">Nearby Hotels</div>' : '<div class="rs_suggest_subhead">' + o + ' <div class="rs_sub_count">' + (this.options.pet_friendly ? "Pet Friendly " : "") + "Hotel" + (o > 1 ? "s" : "") + "</div></div>" : "air" == d || "vp" == d ? "All Airports" : l) + " </div>"), s[g] += "</li>"), "" != r.autosuggest.first && r.autosuggest.first || (r.autosuggest.first = k ? q + ", " + k : q + ", " + l), 0 != r.autosuggest.first.toUpperCase().indexOf(r.query.toUpperCase()) && (r.autosuggest.first = ""), f++;
                s[g] += "</ul></div>"
            }
            r.autosuggest.more_options && ("hotel" == d ? u = "/hotels/search/?query=" + r.query : "car" == d ? u = "/car_rentals/did_you_mean/?rs_pu_city=" + r.query : "air" == d ? u = "rs_o_city" == b.attr("name") || "rs_d_city" == b.attr("name") ? "/air/search/?air_search_type=roundtrip&rs_o_city=" + a("input[name=rs_o_city]", r.$parent).val() + "&rs_d_city=" + a("input[name=rs_d_city]", r.$parent).val() : "rs_o_city1" == b.attr("name") || "rs_d_city1" == b.attr("name") ? "/air/search/?air_search_type=oneway&rs_o_city1=" + a("input[name=rs_o_city1]", r.$parent).val() + "&rs_d_city1=" + a("input[name=rs_d_city1]", r.$parent).val() : "/air/search/?air_search_type=multi&rs_o_city1=" + a("input[name=rs_o_city1]", r.$parent).val() + "&rs_d_city1=" + a("input[name=rs_d_city1]", r.$parent).val() + "&rs_o_city2=" + a("input[name=rs_o_city2]", r.$parent).val() + "&rs_d_city2=" + a("input[name=rs_d_city2]", r.$parent).val() + "&rs_o_city3=" + a("input[name=rs_o_city3]", r.$parent).val() + "&rs_d_city3=" + a("input[name=rs_d_city3]", r.$parent).val() + "&rs_o_city4=" + a("input[name=rs_o_city4]", r.$parent).val() + "&rs_d_city4=" + a("input[name=rs_d_city4]", r.$parent).val() + "&rs_o_city5=" + a("input[name=rs_o_city5]", r.$parent).val() + "&rs_d_city5=" + a("input[name=rs_d_city5]", r.$parent).val() : "vp" == d && (u = "/vp/search/?rs_o_city=" + a("input[name=rs_o_city]", r.$parent).val() + "&rs_d_city=" + a("input[name=rs_d_city]", r.$parent).val()), s.end += '<div class="rs_suggest_show"><a href="' + this.options.cname + u + "&refid=" + this.options.refid + '">Show More Options</a></div>'), s.end += "</div>", t += s.start, r.autosuggest.geolocation && (t += this.create_geo(!1, d)), 1 == r.autosuggest.airports_first || 3 == r.query.length ? (t += s.airports, t += s.cities, t += s.regions, t += s.pois, t += s.hotels) : (t += s.cities, t += s.regions, t += s.airports, t += s.pois, t += s.hotels), t += s.end;
            var v = t.match(r.autosuggest.geolocation ? "<li.+?>(<li.+?>)" : "<li.+?>"),
                w = v[0].length,
                x = v[0].slice(0, -1) + ' class="rs_suggest_active">';
            t = t.substr(0, v.index) + x + t.substr(v.index + w), a(".rs_suggest", r.$parent).html(t).show()
        },
        create_selection: function(a, b) {
            var c, d = a.get(0),
                e = this.options[b].previous.length,
                f = this.options[b].autosuggest.first.length;
            d.createTextRange ? (c = d.createTextRange(), c.collapse(!0), c.moveStart("character", e), c.moveEnd("character", f), c.select()) : d.setSelectionRange ? d.setSelectionRange(e, f) : d.selectionStart && (d.selectionStart = e, d.selectionEnd = f), d.focus()
        },
        move_selection: function(b, c) {
            var d = this.options[c],
                e = null,
                f = 0;
            "down" == b ? f = 1 : "up" == b && (f = -1), a(".rs_suggest li", d.$parent).each(function(b) {
                a(this).hasClass("rs_suggest_active") && (a(this).removeClass("rs_suggest_active"), e = b + f, e == a(".rs_suggest li", d.$parent).get().length && (e = 0), b > e && -1 != e && a(".rs_suggest li", d.$parent).eq(e).addClass("rs_suggest_active")), b == e && a(this).addClass("rs_suggest_active")
            }), 0 == e && a(".rs_suggest ul:first-child li:first-child", d.$parent).get(0).className.replace("rs_suggest_active"), -1 == e && a(".rs_suggest li", d.$parent).eq(a(".rs_suggest li", d.$parent).get().length - 1).addClass("rs_suggest_active")
        },
        save_selection: function(b, c, d) {
            var e, f, i, j = this.options[d],
                k = a(c),
                l = 0;
            b.val(k.data("name")).trigger("autosuggest_change").change(), k.data("cityid") ? (f = "city", i = k.data("cityid"), "hotel" == d ? this.check_add(a(".rs_cityid", j.$parent), j.$parent, '<input name="' + j.names.city_id + '" class="rs_cityid" type="hidden" />', k.data("cityid")) : "car" == d ? b.hasClass(j.elements.from.replace(".", "")) || j.elements.from == "#" + b.attr("id") ? this.check_add(a(".rs_pu_cityid", j.$parent), j.$parent, '<input name="' + j.names.rs_pu_cityid + '" class="rs_pu_cityid" type="hidden" />', k.data("cityid")) : (b.hasClass(j.elements.to.replace(".", "")) || j.elements.to == "#" + b.attr("id")) && this.check_add(a(".rs_do_cityid", j.$parent), j.$parent, '<input name="' + j.names.rs_do_cityid + '" class="rs_do_cityid" type="hidden" />', k.data("cityid")) : "air" == d ? b.parents().is(j.elements.round_trip) ? b.is(j.elements.from) ? this.check_add(a(".rs_o_aircode", j.elements.round_trip), a(j.elements.round_trip, j.$parent), '<input name="' + j.names.rs_o_aircode + '" class="rs_o_aircode" type="hidden" />', k.data("cityid")) : b.is(j.elements.to) && this.check_add(a(".rs_d_aircode", j.elements.round_trip), a(j.elements.round_trip, j.$parent), '<input name="' + j.names.rs_d_aircode + '" class="rs_d_aircode" type="hidden" />', k.data("cityid")) : b.parents().is(j.elements.one_way) ? b.is(j.elements.from) ? this.check_add(a(".rs_o1_aircode", j.elements.one_way), a(j.elements.one_way, j.$parent), '<input name="' + j.names.rs_o1_aircode + '" class="rs_o1_aircode" type="hidden" />', k.data("cityid")) : b.is(j.elements.to) && this.check_add(a(".rs_d1_aircode", j.elements.one_way), a(j.elements.one_way, j.$parent), '<input name="' + j.names.rs_d1_aircode + '" class="rs_d1_aircode" type="hidden" />', k.data("cityid")) : b.parents().is(j.elements.multi_dest) && (b.is(j.elements.from) ? (l = a(j.elements.from, j.elements.multi_dest).index(b) + 1, this.check_add(a(".rs_o" + l + "_aircode", j.elements.multi_dest), a(j.elements.multi_dest, j.$parent), '<input name="' + j.names["rs_o" + l + "_aircode"] + '" class="rs_o' + l + '_aircode" type="hidden" />', k.data("cityid"))) : b.is(j.elements.to) && (l = a(j.elements.to, j.elements.multi_dest).index(b) + 1, this.check_add(a(".rs_d" + l + "_aircode", j.elements.multi_dest), a(j.elements.multi_dest, j.$parent), '<input name="' + j.names["rs_d" + l + "_aircode"] + '" class="rs_d' + l + '_aircode" type="hidden" />', k.data("cityid")), j["d" + l + "_set"] = !1)) : "vp" == d && (b.is(j.elements.from) ? this.check_add(a(".rs_o_cityid", j.$parent), j.$parent, '<input name="' + j.names.rs_o_cityid + '" class="rs_o_cityid" type="hidden" />', k.data("cityid")) : b.is(j.elements.to) && this.check_add(a(".rs_d_cityid", j.$parent), j.$parent, '<input name="' + j.names.rs_d_cityid + '" class="rs_d_cityid" type="hidden" />', k.data("cityid")))) : k.data("airid") ? (f = "airport", i = k.data("airid"), "hotel" == d ? this.check_add(a(".rs_airid", j.$parent), j.$parent, '<input name="' + j.names.airport_code + '" class="rs_airid" type="hidden" />', k.data("airid")) : "car" == d ? b.hasClass(j.elements.from.replace(".", "")) || j.elements.from == "#" + b.attr("id") ? this.check_add(a(".rs_pu_airport", j.$parent), j.$parent, '<input name="' + j.names.rs_pu_airport + '" class="rs_pu_airport" type="hidden" />', k.data("airid")) : (b.hasClass(j.elements.to.replace(".", "")) || j.elements.to == "#" + b.attr("id")) && this.check_add(a(".rs_do_airport", j.$parent), j.$parent, '<input name="' + j.names.rs_do_airport + '" class="rs_do_airport" type="hidden" />', k.data("airid")) : "air" == d ? b.parents().is(j.elements.round_trip) ? b.is(j.elements.from) ? this.check_add(a(".rs_o_aircode", j.elements.round_trip), a(j.elements.round_trip, j.$parent), '<input name="' + j.names.rs_o_aircode + '" class="rs_o_aircode" type="hidden" />', k.data("airid")) : b.is(j.elements.to) && this.check_add(a(".rs_d_aircode", j.elements.round_trip), a(j.elements.round_trip, j.$parent), '<input name="' + j.names.rs_d_aircode + '" class="rs_d_aircode" type="hidden" />', k.data("airid")) : b.parents().is(j.elements.one_way) ? b.is(j.elements.from) ? this.check_add(a(".rs_o1_aircode", j.elements.one_way), a(j.elements.one_way, j.$parent), '<input name="' + j.names.rs_o1_aircode + '" class="rs_o1_aircode" type="hidden" />', k.data("airid")) : b.is(j.elements.to) && this.check_add(a(".rs_d1_aircode", j.elements.one_way), a(j.elements.one_way, j.$parent), '<input name="' + j.names.rs_d1_aircode + '" class="rs_d1_aircode" type="hidden" />', k.data("airid")) : b.parents().is(j.elements.multi_dest) && (b.is(j.elements.from) ? (l = a(j.elements.from, j.elements.multi_dest).index(b) + 1, this.check_add(a(".rs_o" + l + "_aircode", j.elements.multi_dest), a(j.elements.multi_dest, j.$parent), '<input name="' + j.names["rs_o" + l + "_aircode"] + '" class="rs_o' + l + '_aircode" type="hidden" />', k.data("airid"))) : b.is(j.elements.to) && (l = a(j.elements.to, j.elements.multi_dest).index(b) + 1, this.check_add(a(".rs_d" + l + "_aircode", j.elements.multi_dest), a(j.elements.multi_dest, j.$parent), '<input name="' + j.names["rs_d" + l + "_aircode"] + '" class="rs_d' + l + '_aircode" type="hidden" />', k.data("airid")), j["d" + l + "_set"] = !1)) : "vp" == d && (b.is(j.elements.from) ? this.check_add(a(".rs_o_aircode", j.$parent), j.$parent, '<input name="' + j.names.rs_o_aircode + '" class="rs_o_aircode" type="hidden" />', k.data("airid")) : b.is(j.elements.to) && this.check_add(a(".rs_d_aircode", j.$parent), j.$parent, '<input name="' + j.names.rs_d_aircode + '" class="rs_d_aircode" type="hidden" />', k.data("airid")))) : k.data("regionid") ? (f = "region", i = k.data("regionid"), "hotel" == d ? this.check_add(a(".rs_regionid", j.$parent), j.$parent, '<input name="' + j.names.region_id + '" class="rs_regionid" type="hidden" />', k.data("regionid")) : "car" == d ? h("Car does not support region ids.") : "air" == d ? h("Air does not support region ids.") : "vp" == d && h("VP does not support region ids.")) : k.data("poiid") ? (f = "poi", i = k.data("poiid"), "hotel" == d ? this.check_add(a(".rs_poiid", j.$parent), j.$parent, '<input name="' + j.names.poi_id + '" class="rs_poiid" type="hidden" />', k.data("poiid")) : "car" == d ? h("Car does not support poi ids.") : "air" == d ? h("Air does not support poi ids.") : "vp" == d && h("VP does not support poi ids.")) : k.data("hotelid") ? (f = "hotel", i = k.data("hotelid"), "hotel" == d ? this.check_add(a(".rs_hotelid", j.$parent), j.$parent, '<input name="' + j.names.hotel_id + '" class="rs_hotelid" type="hidden" />', k.data("hotelid")) : "car" == d ? h("Car does not support hotel ids.") : "air" == d ? h("Air does not support hotel ids.") : "vp" == d && h("VP does not support hotel ids.")) : k.data("latitude") && k.data("longitude") ? (f = "latitude & longitude", i = k.data("latitude") + " - " + k.data("longitude"), "hotel" == d ? (this.check_add(a(".rs_latitude", j.$parent), j.$parent, '<input name="' + j.names.latitude + '" class="rs_latitude" type="hidden" />', k.data("latitude")), this.check_add(a(".rs_longitude", j.$parent), j.$parent, '<input name="' + j.names.longitude + '" class="rs_longitude" type="hidden" />', k.data("longitude"))) : "car" == d ? h("Car does not support latitude & longitude searches.") : "air" == d ? h("Air does not support latitude & longitude searches.") : "vp" == d && h("VP does not support latitude & longitude searches.")) : k.data("geo") && (f = "geolocation", this.get_location(b, d, k)), a(".rs_suggest", j.$parent).hide(), "" != b.val() && g({
                t: "event",
                ec: "searchbox set",
                ea: this.options.refid + ": " + location.href,
                el: d + ": " + ('"' + b.val() + '" = "' + k.data("name") + '" (' + (k.data("cityid") || k.data("airid") || k.data("regionid") || k.data("poiid") || k.data("hotelid")) + ")")
            }), null != j.search_timout && (clearTimeout(j.search_timout), j.search_timout = null, this.s_add(d, b.prop("name"), b.val())), this.s_add(d, b.prop("name"), !1, {
                type: f,
                id: i,
                name: k.data("name"),
                position: a("li", a(".rs_suggest")).index(k)
            }), "geolocation" !== f && this.store_search(d, k.data()), j.autosuggest.set_callback && (e = j.autosuggest.set_callback, "function" == typeof e ? e(k, b, this, d) : h('Callback function "' + j.autosuggest.set_callback + '" for save_selection does not exist.'))
        },
        store_search: function(a, b) {
            var c = "rsRecentSearch_" + a,
                d = this.read_cookie(c) || [];
            return "object" == typeof b && 899999999 === b.cityid ? !1 : ("string" == typeof d && (d = JSON.parse(d)), void("object" == typeof b && "" != b.name && (d = d.filter(function(a) {
                return a.name !== b.name
            }), d.unshift(b), this.create_cookie(c, JSON.stringify(d.slice(0, 5)), 7))))
        },
        auto_set: function(b) {
            var c = this.options[b];
            c.chk_in = c.currentDate.getMonth() + 1 + "/" + c.currentDate.getDate() + "/" + c.currentDate.getFullYear(), "air" == b && (c.chk_in1 = c.chk_in), a("input[name=" + c.names.check_in + "]", c.$parent).each(function() {
                "air" != b || "rs_chk_in" != a(this).attr("name") && "rs_chk_out" != a(this).attr("name") && "rs_chk_in1" != a(this).attr("name") ? "air" != b && a(this).val(c.chk_in) : a(this).val(c.chk_in)
            }), c.elements.chk_in_display && a(c.elements.chk_in_display, c.$parent).html(this.format_date(b, c.currentDate)), c.currentDate = new Date(c.chk_in), c.currentDate = new Date(c.currentDate.getFullYear(), c.currentDate.getMonth(), c.currentDate.getDate() + 1), c.chk_out = c.currentDate.getMonth() + 1 + "/" + c.currentDate.getDate() + "/" + c.currentDate.getFullYear(), a("input[name=" + c.names.check_out + "]", c.$parent).val(c.chk_out), c.elements.chk_out_display && a(c.elements.chk_out_display, c.$parent).html(this.format_date(b, c.currentDate))
        },
        make_cal: function(b, c, d) {
            var e, f = this.options[c],
                g = !1,
                h = !1;
            if ("air" == c)
                for (g = b.parents().hasClass(f.elements.one_way.replace(".", "")) || b.closest(f.elements.one_way).length ? "one_way" : f.elements.multi_dest && (b.parents().hasClass(f.elements.multi_dest.replace(".", "")) || b.closest(f.elements.multi_dest).length) ? "multi_dest" : "round_trip", "rs_chk_in1" == b.attr("name") || "rs_chk_in2" == b.attr("name") || "rs_chk_in3" == b.attr("name") || "rs_chk_in4" == b.attr("name") || "rs_chk_in5" == b.attr("name") ? d = b.attr("name").replace("rs_", "") : ("rs_chk_in1" == b.siblings("input").attr("name") || "rs_chk_in2" == b.siblings("input").attr("name") || "rs_chk_in3" == b.siblings("input").attr("name") || "rs_chk_in4" == b.siblings("input").attr("name") || "rs_chk_in5" == b.siblings("input").attr("name")) && (d = b.siblings("input").attr("name").replace("rs_", "")), e = 1; 5 >= e; e++) f.elements["chk_in" + e + "_display"] && (b.hasClass(f.elements["chk_in" + e + "_display"].replace(".", "")) || b.children().hasClass(f.elements["chk_in" + e + "_display"].replace(".", ""))) && (d = "chk_in" + e), this.check_remove(a(".rs_chk_in" + e + "_box", f.$parent));
            b.data("cal_after") && (h = a(b.data("cal_after"))), this.check_remove(a(".rs_cal", this.options.$parent)), this.check_add(a(".rs_" + d + "_box", f.$parent), h || b, "<div class='rs_" + d + "_box rs_cal' style='display: none;'/>", !1, "after"), "chk_in" == d && this.close_calendar(a(".rs_chk_out_box", f.$parent), c), "chk_out" == d && this.close_calendar(a(".rs_chk_in_box", f.$parent), c), this.draw_month(c, d, !0, g, b)
        },
        next_month: function(a, b, c) {
            var d = this.options[a];
            d.currentDate = new Date(d.currentDate.getFullYear(), d.currentDate.getMonth() + d.calendar.skip, 1), this.draw_month(a, b, !1, c)
        },
        previous_month: function(a, b, c) {
            var d = this.options[a];
            d.currentDate = new Date(d.currentDate.getFullYear(), d.currentDate.getMonth() - d.calendar.skip, d.currentDate.getDate()), this.draw_month(a, b, !1, c)
        },
        adjust_months: function(a) {
            var b = this.options[a];
            b.currentDate instanceof Date && (this.dates.days_in_months[1] = 29 == new Date(b.currentDate.getFullYear(), 1, 29).getDate() ? 29 : 28)
        },
        draw_month: function(b, d, e, f, g) {
            var i, j, k, l, m, n, o, p, q, r, s, t = this.options[b],
                u = new Date(this.dates.max_date.getFullYear(), this.dates.max_date.getMonth(), this.dates.max_date.getDate() + 1),
                v = {
                    chk_in: "",
                    chk_out: ""
                },
                w = "";
            if (this.adjust_months(b), "air" == b && "round_trip" != f)
                for (v = {}, i = 1;
                    ("one_way" == f ? 1 : 5) >= i; i++) v["chk_in" + i] = "";
            for (j in v) v.hasOwnProperty(j) && t[j] && (v[j] = new Date(t[j]), e && (t.currentDate = v[j]));
            for (k = 0; k < t.calendar.months; k++) {
                for (l = 7, m = 6, n = new Date(t.currentDate.getFullYear(), t.currentDate.getMonth() + k, t.currentDate.getDate()), s = 0, w += '<div class="rs_cal_box">', w += '<div class="rs_calTop">', (n.getMonth() > this.dates.today.getMonth() || n.getFullYear() > this.dates.today.getFullYear()) && 0 == k && (w += '<span class="rs_cal_previous_month">' + t.calendar.previous_display + "</span>"), w += '<span class="rs_cal_month">' + this.dates.months[n.getMonth()] + " " + n.getFullYear() + "</span>", (n.getFullYear() < this.dates.max_date.getFullYear() || n.getMonth() < this.dates.max_date.getMonth() && n.getFullYear() == this.dates.max_date.getFullYear()) && k == t.calendar.months - 1 && (w += '<span class="rs_cal_next_month">' + t.calendar.next_display + "</span>"), w += "</div>", w += '<table class="rs_cal_table">', w += '<tr class="rs_topRow"><td>S</td><td>M</td><td>T</td><td>W</td><td>T</td><td>F</td><td>S</td></tr>', o = 1; o <= this.dates.days_in_months[n.getMonth()]; o++) {
                    if (n = new Date(n.getFullYear(), n.getMonth(), o), 7 == l && (w += "<tr>"), 1 == o && 0 != n.getDay())
                        for (p = 0; p < n.getDay(); p++) w += "<td>&nbsp</td>", l--;
                    n < this.dates.today || n > this.dates.max_date && ("chk_in" == d || "chk_in1" == d) || n > u && ("chk_out" == d || "chk_in2" == d || "chk_in3" == d || "chk_in4" == d || "chk_in5" == d) || v.chk_in > n && "chk_in" != d || v.chk_in1 > n && "chk_in1" != d ? w += '<td class="rs_disabledDate">' + o + "</td>" : v.chk_in && v.chk_in.getTime() == n.getTime() || v.chk_out && v.chk_out.getTime() == n.getTime() || v.chk_in1 && v.chk_in1.getTime() == n.getTime() || v.chk_in2 && v.chk_in2.getTime() == n.getTime() || v.chk_in3 && v.chk_in3.getTime() == n.getTime() || v.chk_in4 && v.chk_in4.getTime() == n.getTime() || v.chk_in5 && v.chk_in5.getTime() == n.getTime() ? (0 == s && v.chk_out && n.getTime() == v.chk_out.getTime() && s++, w += t.calendar.allow_same && ("chk_out" == d || "chk_in1" == d || "chk_in2" == d && n >= v.chk_in1 || "chk_in3" == d && n >= v.chk_in2 || "chk_in4" == d && n >= v.chk_in3 || "chk_in5" == d && n >= v.chk_in4) ? '<td class="rs_setDate ' + (0 == s ? "rs_setDateStart" : "rs_setDateEnd") + ' rs_cal_day" data-day="' + n.getDate() + '" data-month="' + n.getMonth() + '" data-year="' + n.getFullYear() + '">' + o + "</td>" : '<td class="rs_setDate ' + (0 == s ? "rs_setDateStart" : "rs_setDateEnd") + ("chk_in" == d || "chk_out" == d && v.chk_out && v.chk_out.getTime() == n.getTime() ? ' rs_cal_day"' : '"') + ' data-day="' + n.getDate() + '" data-month="' + n.getMonth() + '" data-year="' + n.getFullYear() + '">' + o + "</td>", s++) : n > v.chk_in && n < v.chk_out || n > v.chk_in1 && n < v.chk_in5 || n > v.chk_in1 && n < v.chk_in4 || n > v.chk_in1 && n < v.chk_in3 || n > v.chk_in1 && n < v.chk_in2 ? (w += "chk_in3" == d && n <= v.chk_in2 || "chk_in4" == d && n <= v.chk_in3 || "chk_in5" == d && n <= v.chk_in4 ? '<td class="rs_betweenDate" data-day="' + n.getDate() + '" data-month="' + n.getMonth() + '" data-year="' + n.getFullYear() + '">' + o + "</td>" : '<td class="rs_cal_day rs_betweenDate" data-day="' + n.getDate() + '" data-month="' + n.getMonth() + '" data-year="' + n.getFullYear() + '">' + o + "</td>", s++) : w += '<td class="rs_cal_day" data-day="' + n.getDate() + '" data-month="' + n.getMonth() + '" data-year="' + n.getFullYear() + '">' + o + "</td>", l--, 0 == l && (w += "</tr>", m--, l = 7)
                }
                if (7 != l)
                    for (; 7 > l && l > 0;) w += "<td>&nbsp</td>", l--, 0 == l && (m--, w += "</tr>");
                if (0 != m)
                    for (; m > 0;) {
                        for (w += "<tr>", p = 0; 7 > p; p++) w += "<td>&nbsp</td>";
                        w += "</tr>", m--
                    }
                1 == t.calendar.months && (w += '<tr class="rs_calClose"><td colspan="7">X Close Calendar</td></tr>'), w += "</table></div>"
            }
            if (a(".rs_" + d + "_box", t.$parent).html(w).show(), a(".rs_cal_next_month", t.$parent).on("click.searchbox", {
                    _self: this,
                    product: b,
                    cal: d,
                    air_sub: f
                }, function(a) {
                    a.data._self.next_month(a.data.product, a.data.cal, a.data.air_sub)
                }), a(".rs_cal_previous_month", t.$parent).on("click.searchbox", {
                    _self: this,
                    product: b,
                    cal: d,
                    air_sub: f
                }, function(a) {
                    a.data._self.previous_month(a.data.product, a.data.cal, a.data.air_sub)
                }), a(".rs_calClose", t.$parent).on("click.searchbox", {
                    _self: this,
                    product: b,
                    cal: d
                }, function(b) {
                    b.data._self.close_calendar(a(".rs_" + b.data.cal + "_box", b.data.$parent), b.data.product)
                }), a(".rs_cal_day", t.$parent).on("click.searchbox", {
                    _self: this,
                    product: b,
                    cal: d
                }, function(b) {
                    b.data._self.set_date(a(this), b.data.product, b.data.cal, !1)
                }).on("mouseenter.searchbox", {
                    _self: this,
                    product: b,
                    cal: d
                }, function(b) {
                    {
                        var c = b.data.cal.replace(/([^0-9])/g, ""),
                            d = c ? "check_in" + c : "chk_in" == b.data.cal ? "check_in" : "check_out",
                            e = a("input[name=" + b.data._self.options[b.data.product].names[d] + "]", b.data._self.options[b.data.product].$parent),
                            f = new Date(a(this).data("year"), a(this).data("month"), a(this).data("day"));
                        b.data._self.options[b.data.product].elements[b.data.cal + "_display"]
                    }
                    b.data._self.options[b.data.product].calendar.inputs[d] = e.val(), e.val(b.data._self.dates.days[f.getDay()].substr(0, 3) + " " + f.getDate() + "/" + (f.getMonth() + 1))
                }).on("mouseleave.searchbox", {
                    _self: this,
                    product: b,
                    cal: d
                }, function(b) {
                    {
                        var c = b.data.cal.replace(/([^0-9])/g, ""),
                            d = c ? "check_in" + c : "chk_in" == b.data.cal ? "check_in" : "check_out",
                            e = a("input[name=" + b.data._self.options[b.data.product].names[d] + "]", b.data._self.options[b.data.product].$parent);
                        b.data._self.options[b.data.product].elements[b.data.cal + "_display"]
                    }
                    e.val(b.data._self.options[b.data.product].calendar.inputs[d])
                }), t.calendar.listen || (t.calendar.listen = a(c).click({
                    _self: this,
                    options: t,
                    product: b
                }, function(b) {
                    var c = a(b.target);
                    if (0 == c.parents().is(".rs_cal") && "rs_cal_next_month" != c.attr("class") && "rs_cal_previous_month" != c.attr("class") && "rs_cal_previous_month" != c.parent().attr("class") && "rs_cal_next_month" != c.parent().attr("class") && b.target.classList && "rs_cal_previous_month" != a("." + b.target.classList[0]).parent().attr("class") && b.target.classList && "rs_cal_next_month" != a("." + b.target.classList[0]).parent().attr("class")) {
                        for (q = (b.data.options.elements.chk_in.replace(/\s+/g, "") + "," + b.data.options.elements.chk_out.replace(/\s+/g, "")).split(","), r = !0, i = 0; i < q.length; i++) a(q[i], b.data.options.$parent).each(function() {
                            c.get(0) == this && (r = !1)
                        }), c.parents().is(q[i]) && (r = !1);
                        (c.hasClass("rs_chk_in") || c.hasClass("rs_chk_out") || c.hasClass("rs_cal_arrow")) && (r = !1), r && b.data._self.close_calendar(a(".rs_cal", b.data.options.$parent), b.data.product)
                    }
                })), t.calendar.callback) {
                var x = t.calendar.callback;
                "function" == typeof x ? x(this, b, d, g || !1) : h('Callback function "' + t.calendar.callback + '" for draw_month does not exist.')
            }
        },
        set_date: function(b, c, d, e) {
            var f, g, i, j, k, l, m, n = this.options[c],
                o = d.replace(/([^0-9])/g, ""),
                p = null,
                q = {};
            if (e ? (b = a("option:selected", b), f = "day_in" == d ? "month_in" : "day_out" == d ? "month_out" : "month_in" + o, d = "day_in" == d ? "chk_in" : "day_out" == d ? "chk_out" : "chk_in" + o, g = "air" == c ? a(n.elements[f] + " option:selected", a("" != o ? b.parents().is(n.elements.multi_dest) ? n.elements.multi_dest : n.elements.one_way : n.elements.round_trip)) : a(n.elements[f] + " option:selected", n.$parent)) : this.close_calendar(a(".rs_" + d + "_box", n.$parent), c), this.check_add(a("input[name=" + n.names["chk_in" == d ? "check_in" : "check_out"] + "]", n.$parent), n.$parent, "<input class='rs_" + d + "' name='rs_" + d + "' type='hidden' />"), n[d] = e ? g.data("month") + 1 + "/" + b.data("day") + "/" + g.data("year") : b.data("month") + 1 + "/" + b.data("day") + "/" + b.data("year"), n.calendar.inputs["chk_in" == d || o ? "check_in" + o : "check_out"] = this.format_date(c, new Date(n[d]), n.calendar.date_format), a("input[name=" + n.names[o ? "check_in" + o : "chk_in" == d ? "check_in" : "check_out"] + "]", n.$parent).val(this.format_date(c, new Date(n[d]), n.calendar.date_format)).trigger("change"), i = new Date(n[d]), n.elements[d + "_display"] && ("car" == c && (p = a(n.elements["chk_in" == d ? "time_in" : "time_out"], n.$parent).val().split(":"), i.setHours(p[0], p[1])), a(n.elements[d + "_display"], n.$parent).html(this.format_date(c, i))), "chk_in" == d) n.calendar.next_day ? (n.currentDate = new Date(n.chk_in), n.currentDate = new Date(n.currentDate.getFullYear(), n.currentDate.getMonth(), n.currentDate.getDate() + 1), n.chk_out = n.currentDate.getMonth() + 1 + "/" + n.currentDate.getDate() + "/" + n.currentDate.getFullYear(), this.check_add(a("input[name=" + n.names.check_out + "]", n.$parent), n.$parent, "<input class='rs_chk_out' name='rs_chk_out' type='hidden'/>", this.format_date(c, new Date(n.chk_out), n.calendar.date_format)), e && (this.generate_months(c, "month_out"), a(n.elements.month_out, n.$parent).val(n.currentDate.getMonth()), this.generate_days(a(n.elements.month_out + " option:selected", n.$parent), c, "month_out"), a(n.elements.day_out, n.$parent).val(n.currentDate.getDate())), n.elements.chk_out_display && (i = new Date(n.chk_out), "car" == c && (p = a(n.elements.time_out, n.$parent).val().split(":"), i.setHours(p[0], p[1])), a(n.elements.chk_out_display, n.$parent).html(this.format_date(c, i)))) : e && (n.currentDate = new Date(n.chk_in), n.currentDate = new Date(n.currentDate.getFullYear(), n.currentDate.getMonth(), n.currentDate.getDate() + 1), this.generate_months(c, "month_out")), e || (n.calendar.pop_out && !o && (a("input[name=" + n.names.check_out + "]", n.$parent).is(":visible") ? this.make_cal(a("input[name=" + n.names.check_out + "]", n.$parent), c, "chk_out") : n.elements.chk_out_display && a(n.elements.chk_out_display, n.$parent).is(":visible") && this.make_cal(a(n.elements.chk_out_display, n.$parent), c, "chk_out")), n.elements.month_in && n.elements.day_in && n.elements.month_out && n.elements.day_out && (a(n.elements.month_in, n.$parent).val(b.data("month")), this.generate_days(a(n.elements.month_in + " option:selected", n.$parent), c, "month_in", !0), a(n.elements.day_in, n.$parent).val(b.data("day")), n.calendar.next_day && (a(n.elements.month_out, n.$parent).val(n.currentDate.getMonth()), this.generate_days(a(n.elements.month_out + " option:selected", n.$parent), c, "month_out", !0), a(n.elements.day_out, n.$parent).val(n.currentDate.getDate()))));
            else if ("chk_out" != d || e) {
                if (o && !e)
                    for (k = o; 5 >= k; k++) k == o && n.elements["month_in" + k] && n.elements["day_in" + k] ? (a(n.elements["month_in" + k], n.$parent).val(b.data("month")), this.generate_days(a(n.elements["month_in" + k] + " option:selected", n.$parent), c, "month_in" + k, !0), a(n.elements["day_in" + k], n.$parent).val(b.data("day"))) : n.elements["month_in" + k] && n.elements["day_in" + k] && this.generate_months(c, "month_in" + k)
            } else n.elements.month_in && n.elements.day_in && n.elements.month_out && n.elements.day_out && (a(n.elements.month_out, n.$parent).val(n.currentDate.getMonth()), this.generate_days(a(n.elements.month_out + " option:selected", n.$parent), c, "month_out", !0), j = new Date(n.chk_out), a(n.elements.day_out, n.$parent).val(j.getDate()));
            for (k = 1; 5 >= k; k++)
                if (n["chk_in" + k] && !e) {
                    if (q["chk_in" + k] = new Date(n["chk_in" + k]), k > 1)
                        for (l = k - 1; l > 0; l--)
                            if (q["chk_in" + l] > q["chk_in" + k]) {
                                this.remove_extra(c, k);
                                break
                            }
                } else if (d == "chk_in" + k && e)
                for (n.currentDate = new Date(n["chk_in" + k]), n.currentDate = new Date(n.currentDate.getFullYear(), n.currentDate.getMonth(), n.currentDate.getDate() + 1), q["chk_in" + k] = new Date(n["chk_in" + k]), l = k + 1; 5 >= l; l++)
                    if (q["chk_in" + l] = new Date(n["chk_in" + l]), this.generate_months(c, "month_in" + l), q["chk_in" + l] < q["chk_in" + k]) {
                        this.remove_extra(c, k);
                        break
                    }
            n.calendar.set_callback && (m = n.calendar.set_callback, "function" == typeof m ? m(b, this, c, d) : h('Callback function "' + n.calendar.set_callback + '" for set_date does not exist.'))
        },
        remove_extra: function(b, c) {
            var d = this.options[b];
            for (c; 5 >= c; c++) delete d["chk_in" + c], a("input[name=rs_chk_in" + c + "]", d.$parent).val(""), d.elements["chk_in" + c + "_display"] && a(d.elements["chk_in" + c + "_display"], d.$parent).html(this.format_date(b, !1))
        },
        set_car_time: function(b, c, d) {
            var e = this.options[c],
                f = "time_in" == d ? "chk_in" : "chk_out",
                g = new Date(e[f]),
                h = b.val().split(":");
            e[f] && e.elements[f + "_display"] && (g.setHours(h[0], h[1]), a(e.elements[f + "_display"], e.$parent).html(this.format_date(c, g)))
        },
        format_date: function(a, b, c) {
            var d, e = this.options[a],
                f = "",
                g = "",
                c = c || e.calendar.output_format;
            for (d = 0; d < c.length; d++)
                if (g = c.charAt(d), "[" == g) {
                    switch (g = c.charAt(d + 1)) {
                        case "d":
                            f += b ? (b.getDate() < 10 ? "0" : "") + b.getDate() : "Day";
                            break;
                        case "j":
                            f += b ? b.getDate() : "Day";
                            break;
                        case "D":
                            f += b ? this.dates.days[b.getDay()].substring(0, 3) : "";
                            break;
                        case "l":
                            f += b ? this.dates.days[b.getDay()] : "";
                            break;
                        case "F":
                            f += b ? this.dates.months[b.getMonth()] : "Month";
                            break;
                        case "n":
                            f += b ? b.getMonth() + 1 : "";
                            break;
                        case "m":
                            f += b ? (b.getMonth() < 9 ? "0" : "") + (b.getMonth() + 1) : "";
                            break;
                        case "M":
                            f += b ? this.dates.months[b.getMonth()].substring(0, 3) : "Month";
                            break;
                        case "y":
                            f += b ? String(b.getFullYear()).substring(2, 4) : "Year";
                            break;
                        case "Y":
                            f += b ? b.getFullYear() : "Year";
                            break;
                        case "g":
                            f += b ? b.getHours() % 12 || 12 : "";
                            break;
                        case "G":
                            f += b ? b.getHours() : "";
                            break;
                        case "i":
                            f += String(b.getMinutes()).length < 2 ? b ? "0" + b.getMinutes() : "" : b ? b.getMinutes() : "";
                            break;
                        case "s":
                            f += String(b.getSeconds()).length < 2 ? b ? "0" + b.getSeconds() : "" : b ? b.getSeconds() : "";
                            break;
                        case "a":
                            f += b ? b.getHours() < 12 ? "am" : "pm" : "";
                            break;
                        case "A":
                            f += b ? b.getHours() < 12 ? "AM" : "PM" : "";
                            break;
                        default:
                            f += g
                    }
                    d += 2
                } else f += g;
            return f
        },
        close_calendar: function(a, b) {
            var c = this.options[b];
            if (a.is(":visible") && (a.hide(), c.calendar.close_callback)) {
                var d = c.calendar.close_callback;
                "function" == typeof d ? d(this, b, a) : h('Callback function "' + c.calendar.close_callback + '" for close_calendar does not exist.')
            }
        },
        check_form: function(c, d) {
            var e, f = this.options[d],
                i = null,
                j = null,
                k = "",
                l = 0,
                m = 0;
            if (this.url_string = "", f.pre_check) {
                if (i = f.pre_check, "function" != typeof i) return h('Pre-check function "' + f.pre_check + '" for check_form does not exist.'), g({
                    t: "event",
                    ec: "searchbox error",
                    ea: this.options.refid + ": " + location.href,
                    el: 'Pre-check function "' + f.pre_check + '" for check_form does not exist.'
                }), !1;
                if (1 != i(c, d)) return !1
            }
            if (this.check_add(a("input[name=refid]", f.$parent), f.$parent, "<input name='refid' type='hidden' value='" + this.options.refid + "'/>"), "hotel" == d) {
                if (!this.check_hotel()) return !1;
                k = a(f.elements.autosuggest, f.$parent).val() || "n/a"
            } else if ("car" == d) {
                if (!this.check_car()) return !1;
                k = a(f.elements.from, f.$parent).val() || "n/a", 1 == a(f.elements.different_return, f.$parent).is(":checked") && (k += " - " + (a(f.elements.to, f.$parent).val() || "n/a"))
            } else if ("air" == d) {
                if (!this.check_air(c)) return !1;
                e = c.parents().is(f.elements.round_trip) ? "round_trip" : c.parents().is(f.elements.one_way) ? "one_way" : c.parents().is(f.elements.multi_dest) ? "multi_dest" : !1, a(f.elements.from, f.elements[e]).each(function() {
                    l > 0 && (k += " | "), l++, m = 0, k += a(this).val() || "n/a", a(f.elements.to, f.elements[e]).each(function() {
                        m++, l == m && (k += " - " + (a(this).val() || "n/a"))
                    })
                })
            } else {
                if ("vp" != d) return h("Invalid product in check_form."), !1;
                if (!this.check_vp()) return !1;
                k = a(f.elements.from, f.$parent).val() || "n/a", k += " - " + (a(f.elements.to, f.$parent).val() || "n/a")
            }
            if (this.options.open_window && (a(f.$parent).attr("target", "_blank"), "air" == d && setTimeout(a.proxy(this.re_add_air, this), 1500)), this.options.iframe && a(f.$parent).attr("target", "_parent"), this.s_trk(d), g({
                    t: "event",
                    ec: "searchbox submit",
                    ea: this.options.refid + ": " + location.href,
                    el: this.url_string + f.$parent.serialize(),
                    cn: k,
                    cm: d,
                    ci: ""
                }), (-1 !== this.url_string.indexOf("search") || -1 !== this.url_string.indexOf("did_you_mean")) && g({
                    t: "event",
                    ec: "searchbox search",
                    ea: this.options.refid + " - " + d + ": " + location.href,
                    el: k
                }), f.$parent.is("form") || (b.location.href = this.url_string + a(":input", f.$parent).serialize()), f.post_check) {
                if (j = f.post_check, "function" != typeof j) return h('Post-check function "' + f.post_check + '" for check_form does not exist.'), g({
                    t: "event",
                    ec: "searchbox error",
                    ea: this.options.refid + ": " + location.href,
                    el: 'Post-check function "' + f.post_check + '" for check_form does not exist.'
                }), !1;
                if (1 != j(c, d, this)) return !1
            }
            return !0
        },
        check_hotel: function(c) {
            var d = this.options.hotel,
                e = /^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$/,
                f = a(d.elements.autosuggest, d.$parent),
                g = a("input[name=" + d.names.city_id + "]", d.$parent),
                h = a("input[name=" + d.names.airport_code + "]", d.$parent),
                i = a("input[name=" + d.names.region_id + "]", d.$parent),
                j = a("input[name=" + d.names.poi_id + "]", d.$parent),
                k = a("input[name=" + d.names.hotel_id + "]", d.$parent),
                l = a("input[name=" + d.names.latitude + "]", d.$parent),
                m = a("input[name=" + d.names.longitude + "]", d.$parent),
                n = a("input[name=" + d.names.check_in + "]", d.$parent),
                o = a("input[name=" + d.names.check_out + "]", d.$parent),
                p = a("select[name=" + d.names.rooms + "]", d.$parent),
                q = a("select[name=" + d.names.adults + "]", d.$parent),
                r = a("select[name=" + d.names.children + "]", d.$parent),
                s = !1;
            if (c = c === !0, 0 !== g.length && "" != g.val() || 0 !== h.length && "" != h.val() || 0 !== i.length && "" != i.val() || 0 !== j.length && "" != j.val() || 0 !== l.length && "" != l.val() && 0 !== m.length && "" != m.val() && f.val() != d.autosuggest.default_label) d.action.results ? this.url_string = d.action.results : (s = "results", this.url_string += d.express_deals ? this.options.cname + "/hotels/express_results/?" : this.options.cname + (null !== d.mk ? "/ivm/inbound/?" : "/hotels/results/?"));
            else if (0 !== k.length && "" != k.val() && f.val() != d.autosuggest.default_label) d.action.hotel ? this.url_string = d.action.hotel : (s = "hotel", this.url_string += this.options.cname + (null !== d.mk ? "/ivm/inbound/?" : "/hotels/hotel/?"));
            else {
                if ("" == f.val() || f.val() == d.autosuggest.default_label || null === f.val().match(/[^\s*]/)) return c || (this.check_alert("hotel", a.i18n._t("Please enter a location to search for.")), f.val("").focus()), !1;
                d.action.search ? this.url_string = d.action.search : (s = "search", this.url_string += this.options.cname + (null !== d.mk ? "/ivm/inbound/?" : "/hotels/search/?"))
            }
            if (a(d.$parent).prop("action", this.url_string), d.calendar.required && 0 == e.test(n.val())) return c || (this.check_alert("hotel", a.i18n._t("Please select a check in date.")), n.trigger("click")), !1;
            if (d.calendar.required && 0 == e.test(o.val())) return c || (this.check_alert("hotel", a.i18n._t("Please select a check out date.")), o.trigger("click")), !1;
            if (0 != e.test(o.val()) && 0 != e.test(n.val()) && (this.string_to_date(o.val(), d.calendar.date_format) - this.string_to_date(n.val(), d.calendar.date_format)) / 864e5 > 21) return this.options.allow_group_booking && !c ? (d.popup && this.do_pop(f.val(), "&hotels=true"), b.location.href = "" != this.options.group_booking && null != this.options.group_booking ? this.options.group_booking : "http://hotelsbycity.hotelplanner.com/Search/Index.cfm?City=" + f.val() + "&InDate=" + n.val() + "&Outdate=" + o.val() + "&NumRooms=" + p.val() + "&sc=HBC" + this.options.refid, !1) : (c || this.check_alert("hotel", a.i18n._t("Sorry, reservations have a maximum length of 21 days.")), !1);
            if (0 != e.test(o.val()) && 0 != e.test(n.val()) || c || (o.val(""), n.val("")), 0 !== p.length && p.val() >= 5 && p.val() <= 8) {
                if (!this.options.allow_group_booking || c) return c || this.check_alert("hotel", a.i18n._t("Sorry, we only allow reservations between 1 and 4 rooms.")), !1;
                if (f.val() != d.autosuggest.default_label) return d.popup && this.do_pop(f.val(), "&hotels=true"), b.location.href = "" != this.options.group_booking && null != this.options.group_booking ? this.options.group_booking : 2055 == this.options.refid ? "http://motelgroups.com/Search/Index.cfm?City=" + f.val() + "&InDate=" + n.val() + "&Outdate=" + o.val() + "&NumRooms=" + p.val() + "&sc=HBC" + this.options.refid : "http://hotelsbycity.hotelplanner.com/Search/Index.cfm?City=" + f.val() + "&InDate=" + n.val() + "&Outdate=" + o.val() + "&NumRooms=" + p.val() + "&sc=HBC" + this.options.refid, !1
            } else if (0 !== p.length && p.val() > 8) {
                if (!this.options.allow_group_booking || c) return c || this.check_alert("hotel", a.i18n._t("Sorry, we only allow reservations between 1 and 4 rooms.")), !1;
                if (f.val() != d.autosuggest.default_label) return d.popup && this.do_pop(f.val(), "&hotels=true"), b.location.href = "" != this.options.group_booking && null != this.options.group_booking ? this.options.group_booking : 2055 == this.options.refid ? "http://motelgroups.com/GroupForm.cfm?City=" + f.val() + "&InDate=" + n.val() + "&Outdate=" + o.val() + "&NumRooms=" + p.val() + "&sc=HBC" + this.options.refid : "http://hotelsbycity.hotelplanner.com/GroupForm.cfm?City=" + f.val() + "&InDate=" + n.val() + "&Outdate=" + o.val() + "&NumRooms=" + p.val() + "&sc=HBC" + this.options.refid, !1
            }
            return 0 !== q.length && 0 !== r.length && 0 !== p.length && parseInt(q.val()) + parseInt(r.val()) > 4 * parseInt(p.val()) || 0 !== q.length && 0 !== p.length && parseInt(q.val()) > 4 * parseInt(p.val()) ? (c || this.check_alert("hotel", "Sorry, we only allow a max of " + 4 * p.val() + " guests for " + p.val() + " room" + (p.val() > 1 ? "s" : "") + ".\nPlease increase the number of rooms or decrease the number of guests."), !1) : 0 !== q.length && 0 !== r.length && 0 !== p.length && parseInt(q.val()) + parseInt(r.val()) < parseInt(p.val()) || 0 !== q.length && 0 !== p.length && parseInt(q.val()) < parseInt(p.val()) ? (c || this.check_alert("hotel", a.i18n._t("Sorry, we have a minimum of 1 guest per room. Please increase the number of guests or decrease the number of rooms.")), !1) : (0 !== l.length && "" != l.val() && 0 !== m.length && "" != m.val() && f.prop("name", "poi_name"), d.express_deals && this.check_add(a("input[name=express_deals]", d.$parent), d.$parent, "<input name='express_deals' type='hidden' value='true'/>"), null !== d.mk && (s && this.check_add(a("input[name=product_page]", d.$parent), d.$parent, "<input name='product_page' type='hidden' value='" + s + "'/>"), this.check_add(a("input[name=mk]", d.$parent), d.$parent, "<input name='mk' type='hidden' value='" + d.mk + "'/>")), this.plugins.aaa_redirect && this.check_add(a("input[name=ace_redirect]", d.$parent), d.$parent, "<input name='ace_redirect' type='hidden' value='true'/>"), f.val() && d.popup && !c && this.do_pop(f.val(), "&hotels=true"), "[m]/[d]/[Y]" != d.calendar.date_format && (d.chk_in && n.val(this.format_date("hotel", new Date(d.chk_in), "[m]/[d]/[Y]")), d.chk_out && o.val(this.format_date("hotel", new Date(d.chk_out), "[m]/[d]/[Y]"))), !0)
        },
        check_car: function(b) {
            var c = this.options.car,
                d = /^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$/,
                e = a(c.elements.from, c.$parent),
                f = a(c.elements.to, c.$parent),
                g = a(c.elements.different_return, c.$parent),
                h = a("input[name=" + c.names.rs_pu_cityid + "]", c.$parent),
                i = a("input[name=" + c.names.rs_do_cityid + "]", c.$parent),
                j = a("input[name=" + c.names.rs_pu_airport + "]", c.$parent),
                k = a("input[name=" + c.names.rs_do_airport + "]", c.$parent),
                l = a("input[name=" + c.names.check_in + "]", c.$parent),
                m = a("input[name=" + c.names.check_out + "]", c.$parent),
                n = new Date(l.val()),
                o = a(c.elements.time_in).val().split(":"),
                p = new Date(m.val()),
                q = a(c.elements.time_out).val().split(":"),
                b = b === !0;
            if (n.setHours(o[0], o[1]), p.setHours(q[0], q[1]), 1 == g.is(":checked") && (0 !== h.length && "" != h.val() || 0 !== j.length && "" != j.val() && e.val() != c.autosuggest.from_default_label) && (0 !== i.length && "" != i.val() || 0 !== k.length && "" != k.val() && f.val() != c.autosuggest.to_default_label) || 0 == g.is(":checked") && (0 !== h.length && "" != h.val() || 0 !== j.length && "" != j.val() && e.val() != c.autosuggest.from_default_label)) {
                if (1 == g.is(":checked") && 0 !== h.length && "" != h.val() && 0 !== i.length && "" != i.val()) return b || this.check_alert("car", a.i18n._t("For a one-way itinerary, either pick up or drop off has to be an airport location.")), !1;
                this.url_string = c.action.results || this.options.cname + "/car_rentals/results/?", a(c.$parent).attr("action", this.url_string)
            } else {
                if (!(1 == g.is(":checked") && "" != e.val() && "" != f.val() && e.val() != c.autosuggest.from_default_label && f.val() != c.autosuggest.to_default_label && null !== e.val().match(/[^\s*]/) && null !== f.val().match(/[^\s*]/) || 0 == g.is(":checked") && "" != e.val() && e.val() != c.autosuggest.from_default_label && null !== e.val().match(/[^\s*]/))) return 1 == g.is(":checked") && "" != e.val() && e.val() != c.autosuggest.from_default_label && null !== e.val().match(/[^\s*]/) ? (b || this.check_alert("car", a.i18n._t("Please enter a dropoff location to search for.")), e.val("").focus()) : (b || this.check_alert("car", a.i18n._t("Please enter a pickup location to search for.")), f.val("").focus()), !1;
                this.url_string = c.action.search || this.options.cname + "/car_rentals/did_you_mean/?", a(c.$parent).attr("action", this.url_string)
            }
            if (0 == g.is(":checked") && ("" != i.val() && i.val(""), "" != k.val() && k.val(""), "" != f.val() && f.val("")), c.calendar.required && 0 == d.test(l.val())) return b || this.check_alert("car", a.i18n._t("Please select a pick up date.")), !1;
            if (c.calendar.required && 0 == d.test(m.val())) return b || this.check_alert("car", a.i18n._t("Please select a drop off date.")), !1;
            if (0 != d.test(m.val()) && 0 != d.test(l.val()) && (this.string_to_date(m.val(), c.calendar.date_format) - this.string_to_date(l.val(), c.calendar.date_format)) / 864e5 > 310) return b || this.check_alert("car", a.i18n._t("Sorry, reservations have a maximum length of 310 days.")), !1;
            if (0 != d.test(m.val()) && 0 != d.test(l.val()) && m.val() == l.val()) {
                if (18e5 > p - n) return b || this.check_alert("car", a.i18n._t("Please choose a drop off time at least 30 minutes after your pick up time.")), !1
            } else if (0 == d.test(m.val()) || 0 == d.test(l.val())) m.val(""), l.val("");
            else if ((n - this.dates.now) / 1e3 / 60 < 30) return b || this.check_alert("car", a.i18n._t("You cannot select a pickup time less than 30 minutes from now.")), !1;
            return e.val() && c.popup && !b && this.do_pop(e.val(), "&hotels=true"), "[m]/[d]/[Y]" != c.calendar.date_format && (c.chk_in && l.val(this.format_date("car", new Date(c.chk_in), "[m]/[d]/[Y]")), c.chk_out && m.val(this.format_date("car", new Date(c.chk_out), "[m]/[d]/[Y]"))), !0
        },
        check_air: function(b, c) {
            var d, e = this.options.air,
                f = /^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$/,
                g = b.parents().is(e.elements.round_trip) ? "round_trip" : b.parents().is(e.elements.one_way) ? "one_way" : b.parents().is(e.elements.multi_dest) ? "multi_dest" : !1,
                h = a(e.elements.from, e.elements[g]),
                i = a(e.elements.to, e.elements[g]),
                j = a("input[name=" + e.names["round_trip" == g ? "rs_o_aircode" : "rs_o1_aircode"] + "]", e.elements[g]),
                k = a("input[name=" + e.names["round_trip" == g ? "rs_d_aircode" : "rs_d1_aircode"] + "]", e.elements[g]),
                l = a("input[name=" + e.names["round_trip" == g ? "check_in" : "check_in1"] + "]", e.elements[g]),
                m = a("input[name=" + e.names.check_out + "]", e.elements[g]),
                n = a("select[name=" + e.names.adults + "]", e.elements[g]),
                o = a("select[name=" + e.names.children + "]", e.elements[g]),
                p = [],
                q = [],
                r = [],
                s = [],
                t = [],
                c = c === !0;
            if ("multi_dest" == g)
                for (h.each(function() {
                        p.push(a(this).val() || !1)
                    }), i.each(function() {
                        q.push(a(this).val() || !1)
                    }), d = 1; 5 >= d; d++) r.push(a("input[name=" + e.names["rs_o" + d + "_aircode"] + "]", e.elements.multi_dest).val() || !1), s.push(a("input[name=" + e.names["rs_d" + d + "_aircode"] + "]", e.elements.multi_dest).val() || !1);
            if ("multi_dest" != g) {
                if (h.val() == e.autosuggest.from_default_label || i.val() == e.autosuggest.to_default_label || "" == h.val() || "" == i.val() || "" == j.val() || 0 === j.length || "" == k.val() || 0 === k.length || (3 != j.val().length || 3 != k.val().length) && ("3" != e.version || 3 != j.val().length && 9 != j.val().length || 3 != k.val().length && 9 != k.val().length))
                    if (h.val() != e.autosuggest.from_default_label && i.val() != e.autosuggest.to_default_label && "" != h.val() && "" != i.val() && null !== h.val().match(/[^\s*]/) && null !== i.val().match(/[^\s*]/)) this.url_string = e.action.search || this.options.cname + ("3" == e.version ? "/flights/search/?" : "/air/search/?"), a(e.$parent).attr("action", this.url_string);
                    else {
                        if (h.val() == e.autosuggest.from_default_label || "" == h.val() || null === h.val().match(/[^\s*]/)) return c || (this.check_alert("air", a.i18n._t("Please enter an origin location to search for.")), h.val("").focus()), !1;
                        if (i.val() == e.autosuggest.to_default_label || "" == i.val() || null === i.val().match(/[^\s*]/)) return c || (this.check_alert("air", a.i18n._t("Please enter a destination location to search for.")), i.val("").focus()), !1
                    }
                else this.url_string = e.action.results || this.options.cname + ("3" == e.version ? "/flights/results/depart/?" : "/air/search/?"), a(e.$parent).attr("action", this.url_string);
                if (e.calendar.required && 0 == f.test(l.val())) return c || this.check_alert("air", a.i18n._t("Please select a departure date.")), !1;
                if (e.calendar.required && 0 == f.test(m.val()) && "round_trip" == g) return c || this.check_alert("air", a.i18n._t("Please select a return date.")), !1
            } else if ("multi_dest" == g) {
                for (d = 0; 5 > d; d++)
                    if (t.push("blank"), p[d] != e.autosuggest.from_default_label && "" != p[d] || q[d] != e.autosuggest.to_default_label && "" != q[d]) {
                        if (d > 0 && -1 !== t.indexOf("blank") && t.indexOf("blank") !== d) return c || (this.check_alert("air", a.i18n._t("You are missing a previous location. Please enter an origin location to search for.")), a(e.elements.multi_dest + " input[name=rs_o_city" + (t.indexOf("blank") + 1) + "]", e.$parent).val("").focus()), !1;
                        if (p[d] != e.autosuggest.from_default_label && "" != p[d] && q[d] != e.autosuggest.to_default_label && "" != q[d] && r[d] && s[d] && (3 == r[d].length && 3 == s[d].length || "3" == e.version && (3 == r[d].length || 9 == r[d].length) && (3 == s[d].length || 9 == s[d].length))) t[d] = "results";
                        else if (p[d] != e.autosuggest.from_default_label && "" != p[d] && q[d] != e.autosuggest.to_default_label && "" != q[d] && null !== p[d].match(/[^\s*]/) && null !== q[d].match(/[^\s*]/)) t[d] = "search";
                        else {
                            if (p[d] == e.autosuggest.from_default_label || "" == p[d] || null === p[d].match(/[^\s*]/)) return c || (this.check_alert("air", a.i18n._t("Please enter an origin location to search for.")), a(e.elements.multi_dest + " input[name=rs_o_city" + (d + 1) + "]", e.$parent).val("").focus()), !1;
                            if (q[d] == e.autosuggest.to_default_label || "" == q[d] || null === q[d].match(/[^\s*]/)) return c || (this.check_alert("air", a.i18n._t("Please enter a destination location to search for.")), a(e.elements.multi_dest + " input[name=rs_d_city" + (d + 1) + "]", e.$parent).val("").focus()), !1
                        }
                        if (e.calendar.required && 0 == f.test(a("input[name=rs_chk_in" + (d + 1) + "]", e.elements.multi_dest).val())) return c || this.check_alert("air", a.i18n._t("Please select a departure date.")), !1
                    } else {
                        if (0 == d) return c || (this.check_alert("air", a.i18n._t("You must enter a starting location.")), a(e.elements.multi_dest + " input[name=rs_o_city1]", e.$parent).val("").focus()), !1;
                        a(e.elements.multi_dest + " input[name=rs_o_city" + (d + 1) + "]", e.$parent).val(""), a(e.elements.multi_dest + " input[name=rs_d_city" + (d + 1) + "]", e.$parent).val("")
                    } - 1 === t.indexOf("search") ? (this.url_string = e.action.results || this.options.cname + ("3" == e.version ? "/flights/results/depart/?" : "/air/search/?"), a(e.$parent).attr("action", this.url_string)) : (this.url_string = e.action.search || this.options.cname + ("3" == e.version ? "/flights/search/?" : "/air/search/?"), a(e.$parent).attr("action", this.url_string))
            }
            if (parseInt(n.val()) + parseInt(o.val()) > 8) return c || this.check_alert("air", a.i18n._t("Sorry, flight reservations are limited to 8 passengers.")), !1;
            if (this.check_add(a(e.elements[g] + " input[name=air_search_type]", e.$parent), a(e.elements[g], e.$parent), '<input name="air_search_type" type="hidden" />'), e._private = {
                    removed: {}
                }, "round_trip" == g ? (e._private.removed.one_way = e.elements.one_way ? a(e.elements.one_way, e.$parent).detach() : !1, e._private.removed.multi_dest = e.elements.multi_dest ? a(e.elements.multi_dest, e.$parent).detach() : !1, a(e.elements.one_way, e.$parent).remove(), a(e.elements.multi_dest, e.$parent).remove(), a(e.elements.round_trip + " input[name=air_search_type]", e.$parent).val("roundtrip")) : "one_way" == g ? (e._private.removed.round_trip = e.elements.round_trip ? a(e.elements.round_trip, e.$parent).detach() : !1, e._private.removed.multi_dest = e.elements.multi_dest ? a(e.elements.multi_dest, e.$parent).detach() : !1, a(e.elements.round_trip, e.$parent).remove(), a(e.elements.multi_dest, e.$parent).remove(), a(e.elements.one_way + " input[name=air_search_type]", e.$parent).val("oneway")) : "multi_dest" == g && (e._private.removed.round_trip = e.elements.round_trip ? a(e.elements.round_trip, e.$parent).detach() : !1, e._private.removed.one_way = e.elements.one_way ? a(e.elements.one_way, e.$parent).detach() : !1, a(e.elements.round_trip, e.$parent).remove(), a(e.elements.one_way, e.$parent).remove(), a(".rs_chk_in", e.elements.multi_dest).each(function() {
                    e.calendar.required && 0 == f.test(a(this).val()) && a(this).val("")
                }), a(e.elements.multi_dest + " input[name=air_search_type]", e.$parent).val("multi")), i.val() && e.popup && !c && this.do_pop(i.val(), "&hotels=true"), "[m]/[d]/[Y]" != e.calendar.date_format)
                for (e.chk_in && l.val(this.format_date("air", new Date(e.chk_in), "[m]/[d]/[Y]")), e.chk_out && m.val(this.format_date("air", new Date(e.chk_out), "[m]/[d]/[Y]")), d = 1; 5 >= d; d++) e["chk_in" + d] && "" != a("input[name=rs_chk_in" + d + "]", e.elements.multi_dest).val() && a("input[name=rs_chk_in" + d + "]", e.elements.multi_dest).val(this.format_date("air", new Date(e["chk_in" + d]), "[m]/[d]/[Y]"));
            return !0
        },
        check_vp: function() {
            var c = this.options.vp,
                d = /^[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{4}$/,
                e = a(c.elements.from, c.$parent),
                f = a(c.elements.to, c.$parent),
                g = a("input[name=" + c.names.rs_o_cityid + "]", c.$parent),
                h = a("input[name=" + c.names.rs_d_cityid + "]", c.$parent),
                i = a("input[name=" + c.names.rs_o_aircode + "]", c.$parent),
                j = a("input[name=" + c.names.rs_d_aircode + "]", c.$parent),
                k = a("input[name=" + c.names.check_in + "]", c.$parent),
                l = a("input[name=" + c.names.check_out + "]", c.$parent),
                m = a("select[name=" + c.names.rooms + "]", c.$parent),
                n = a("select[name=" + c.names.adults + "]", c.$parent),
                o = a("select[name=" + c.names.children + "]", c.$parent),
                p = a(".childrens_ages", c.$parent),
                q = 0,
                r = "",
                s = 1,
                t = 0,
                u = "";
            if ((0 !== g.length && "" != g.val() || 0 !== i.length && "" != i.val()) && (0 !== h.length && "" != h.val() || 0 !== j.length && "" != j.val()) && e.val() != c.autosuggest.from_default_label && f.val() != c.autosuggest.to_default_label) this.url_string = c.action.results || this.options.cname + "/vp/results/?", a(c.$parent).attr("action", this.url_string);
            else if ("" != e.val() && e.val() != c.autosuggest.from_default_label && null !== e.val().match(/[^\s*]/) && "" != f.val() && f.val() != c.autosuggest.to_default_label && null !== f.val().match(/[^s*]/)) this.url_string = c.action.search || this.options.cname + "/vp/search/?", a(c.$parent).attr("action", this.url_string);
            else {
                if ("" == e.val() || e.val() == c.autosuggest.from_default_label || null === e.val().match(/[^\s*]/)) return this.check_alert("vp", a.i18n._t("Please enter an origin location to search for.")), e.val("").focus(), !1;
                if ("" == f.val() || f.val() == c.autosuggest.to_default_label || null === f.val().match(/[^\s*]/)) return this.check_alert("vp", a.i18n._t("Please enter a destination location to search for.")), f.val("").focus(), !1
            }
            return c.calendar.required && 0 == d.test(k.val()) ? (this.check_alert("vp", a.i18n._t("Please select a check in date.")), !1) : c.calendar.required && 0 == d.test(l.val()) ? (this.check_alert("vp", a.i18n._t("Please select a check out date.")), !1) : 0 != d.test(l.val()) && 0 != d.test(k.val()) && (this.string_to_date(l.val(), c.calendar.date_format) - this.string_to_date(k.val(), c.calendar.date_format)) / 864e5 > 21 ? this.options.allow_group_booking ? confirm(a.i18n._t("Sorry, vacation packages can only be booked for stays of 21 days or less.") + "\n" + a.i18n._t("If you wish to search for only a hotel, click OK, otherwise click CANCEL and modify your search.")) ? (c.popup && this.do_pop(f.val(), "&vp=true"), b.location.href = "" != this.options.group_booking && null != this.options.group_booking ? this.options.group_booking : "http://hotelsbycity.hotelplanner.com/Search/Index.cfm?City=" + f.val() + "&InDate=" + k.val() + "&Outdate=" + l.val() + "&NumRooms=" + m.val() + "&sc=HBC" + this.options.refid, !1) : !1 : (this.check_alert("vp", a.i18n._t("Sorry, reservations have a maximum length of 21 days.")), !1) : ((0 == d.test(l.val()) || 0 == d.test(k.val())) && (l.val(""), k.val("")), (parseInt(n.val(), 10) + parseInt(o.val(), 10)) / parseInt(m.val(), 10) > 4 && (q = Math.ceil((parseInt(n.val(), 10) + parseInt(o.val(), 10)) / parseInt(m.val(), 10) / 4)), 0 != q ? (this.check_alert("vp", a.i18n._t("Sorry, at most four guests can stay in each hotel room.") + "\n" + a.i18n._t("Please increase the number of rooms for your trip.") + "\n" + a.i18n._t("For the number of travelers you have selected, you will require at least " + q + " rooms.")), !1) : (r += 0 !== n.length && "" != n.val() ? n.val() : "1", u = parseInt(r), 0 !== p.length && p.each(function() {
                r += "^" + a(this).val(), 0 == a(this).val() ? (r += "-" + a(c.elements.infant_seating + " input[name=rs_infant" + s + "]:checked", c.$parent).val(), "lap" == a(c.elements.infant_seating + " input[name=rs_infant" + s + "]:checked", c.$parent).val() ? t++ : u++, s++) : u++
            }), t > parseInt(n.val(), 10) ? (this.check_alert("vp", a.i18n._t("Sorry, due to airline regulations, at most one lap infant is permitted per adult traveler.")), !1) : u > 8 ? (this.check_alert("vp", a.i18n._t("Sorry, at most 8 passengers can travel for a vacation package.")), !1) : (this.check_add(a("input[name=rs_passengers]", c.$parent), a(c.$parent), '<input name="rs_passengers" type="hidden" />', r), this.plugins.aaa_redirect && this.check_add(a("input[name=ace_redirect]", c.$parent), c.$parent, "<input name='ace_redirect' type='hidden' value='true'/>"), f.val() && c.popup && this.do_pop(f.val(), "&hotels=true"), "[m]/[d]/[Y]" != c.calendar.date_format && (c.chk_in && k.val(this.format_date("vp", new Date(c.chk_in), "[m]/[d]/[Y]")), c.chk_out && l.val(this.format_date("vp", new Date(c.chk_out), "[m]/[d]/[Y]"))), !0)))
        },
        re_add_air: function() {
            var b, c = this.options.air;
            if (c._private.removed) {
                for (b in c._private.removed) c._private.removed.round_trip ? c._private.removed.one_way ? c._private.removed.multi_dest || a(c.elements.multi_dest, c.$parent).after(c._private.removed[b]) : a(c.elements.one_way, c.$parent).after(c._private.removed[b]) : a(c.elements.round_trip, c.$parent).after(c._private.removed[b]);
                delete c._private.removed
            }
        },
        s_trk: function(b) {
            var c, d, e, f = {
                device_type: this.config.mobile ? 1 : 3,
                refid: this.options.refid,
                datetime: this.dates.now.getFullYear() + "-" + ("0" + (this.dates.now.getMonth() + 1)).slice(-2) + "-" + ("0" + this.dates.now.getDate()).slice(-2) + " " + this.dates.now.getHours() + ":" + this.dates.now.getMinutes() + ":" + this.dates.now.getSeconds(),
                check_in_date: "",
                uuid: uuid
            };
            if (!this.config.search_logging) return !1;
            if (!this.trk) return !1;
            if (this.trk[b]) {
                for (e in this.trk[b])
                    for (c in this.trk[b][e]) "" != this.trk[b][e][c].selected_id && (d = this.options[b].chk_in ? this.options[b].chk_in.split("/") : !1, f.check_in_date = d ? d[2] + "-" + ("0" + d[0]).slice(-2) + "-" + ("0" + d[1]).slice(-2) + " 00:00:00" : "", this.trk[b][e][c].typed_value = this.trk[b][e][c].typed_value.join("^"), this.trk[b][e][c].product = b, this.trk[b][e][c].input = e, a.ajax({
                        type: "POST",
                        url: this.init_getURL("trk"),
                        data: {
                            data: JSON.stringify(a.extend(!0, {}, f, this.trk[b][e][c]))
                        }
                    }));
                delete this.trk[b]
            }
        },
        s_add: function(a, b, c, d) {
            var e, f = {
                typed_value: [],
                selected_result_type: "",
                selected_result_id: "",
                selected_result_display_name: "",
                selected_result_position: ""
            };
            return this.config.search_logging ? (!b || -1 === b.indexOf("rs_o_city") && "rs_pu_city" != b ? !b || -1 === b.indexOf("rs_d_city") && "rs_do_city" != b && "query" != b && "rs_city" != b && "ss" != b || (b = "destination") : b = "origin", this.trk || (this.trk = {}), this.trk[a] || (this.trk[a] = {}), this.trk[a][b] || (this.trk[a][b] = [f]), e = this.trk[a][b].length - 1, void(c ? ("" != this.trk[a][b][e].selected_result_id && (this.trk[a][b].push(f), e++), this.trk[a][b][e].typed_value.push(c)) : d && "object" == typeof d && (this.trk[a][b][e].selected_result_type = d.type, this.trk[a][b][e].selected_result_id = d.id.toString(), this.trk[a][b][e].selected_result_display_name = d.name, this.trk[a][b][e].selected_result_position = d.position + 1))) : !1
        },
        destroy: function() {
            this.$el.removeData("_searchbox").removeAttr("data-_searchbox"), a("*", this.$el).off(".searchbox")
        }
    }, a.fn.searchbox = function(b, c) {
        return uuid || (uuid = f()), a.fn.searchbox.check_options(b) ? (b = a.extend(!0, {}, a.fn.searchbox.defaults, b), 1 != this.length ? (g({
            t: "event",
            ec: "searchbox error",
            ea: b.refid + ": " + location.href,
            el: 'The div "' + this.selector + '" does not exist.'
        }), h('The div "' + this.selector + '" does not exist.')) : this.each(function() {
            a.data(this, "_searchbox") || a.data(this, "_searchbox", new i(a(this), b, c))
        })) : !1
    }, a.fn.searchbox.check_options = function(a) {
        var b = !0,
            c = "";
        return a.refid && String(a.refid).match("^[0-9]{4}$") || (c += "You must specify a refid.", b = !1), a.hotel && a.hotel.enabled === !1 && a.car && a.car.enabled === !1 && a.air && a.air.enabled === !1 && a.vp && a.vp.enabled === !1 && (c += "You must enable at least one product.", b = !1), a.skip_ref && 1 == a.skip_ref && (1 != a.hotel.active && 1 != a.car.active && 1 != a.air.active && 1 != a.vp.active && (c += "Because you are skipping RefData, you must activate at least one product.", b = !1), a.accountid && String(a.accountid).match("^[0-9]{4}$") || (c += "Because you are skipping RefData, you must specify the accountid.", b = !1)), b || (g({
            t: "event",
            ec: "searchbox error",
            ea: (a.refid || "0000") + ": " + location.href,
            el: c
        }), h(c)), b
    }, a.fn.searchbox.get = function(b, c) {
        var e, f;
        if (b instanceof jQuery) return b.data("_searchbox") ? b.data("_searchbox") : !1;
        if (e = a("*").filter(function() {
                return a(this).data("_searchbox") !== d
            }), b || (h("Listing all serachboxes...", !1), h("----", !1)), e.length > 0) {
            if (b) return c ? e.data("_searchbox") : e;
            for (f = 0; f < e.length; f++) h(a(e[f]), !1), h(a(e[f]).data("_searchbox"), !1), h("----", !1)
        } else {
            if (b) return !1;
            h("No searchboxes found...", !1), h("----", !1)
        }
        return !0
    }, a.fn.searchbox.set = function(a, b, c) {
        var d, e, b = b.split(".") || !1;
        return 1 != a.length ? h('The div "' + a.selector + '" does not exist.') : a.data("_searchbox") ? !b || !c && null != c ? h("You must specify an option and a value.") : (d = a.data("_searchbox"), e = function(a, b, c) {
            if (b.length > 1) {
                var d = b.shift();
                e(a[d] = "[object Object]" == Object.prototype.toString.call(a[d]) ? a[d] : {}, b, c)
            } else a[b[0]] = c
        }, e(d, b, c), a.data("_searchbox", d), !0) : h('The div "' + a.selector + '" does not have a searchbox attached to it at this time. Maybe try on document.ready?')
    }, a.fn.searchbox.defaults = {
        hotel: {
            elements: {
                form: "hotel",
                autosuggest: ".autosuggest",
                chk_in: ".rs_chk_in",
                chk_out: ".rs_chk_out",
                rooms: ".rooms",
                search: ".search",
                required: !0
            },
            calendar: {
                next_day: !0,
                today: !1,
                pop_out: !0,
                months: 1,
                skip: 1,
                previous_display: "&larr;",
                next_display: "&rarr;",
                allow_same: !1,
                output_format: "[M] - [d] - [Y]",
                date_format: "[m]/[d]/[Y]",
                required: !1,
                callback: null,
                set_callback: null,
                close_callback: null,
                inputs: {}
            },
            autosuggest: {
                enabled: !0,
                callback: null,
                generate: !0,
                cities: !0,
                airports: !0,
                regions: !0,
                pois: !1,
                hotels: !1,
                num_hotels: 3,
                num_regions: 3,
                num_airports: 4,
                num_pois: 3,
                num_cities: 4,
                icon_cities: '<span class="rs_icon icon_city"></span>',
                icon_airports: '<span class="rs_icon icon_airport"></span>',
                icon_regions: '<span class="rs_icon icon_region"></span>',
                icon_pois: '<span class="rs_icon icon_poi"></span>',
                icon_hotels: '<span class="rs_icon icon_hotel"></span>',
                icon_location: '<span class="rs_icon icon_location"></span>',
                more_options: !0,
                airports_first: !1,
                default_label: a.i18n._t("Enter a City or Airport"),
                geolocation: !1,
                set_callback: null,
                plugin: !1,
                mobile_scroll: !0
            },
            action: {
                results: !1,
                hotel: !1,
                search: !1
            },
            names: {
                query: "query",
                city_id: "city_id",
                airport_code: "airport_code",
                region_id: "region_id",
                poi_id: "poi_id",
                hotel_id: "hotel_id",
                check_in: "check_in",
                check_out: "check_out",
                rooms: "rooms",
                adults: "adults",
                children: "children",
                latitude: "latitude",
                longitude: "longitude"
            },
            version: 3,
            autocomplete: !1,
            generate_rooms: !0,
            generate_guests: !0,
            default_rooms: 1,
            default_guests: 2,
            select_name: !1,
            pre_check: null,
            post_check: null,
            express_deals: !1,
            mk: null,
            cug_enabled: !1,
            popup: !0,
            ignore_alerts: !1,
            enabled: !0,
            active: !1
        },
        car: {
            elements: {
                form: "car",
                from: ".pickup",
                different_return: "#different_return",
                to: ".dropoff",
                chk_in: ".rs_chk_in",
                chk_out: ".rs_chk_out",
                time_in: ".rs_time_in",
                time_out: ".rs_time_out",
                search: ".search",
                required: !0
            },
            calendar: {
                next_day: !0,
                today: !1,
                pop_out: !0,
                months: 1,
                skip: 1,
                previous_display: "&larr;",
                next_display: "&rarr;",
                allow_same: !0,
                output_format: "[M] - [d] - [Y] - [g]:[i]:[s] [a]",
                date_format: "[m]/[d]/[Y]",
                time_format: "[g]:[i] [a]",
                generate_time: !0,
                pickup_time: "10:00",
                dropoff_time: "10:00",
                required: !0,
                callback: null,
                set_callback: null,
                close_callback: null,
                inputs: {}
            },
            autosuggest: {
                enabled: !0,
                callback: null,
                generate: !0,
                cities: !0,
                airports: !0,
                regions: !1,
                pois: !1,
                hotels: !1,
                num_hotels: 0,
                num_regions: 0,
                num_airports: 4,
                num_pois: 0,
                num_cities: 4,
                icon_cities: '<span class="rs_icon icon_city"></span>',
                icon_airports: '<span class="rs_icon icon_airport"></span>',
                icon_regions: '<span class="rs_icon icon_region"></span>',
                icon_pois: '<span class="rs_icon icon_poi"></span>',
                icon_hotels: '<span class="rs_icon icon_hotel"></span>',
                icon_location: '<span class="rs_icon icon_location"></span>',
                more_options: !0,
                airports_first: !1,
                from_default_label: a.i18n._t("Enter a City or Airport"),
                to_default_label: a.i18n._t("Enter a City or Airport"),
                geolocation: !1,
                set_callback: null,
                plugin: !1,
                mobile_scroll: !0
            },
            action: {
                results: !1,
                search: !1
            },
            names: {
                rs_pu_cityid: "rs_pu_cityid",
                rs_do_cityid: "rs_do_cityid",
                rs_pu_airport: "rs_pu_airport",
                rs_do_airport: "rs_do_airport",
                check_in: "rs_pu_date",
                check_out: "rs_do_date"
            },
            version: 3,
            autocomplete: !1,
            pre_check: null,
            post_check: null,
            select_name: !1,
            popup: !0,
            ignore_alerts: !1,
            enabled: !0,
            active: !1
        },
        air: {
            elements: {
                form: "air",
                round_trip: "#air_round_trip",
                one_way: "#air_one_way",
                multi_dest: "#air_multi_dest",
                from: ".from",
                to: ".to",
                chk_in: ".rs_chk_in",
                chk_out: ".rs_chk_out",
                adults: ".rs_adults",
                children: ".rs_children",
                search: ".search",
                required: !0
            },
            calendar: {
                next_day: !0,
                today: !1,
                pop_out: !0,
                months: 1,
                skip: 1,
                previous_display: "&larr;",
                next_display: "&rarr;",
                allow_same: !0,
                output_format: "[M] - [d] - [Y]",
                date_format: "[m]/[d]/[Y]",
                required: !0,
                callback: null,
                set_callback: null,
                close_callback: null,
                inputs: {}
            },
            autosuggest: {
                enabled: !0,
                callback: null,
                generate: !0,
                cities: !0,
                airports: !0,
                regions: !1,
                pois: !1,
                hotels: !1,
                num_hotels: 0,
                num_regions: 0,
                num_airports: 4,
                num_pois: 0,
                num_cities: 4,
                icon_cities: '<span class="rs_icon icon_city"></span>',
                icon_airports: '<span class="rs_icon icon_airport"></span>',
                icon_regions: '<span class="rs_icon icon_region"></span>',
                icon_pois: '<span class="rs_icon icon_poi"></span>',
                icon_hotels: '<span class="rs_icon icon_hotel"></span>',
                icon_location: '<span class="rs_icon icon_location"></span>',
                more_options: !1,
                airports_first: !1,
                from_default_label: a.i18n._t("Enter a City or Airport"),
                to_default_label: a.i18n._t("Enter a City or Airport"),
                geolocation: !1,
                set_callback: null,
                plugin: !1,
                mobile_scroll: !0
            },
            action: {
                results: !1,
                search: !1
            },
            names: {
                rs_o_aircode: "rs_o_aircode",
                rs_d_aircode: "rs_d_aircode",
                rs_o1_aircode: "rs_o_aircode1",
                rs_d1_aircode: "rs_d_aircode1",
                rs_o2_aircode: "rs_o_aircode2",
                rs_d2_aircode: "rs_d_aircode2",
                rs_o3_aircode: "rs_o_aircode3",
                rs_d3_aircode: "rs_d_aircode3",
                rs_o4_aircode: "rs_o_aircode4",
                rs_d4_aircode: "rs_d_aircode4",
                rs_o5_aircode: "rs_o_aircode5",
                rs_d5_aircode: "rs_d_aircode5",
                check_in: "rs_chk_in",
                check_out: "rs_chk_out",
                check_in1: "rs_chk_in1",
                check_in2: "rs_chk_in2",
                check_in3: "rs_chk_in3",
                check_in4: "rs_chk_in4",
                check_in5: "rs_chk_in5",
                adults: "rs_adults",
                children: "rs_children"
            },
            version: 3,
            generate_guests: !0,
            default_guests: 1,
            default_children: 0,
            autocomplete: !1,
            select_name: !1,
            pre_check: null,
            post_check: null,
            popup: !0,
            ignore_alerts: !1,
            enabled: !0,
            active: !1
        },
        vp: {
            elements: {
                form: "vp",
                from: ".from",
                to: ".to",
                chk_in: ".rs_chk_in",
                chk_out: ".rs_chk_out",
                rooms: ".rooms",
                adults: ".rs_adults_input",
                children: ".rs_child_input",
                children_ages: "#childrens_ages",
                infant_seating: "#infant_seating",
                search: ".search",
                required: !0
            },
            calendar: {
                next_day: !0,
                today: !1,
                pop_out: !0,
                months: 1,
                skip: 1,
                previous_display: "&larr;",
                next_display: "&rarr;",
                allow_same: !1,
                output_format: "[M] - [d] - [Y]",
                date_format: "[m]/[d]/[Y]",
                required: !0,
                callback: null,
                set_callback: null,
                close_callback: null,
                inputs: {}
            },
            autosuggest: {
                enabled: !0,
                callback: null,
                generate: !0,
                cities: !0,
                airports: !0,
                regions: !1,
                pois: !1,
                hotels: !1,
                num_hotels: 0,
                num_regions: 0,
                num_airports: 4,
                num_pois: 0,
                num_cities: 4,
                icon_cities: '<span class="rs_icon icon_city"></span>',
                icon_airports: '<span class="rs_icon icon_airport"></span>',
                icon_regions: '<span class="rs_icon icon_region"></span>',
                icon_pois: '<span class="rs_icon icon_poi"></span>',
                icon_hotels: '<span class="rs_icon icon_hotel"></span>',
                icon_location: '<span class="rs_icon icon_location"></span>',
                more_options: !0,
                airports_first: !1,
                from_default_label: a.i18n._t("Enter a City or Airport"),
                to_default_label: a.i18n._t("Enter a City or Airport"),
                geolocation: !1,
                set_callback: null,
                plugin: !1,
                mobile_scroll: !0
            },
            action: {
                results: !1,
                search: !1
            },
            names: {
                rs_o_cityid: "rs_o_cityid",
                rs_d_cityid: "rs_d_cityid",
                rs_o_aircode: "rs_o_aircode",
                rs_d_aircode: "rs_d_aircode",
                check_in: "rs_chk_in",
                check_out: "rs_chk_out",
                rooms: "rs_rooms",
                adults: "rs_adults",
                children: "rs_children"
            },
            version: 2,
            autocomplete: !1,
            generate_guests: !0,
            generate_rooms: !0,
            default_rooms: 1,
            default_guests: 1,
            select_name: !1,
            pre_check: null,
            post_check: null,
            popup: !0,
            ignore_alerts: !1,
            enabled: !0,
            active: !1
        },
        refid: 2050,
        cname: "//secure.rezserver.com",
        protocol: !1,
        skip_ref: !1,
        filter_city: !1,
        filter_country: !1,
        filter_state: !1,
        group_booking: null,
        allow_group_booking: !0,
        vcid: null,
        pet_friendly: !1,
        environment: "local",
        backend: !1,
        open_window: !1,
        iframe: !1
    }
}(jQuery, window, document);