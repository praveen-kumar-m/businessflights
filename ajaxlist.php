<?php
$unique_id = $_GET[ 'uniqueId' ];

$ipaddress = $this->get_client_ip();

$ResStatus = '';

if ( isset( $_SESSION[ $unique_id ][ 'sessionkey' ] ) && $_SESSION[ $unique_id ][ 'sessionkey' ] ):

  $apikey = 'as901892827258298792949665487652';
//$apikey = 'prtl6749387986743898559646983194';

//$filter = '&pageindex='.$_SESSION[$uniqueId]['pageindex'].'&pagesize=50';

if ( !$_GET[ 'loadmore' ] ) {

  $sorting = '';

  if ( isset( $_GET[ 'sorttype' ] ) ) {
    $sorting = '&sorttype=' . $_GET[ 'sorttype' ] . '&sortorder=' . ( isset( $_GET[ 'sortorder' ] ) ? $_GET[ 'sortorder' ] : 'asc' );
  } else {
    $sorting = '&sorttype=price&sortorder=' . ( isset( $_GET[ 'sortorder' ] ) ? $_GET[ 'sortorder' ] : 'asc' );
  }

  $filter = '';

  if ( isset( $_GET[ 'stops' ] ) && $_GET[ 'stops' ] != '' ):
    $filter .= '&stops=' . max( explode( ',', $_GET[ 'stops' ] ) );
  $sorting = '&sorttype=price&sortorder=asc';
  endif;

  if ( isset( $_GET[ 'outbounddeparttime' ] ) && $_GET[ 'outbounddeparttime' ] != '' ):
    $filter .= '&outbounddeparttime=' . $_GET[ 'outbounddeparttime' ];
  $sorting = '&sorttype=price&sortorder=asc';
  endif;

  if ( isset( $_GET[ 'excludecarriers' ] ) && $_GET[ 'excludecarriers' ] != '' ):
    $filter .= '&excludecarriers=' . $_GET[ 'excludecarriers' ];
  $sorting = '&sorttype=price&sortorder=asc';
  endif;

  if ( $sorting == '&sorttype=&sortorder=' )$sorting = '&sorttype=price&sortorder=asc';

  if ( isset( $_SESSION[ $unique_id ][ 'get' ][ 'test' ] ) && $_SESSION[ $unique_id ][ 'get' ][ 'test' ] ):
    if ( $_GET[ 'loadmore' ] ) {
      echo "https://partners.api.skyscanner.net/apiservices/pricing/v1.0/" . $_SESSION[ $unique_id ][ 'sessionkey' ] . "?apikey=" . $apikey . '&pageindex=' . $_GET[ 'loadmore' ] . '&pagesize=20' . $sorting . $filter;
    } else {
      echo "https://partners.api.skyscanner.net/apiservices/pricing/v1.0/" . $_SESSION[ $unique_id ][ 'sessionkey' ] . "?apikey=" . $apikey . $sorting . $filter;
    }
  endif;

  $che = curl_init();

  if ( $_GET[ 'loadmore' ] ) {
    curl_setopt( $che, CURLOPT_URL, "https://partners.api.skyscanner.net/apiservices/pricing/v1.0/" . $_SESSION[ $unique_id ][ 'sessionkey' ] . "?apikey=" . $apikey . '&pageindex=' . $_GET[ 'loadmore' ] . '&pagesize=20' . $sorting . $filter );
  } else {
    curl_setopt( $che, CURLOPT_URL, "https://partners.api.skyscanner.net/apiservices/pricing/v1.0/" . $_SESSION[ $unique_id ][ 'sessionkey' ] . "?apikey=" . $apikey . $sorting . $filter );
  }
  curl_setopt( $che, CURLOPT_RETURNTRANSFER, true );
  curl_setopt( $che, CURLOPT_POST, false );
  curl_setopt( $che, CURLOPT_HEADER, false );
  curl_setopt( $che, CURLOPT_VERBOSE, 1 );
  $result = curl_exec( $che );
  //echo $che;

  /*bubble buttons code start*/

  $resultsc = json_decode( $result, true );
  //echo '<pre>';

  function compare( $a, $b ) {
    if ( $a == $b ) {
      return 0;
    }
    return ( $a > $b ) ? -1 : 1;
  }

  function convertToHoursMins( $time, $format = '%02d:%02d' ) {
    if ( $time < 1 ) {
      return;
    }
    $hours = floor( $time / 60 );
    $minutes = ( $time % 60 );
    return sprintf( $format, $hours, $minutes );
  }

  function compareFlights( & $resultsc ) {
    foreach ( $resultsc[ 'Itineraries' ] as $deepPriceResults ) {
      $randHash = bin2hex( mcrypt_create_iv( 22, MCRYPT_DEV_URANDOM ) );
      $thisOutboundLegId[ $randHash ] = $deepPriceResults[ 'OutboundLegId' ];
      if ( isset( $deepPriceResults[ 'InboundLegId' ] ) ) {
        $thisInboundLegId[ $randHash ] = $deepPriceResults[ 'InboundLegId' ];
      }

      $lowestprice[ $randHash ] = '1000000000';
      foreach ( $deepPriceResults[ 'PricingOptions' ] as $pricevalues ) {
        if ( $lowestprice[ $randHash ] > $pricevalues[ 'Price' ] )$lowestprice[ $randHash ] = $pricevalues[ 'Price' ];
      }
    }

    foreach ( $resultsc[ 'Legs' ] as $deepDurationResults ) {
      $segmentID = $deepDurationResults[ 'Id' ];
      $segmentDuration[ $segmentID ] = $deepDurationResults[ 'Duration' ];
    }

    uasort( $lowestprice, 'compare' );
    end( $lowestprice );
    $lastKey = key( $lowestprice );
    $totalflighttime = $segmentDuration[ $thisOutboundLegId[ $lastKey ] ];
    $divideby = 1;
    if ( isset( $thisInboundLegId[ $lastKey ] ) ) {
      $totalflighttime += $segmentDuration[ $thisInboundLegId[ $lastKey ] ];
      $divideby = 2;
      $dividebytext = ' <small>(average)</small>';
    }

    foreach ( $lowestprice as $key => $value ) {
      $shortestFlightTime[ $key ] = $segmentDuration[ $thisOutboundLegId[ $key ] ];
      if ( isset( $thisInboundLegId[ $key ] ) )$shortestFlightTime[ $key ] += $segmentDuration[ $thisInboundLegId[ $key ] ];
    }

    uasort( $shortestFlightTime, 'compare' );
    end( $shortestFlightTime );
    $lastDurationKey = key( $shortestFlightTime );
    $keyDSearch = array_keys( $shortestFlightTime, $shortestFlightTime[ $lastDurationKey ] );

    if ( count( $keyDSearch ) > 1 ) {
      foreach ( $keyDSearch as $keyDSearchVAL ) {
        if ( $lowestprice[ $keyDSearchVAL ] < $lowestprice[ $lastDurationKey ] )$lowestprice[ $lastDurationKey ] = $lowestprice[ $keyDSearchVAL ];
      }
    }

    $shortestFlightTime[ $lastDurationKey ] = $shortestFlightTime[ $lastDurationKey ] / $divideby;
    $totalflighttime = $totalflighttime / $divideby;
    if ( isset( $shortestFlightTime[ $lastDurationKey ] ) && $shortestFlightTime[ $lastDurationKey ] != '' )echo '<li class="bubbleresults"><div class="shortest"><a href="javascript:;" class="bubbleshort"><span class="title">View Shortest Flight</span><span class="value">' . convertToHoursMins( $shortestFlightTime[ $lastDurationKey ], '%02dh %02dm' ) . $dividebytext . '</span><span class="details">Price:$ ' . number_format( $lowestprice[ $lastDurationKey ], 2 ) . '</span></a></div><div class="cheapest"><a href="javascript:;" class="bubblecheap"><span class="title">View Cheapest Flight</span><span class="value">$ ' . number_format( $lowestprice[ $lastKey ], 2 ) . '</span><span class="details">Duration ' . convertToHoursMins( $totalflighttime, '%02dh %02dm' ) . $dividebytext . '</span></a></div></li>';
  }

  //disabled temporary
  if ( is_array( $resultsc ) )compareFlights( $resultsc );

  /*bubble buttons code end*/

  $results = json_decode( $result );

  if ( isset( $_SESSION[ $unique_id ][ 'get' ][ 'test' ] ) && $_SESSION[ $unique_id ][ 'get' ][ 'test' ] ):

    echo '<pre>';
  print_r( $results );
  echo '</pre>';

  endif;

  if ( isset( $results->Status ) && ( string )trim( $results->Status ) == 'UpdatesComplete' ):
    if ( !$_GET[ 'loadmore' ] ):
      file_put_contents( 'response/res' . $ipaddress . $unique_id . '.txt', $result );
  endif;
  else :
    if ( isset( $results->Status ) && ( string )trim( $results->Status ) == 'UpdatesPending' ):
      $ResStatus = 'UpdatesPending';
    else :
      if ( empty( $result ) ):
        $ResStatus = 'UpdatesPending';
      else :
        $ResStatus = 'noresults';
  die();
  endif;
  endif;
  endif;

} else {
  $results = json_decode( file_get_contents( 'response/res' . $ipaddress . $unique_id . '.txt' ) );
}

//

//Functions
function ModifyArray( $object, $element ) {

  $funArray = array();

  if ( isset( $object->$element ) && count( $object->$element ) > 0 ): //Legs

    foreach ( $object->$element as $list ):

      $funArray[ $list->Id ] = $list;

  endforeach;

  endif;

  return $funArray;

}

//$results = array(); //Variables

$legs = array();

$Itineraries = array();

$Segments = array();

$Carriers = array();

$Agents = array();

$Places = array();

$Itineraries = ( isset( $results->Itineraries ) ? $results->Itineraries : '0' ); //Itineraries

if ( $_GET[ 'loadmore' ] ) {
  $Itineraries = array_slice( $Itineraries, $_GET[ 'loadmore' ] - 1, 20 );
}

$Legs = ModifyArray( $results, 'Legs' ); //Legs

$Segments = ( isset( $results->Segments ) ? $results->Segments : '' ); //Segments

$Carriers = ModifyArray( $results, 'Carriers' ); //Carriers

$Agents = ModifyArray( $results, 'Agents' ); //Agents

$Places = ModifyArray( $results, 'Places' ); //Places

$TotalFlight = 0;

if ( isset( $Itineraries ) && count( $Itineraries ) > 0 ):

  $TotalFlight = count( $Itineraries );

endif;

$airlineNameAndCodes = array();

if ( isset( $Carriers ) && $Carriers ):

  endif;

//$unique_id = uniqid(); // Generate for ajax values

//$_SESSION[$unique_id] = array();

//$_SESSION[$unique_id]['get'] = $_GET;

$MINUTES_PER_HOUR = '60';

$Itiner_Cnt = 0;

if ( $Itineraries ):

  if ( $_SESSION[ $unique_id ][ 'way' ] == 1 ) {

    if ( $Itineraries ):
      foreach ( $Itineraries as $key => $list ):

        ++$Itiner_Cnt;

    if ( !$_GET[ 'loadmore' ] ) {
      if ( $key > 20 ):
        break;
      endif;
    }

    $Outbound = array(); //Variable

    $Outbound = $Legs[ ( string )$list->OutboundLegId ]; //Set Values

    $out_Origin = array();
    $out_Destination = array();
    $out_Segment = array();
    $out_Departure = '';
    $out_Arrival = '';
    $out_Duration = '';

    $out_Origin = $Places[ ( string )$Outbound->OriginStation ];
    $out_Destination = $Places[ ( string )$Outbound->DestinationStation ];
    $out_Departure = $Outbound->Departure;
    $out_Arrival = $Outbound->Arrival;
    $out_Duration = $Outbound->Duration;
    if ( isset( $Outbound->SegmentIds ) && count( $Outbound->SegmentIds ) > 0 ):
      foreach ( $Outbound->SegmentIds as $Ids ):
        $out_Segment[ $Ids ] = $Segments[ $Ids ];
    endforeach;
    endif;

    $out_Stop = 0; //Stop
    $out_Stop_Text = 'Direct';

    if ( isset( $Outbound->Stops ) && count( $Outbound->Stops ) > 0 ):
      $out_Stop = count( $Outbound->Stops );
    endif;

    if ( $out_Stop > 0 ):
      $out_Stop_Text = $out_Stop . ' Stop';
    endif;

    $out_hours = 0; //Hours and Minutes
    $out_minutes = 0;

    $out_hours = intval( $out_Duration / $MINUTES_PER_HOUR );
    $out_minutes = $out_Duration % $MINUTES_PER_HOUR;

    $out_hours = ( strlen( $out_hours ) >= 2 ? $out_hours : '0' . $out_hours );
    $out_minutes = ( strlen( $out_minutes ) >= 2 ? $out_minutes : '0' . $out_minutes );

    $out_Airline = array(); //Airline

    if ( isset( $Outbound->Carriers ) && count( $Outbound->Carriers ) > 0 ):
      $out_Airline = $Carriers[ $Outbound->Carriers[ 0 ] ];
    endif;

    $price = 0;
    $AgentName = '';
    $Agent_DeeplinkUrl = '';

    if ( isset( $list->PricingOptions[ 0 ]->Price ) && $list->PricingOptions[ 0 ]->Price ):

      $price = $list->PricingOptions[ 0 ]->Price;
    $AgentName = $Agents[ $list->PricingOptions[ 0 ]->Agents[ 0 ] ]->Name;
    $Agent_DeeplinkUrl = $list->PricingOptions[ 0 ]->DeeplinkUrl;

    endif;

    $filter_stops = array(); //Filter Data
    $filter_stops[] = $out_Stop;
    //$filter_stops[] = $in_Stop

    ?>
<li class="FlightList" <?php echo ($Itiner_Cnt > 20 ? 'style="display:none;"': ''); ?>>
  <div class="EachFlightRow" data-stop="<?php echo max($filter_stops); ?>" data-order="<?php echo $Itiner_Cnt; ?>" data-name="<?php echo $out_Airline->Name; ?>" data-price="<?php echo str_replace(',','.',number_format($price,2)); ?>" data-from="<?php echo strtotime($out_Departure); ?>" data-to="<?php echo strtotime($out_Arrival); ?>" data-duration="<?php echo $out_Duration; ?>" data-obid="<?=$list->OutboundLegId?>" data-ibid="<?=$list->InboundLegId?>">
    <div class="flightrow moreresult-dialog" data-rel="<?php echo $Itiner_Cnt;?>"> 
      <!-- New Style Start-->
      <div class="newview<?php echo $Itiner_Cnt;?> Flightsearchnew" style="display:none;" id="nv<?=md5(rand())?>">
        <div class="NewHead">
          <div class="Newbacktoresult closenew" data-rel="<?php echo $Itiner_Cnt;?>">
            <h4>Back to Results</h4>
          </div>
          <div class="Flightheaddetails">
            <h3>Flight Details</h3>
          </div>
          <div class="Newclose closenew" data-rel="<?php echo $Itiner_Cnt;?>"> <a href="javascript:void(0);" class="closenew"><img src="images/csclose.svg" width="22" height="22" alt="close icons"></a> </div>
        </div>
        <div class="flightrownew">
          <div class="flightll pull-left colleft" style="padding:0">
            <div class="booking-item-container active">
              <div class="booking-item popup-text pull-left itinerarypop">
								<div class="border">
									<div class="Newpoup">
										<div class="itinerarywrapper">
											<h5><?php echo $out_Origin->Name; ?>&mdash;<?php echo $out_Destination->Name;?></h5>
										</div>
									</div>
								</div>
							</div>
              <div class="booking-item popup-text pull-left">
                <div class="border">
                  <div class="Newpoup">
                    <div class="Firstadvs">
                      <div class="agentdetails">
                        <h6 class="lowhid">Book it on <?php echo $AgentName; ?></h6>
                      </div>
                      <div class="pull-left flighrr book-price"> <span class="booking-item-price">$ <?php echo number_format($price,2); ?></span><span class="perpp"><!--/person--> 
                        <!--<p class="booking-item-flight-class"><span class="lowhid">Class: </span>Economy</p>--> 
                        <a class="btn btn-primary alldeal moreresult-dialog" href="<?php echo $Agent_DeeplinkUrl; ?>" >Book</a> </span>
											</div>
											<div class="mobileonly">Lowest price on <strong><?php echo $AgentName; ?></strong></div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <?php
                  if ( isset( $Outbound->SegmentIds ) && count( $Outbound->SegmentIds ) > 0 ):
                    if ( $Segments[ $Outbound->SegmentIds[ 0 ] ]->Carrier != $Segments[ $Outbound->SegmentIds[ 0 ] ]->OperatingCarrier ):
                      ?>
                  <!--p>Partly operated by <b><?php echo $Carriers[$Segments[$Outbound->SegmentIds[0]]->OperatingCarrier]->Name; ?></b></p-->
                  <?php
                  endif;
                  endif;
                  ?>
                  <div class="booking-item-details">
                    <div class="infopad">
                      <div class="dlbox">
                        <?php

                        $segCnt = 0;
                        $Segment_keys = array();
                        $Segment_keys = array_keys( $out_Segment );

                        if ( isset( $out_Segment ) && count( $out_Segment ) ):
                          foreach ( $out_Segment as $seg_key => $seg ):

                            ++$segCnt;

                        $seg_Origin = array();
                        $seg_Destination = array();
                        $seg_Departure = '';
                        $seg_Arrival = '';
                        $seg_Duration = '';
                        $seg_Carrier = array();
                        $seg_OperatingCarrier = array();
                        $seg_hours = 0; //Hours and Minutes
                        $seg_minutes = 0;

                        $seg_Origin = $Places[ ( string )$seg->OriginStation ];
                        $seg_Destination = $Places[ ( string )$seg->DestinationStation ];
                        $seg_Departure = $seg->DepartureDateTime;
                        $seg_Arrival = $seg->ArrivalDateTime;
                        $seg_Duration = $seg->Duration;
                        $seg_Carrier = $Carriers[ ( string )$seg->Carrier ];
                        if ( ( string )$seg->Carrier != $seg->OperatingCarrier ):
                          $seg_OperatingCarrier = $Carriers[ ( string )$seg->OperatingCarrier ];
                        endif;
                        $seg_hours = intval( $seg_Duration / $MINUTES_PER_HOUR );
                        $seg_minutes = $seg_Duration % $MINUTES_PER_HOUR;

                        $seg_hours = ( strlen( $seg_hours ) >= 2 ? $seg_hours : '0' . $seg_hours );
                        $seg_minutes = ( strlen( $seg_minutes ) >= 2 ? $seg_minutes : '0' . $seg_minutes );

                        $Con_Airport[ $segCnt ][ 'Departure' ] = $seg_Departure;
                        $Con_Airport[ $segCnt ][ 'Arrival' ] = $seg_Arrival;

                        $seg_Con_hours = 0; //Connecting Hours and Minutes
                        $seg_Con_minutes = 0;

                        //Connecting an Airport Hours and Minutes
                        $key = array_search( $seg_key, $Segment_keys );

                        if ( false !== $key ):
                          unset( $Segment_keys[ $key ] );
                        endif;

                        if ( isset( $out_Segment[ current( $Segment_keys ) ]->Id ) && $out_Segment[ current( $Segment_keys ) ]->Id ):
                          $start = '';
                        $end = '';
                        $diff = '';

                        $start = date_create( $out_Segment[ current( $Segment_keys ) ]->DepartureDateTime );
                        $end = date_create( $seg_Arrival );

                        $diff = date_diff( $end, $start );

                        $seg_Con_hours = ( strlen( $diff->h ) >= 2 ? $diff->h : '0' . $diff->h );
                        $seg_Con_minutes = ( strlen( $diff->i ) >= 2 ? $diff->i : '0' . $diff->i );

                        endif;

                        if ( $segCnt == 1 ):
                          ?>
                        <div class="row nomargin depart">
                          <h5><b>DEPART</b> <?php echo date("D, d M Y", strtotime($seg_Departure)); ?></h5>
                        </div>
                        <?php
                        endif;
                        ?>
                        <div class="row nomargin">
                          <div class="flightnewinfo">
                            <div class="Firstinfolight">
                              <div class="Flightstation">
                                <h4><?php echo '('.$seg_Origin->Code.') ';?></h4>
                                <span><?php echo $Places[$seg_Origin->ParentId]->Name.', '.$seg_Origin->Name; ?></span> </div>
                              <div class="FlightTiming">
                                <h4><?php echo date("H:i", strtotime($seg_Departure)); ?></h4>
                                <span><?php echo date("D, d M Y", strtotime($seg_Departure)); ?></span> </div>
                              <div class="Flightcode"> <span><?php echo $seg_Carrier->Name; ?></span> <img src="<?php echo str_replace('http://','//',$seg_Carrier->ImageUrl); ?>" alt="<?php echo $out_Airline->Name; ?>" title="<?php echo $out_Airline->Name; ?>" class="lufthansa" /> <span><?php echo $seg_Carrier->DisplayCode.$seg->FlightNumber; ?></span> </div>
                            </div>
                            <div class="Secondinfolight">
                              <div class="Flightstation">
                                <h4><?php echo '('.$seg_Destination->Code.') ';?></h4>
                                <span><?php echo $Places[$seg_Destination->ParentId]->Name.', '.$seg_Destination->Name; ?></span> </div>
                              <div class="FlightTiming">
                                <h4><?php echo date("H:i", strtotime($seg_Arrival)); ?></h4>
                                <span><?php echo date("D, d M Y", strtotime($seg_Arrival)); ?></span> </div>
                              <div class="Flightcode"><h4>Duration</h4><?php echo $seg_hours; ?>h <?php echo $seg_minutes; ?></div>
                            </div>
                          </div>
                        </div>
                        <?php
                        if ( count( $seg_OperatingCarrier ) ):
                          ?>
                        <div class="row nomargint">
                          <p>Partly operated by <b><?php echo $seg_OperatingCarrier->Name; ?></b></p>
                        </div>
                        <?php
                        endif;
                        if ( $segCnt < count( $out_Segment ) ):
                          ?>
                        <div class="row nomargin connect-airport">
                          <div class="pull-left">
                            <p>Connect in airport <b><?php echo '('.$seg_Destination->Code.') '.$Places[$seg_Destination->ParentId]->Name; ?></b></p>
                          </div>
                          <div class="pull-right"> <b>Layover</b>&nbsp;&nbsp; <span><?php echo $seg_Con_hours; ?>h <?php echo $seg_Con_minutes; ?></span> </div>
                        </div>
                        <?php
                        endif;
                        if ( count( $out_Segment ) == $segCnt ):
                          ?>
                        <div class="row arrive">
                          <div class="col-xs-6">
                            <ul class="list">
                              <li>Arrives <?php echo date("D, d M Y", strtotime($seg_Arrival)); ?></li>
                            </ul>
                          </div>
                          <div class="col-xs-6 text-right">
                            <ul class="list list-inline">
                              <li>Journey Duration: <?php echo $out_hours; ?>h <?php echo $out_minutes; ?>m</li>
                            </ul>
                          </div>
                        </div>
                        <?php
                        endif;
                        ?>
                        <?php
                        endforeach;
                        endif;
                        ?>
                      </div>
                      <?php
                      if ( count( $list->PricingOptions ) ):
                        ?>
                      <div class="agent-deals">
                        <h4>More Booking Options</h4>
                        <ul class="bookinglinkwrapper">
                          <?php
                          foreach ( $list->PricingOptions as $AList ):
                            ?>
                          <li> <span class="agentimage"><img style="width:auto; height:55px;" src="<?php echo str_replace('http://','//',$Agents[$AList->Agents[0]]->ImageUrl); ?>" alt="<?php echo $Agents[$AList->Agents[0]]->Name; ?>"></span><span class="agentname"><?php echo $Agents[$AList->Agents[0]]->Name;; ?></span><span class="agentprice">$ <?php echo number_format($AList->Price,2); ?></span><a target="_blank" class="btn bookbt btn-primary" href="<?php echo $AList->DeeplinkUrl; ?>">Book</a> </li>
                          <?php
                          endforeach;
                          ?>
                        </ul>
                        <span class="addbags"><a href="https://www.discountflights.com/airline-baggage-fees.php" target="_blank">Additional bag fees may apply</a></span> 
											</div>
                      <?php
                      endif;
                      ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
      
      <!-- New Style End -->
      
      <div class="flightll pull-left colleft" style="padding:0">
        <div class="booking-item-container">
          <div class="booking-item">
            <div class="border">
              <div class="airlinelogos pull-left">
                <div class="alogo">
                  <div class="booking-item-airline-logo"> <img src="<?php echo str_replace('http://','//',$out_Airline->ImageUrl); ?>" alt="<?php echo $out_Airline->Name; ?>" title="<?php echo $out_Airline->Name; ?>" class="lufthansa" /> 
                    <!--<p>Lufthansa</p>--> 
                  </div>
                </div>
              </div>
              <div class="flightintro pull-left">
                <div class="row">
                  <div class="fileft pull-left">
                    <div class="booking-item-flight-details" id="arrow">
                      <div class="booking-item-departure"><!-- <i class="fa fa-plane"></i> --> 
                        <span class="booking-item-date"><?php echo date("H:i", strtotime($out_Departure)); ?></span> <span class="jfk">(<?php echo $out_Origin->Code; ?>)</span> </div>
                      <div class="arrowctrl">
                        <div class="linedot <?php echo ($out_Stop > 1 ? 'twostop' : 'onestop'); ?> pull-left"> <span class="arrowline"></span>
                          <?php
                          for ( $i = 1; $i <= $out_Stop; $i++ ):
                            echo '<div class="stopcircle stop' . $i . '"> </div>';
                          endfor;
                          ?>
                        </div>
                        <img src="images/arrow-end.png" class="arrow-small">
                        <div class="clearfix"></div>
                      </div>
                      <div class="booking-item-arrival arrive"><!-- <i class="fa fa-plane fa-flip-vertical"></i> --> 
                        <span class="booking-item-date"><?php echo date("H:i", strtotime($out_Arrival)); ?></span> <span class="jfk">(<?php echo $out_Destination->Code; ?>)</span> </div>
                    </div>
                  </div>
                  <div class="firight pull-left"> <span><?php echo $out_hours; ?>h <?php echo $out_minutes; ?>m</span> <span class="stops"><?php echo $out_Stop_Text; ?></span> </div>
                </div>
              </div>
              <div class="clearfix"></div>
              <?php
              if ( isset( $Outbound->SegmentIds ) && count( $Outbound->SegmentIds ) > 0 ):
                if ( $Segments[ $Outbound->SegmentIds[ 0 ] ]->Carrier != $Segments[ $Outbound->SegmentIds[ 0 ] ]->OperatingCarrier ):
                  ?>
              <p>Partly operated by <b><?php echo $Carriers[$Segments[$Outbound->SegmentIds[0]]->OperatingCarrier]->Name; ?></b></p>
              <?php
              endif;
              endif;
              ?>
              <div class="booking-item-details">
                <div class="infopad">
                  <div class="dlbox">
                    <?php

                    $segCnt = 0;
                    $Segment_keys = array();
                    $Segment_keys = array_keys( $out_Segment );

                    if ( isset( $out_Segment ) && count( $out_Segment ) ):
                      foreach ( $out_Segment as $seg_key => $seg ):

                        ++$segCnt;

                    $seg_Origin = array();
                    $seg_Destination = array();
                    $seg_Departure = '';
                    $seg_Arrival = '';
                    $seg_Duration = '';
                    $seg_Carrier = array();
                    $seg_OperatingCarrier = array();
                    $seg_hours = 0; //Hours and Minutes
                    $seg_minutes = 0;

                    $seg_Origin = $Places[ ( string )$seg->OriginStation ];
                    $seg_Destination = $Places[ ( string )$seg->DestinationStation ];
                    $seg_Departure = $seg->DepartureDateTime;
                    $seg_Arrival = $seg->ArrivalDateTime;
                    $seg_Duration = $seg->Duration;
                    $seg_Carrier = $Carriers[ ( string )$seg->Carrier ];
                    if ( ( string )$seg->Carrier != $seg->OperatingCarrier ):
                      $seg_OperatingCarrier = $Carriers[ ( string )$seg->OperatingCarrier ];
                    endif;
                    $seg_hours = intval( $seg_Duration / $MINUTES_PER_HOUR );
                    $seg_minutes = $seg_Duration % $MINUTES_PER_HOUR;

                    $seg_hours = ( strlen( $seg_hours ) >= 2 ? $seg_hours : '0' . $seg_hours );
                    $seg_minutes = ( strlen( $seg_minutes ) >= 2 ? $seg_minutes : '0' . $seg_minutes );

                    $Con_Airport[ $segCnt ][ 'Departure' ] = $seg_Departure;
                    $Con_Airport[ $segCnt ][ 'Arrival' ] = $seg_Arrival;

                    $seg_Con_hours = 0; //Connecting Hours and Minutes
                    $seg_Con_minutes = 0;

                    //Connecting an Airport Hours and Minutes
                    $key = array_search( $seg_key, $Segment_keys );

                    if ( false !== $key ):
                      unset( $Segment_keys[ $key ] );
                    endif;

                    if ( isset( $out_Segment[ current( $Segment_keys ) ]->Id ) && $out_Segment[ current( $Segment_keys ) ]->Id ):
                      $start = '';
                    $end = '';
                    $diff = '';

                    $start = date_create( $out_Segment[ current( $Segment_keys ) ]->DepartureDateTime );
                    $end = date_create( $seg_Arrival );

                    $diff = date_diff( $end, $start );

                    $seg_Con_hours = ( strlen( $diff->h ) >= 2 ? $diff->h : '0' . $diff->h );
                    $seg_Con_minutes = ( strlen( $diff->i ) >= 2 ? $diff->i : '0' . $diff->i );

                    endif;

                    if ( $segCnt == 1 ):
                      ?>
                    <div class="row nomargin depart">
                      <h5><b>DEPART</b> <?php echo date("D, d M Y", strtotime($seg_Departure)); ?></h5>
                    </div>
                    <?php
                    endif;
                    ?>
                    <div class="row nomargin">
                      <div class="flightnewinfo">
                        <div class="Firstinfolight">
                          <div class="Flightstation">
                            <h4><?php echo '('.$seg_Origin->Code.') ';?></h4>
                            <span><?php echo $Places[$seg_Origin->ParentId]->Name.', '.$seg_Origin->Name; ?></span> </div>
                          <div class="FlightTiming">
                            <h4><?php echo date("H:i", strtotime($seg_Departure)); ?></h4>
                            <span><?php echo date("D, d M Y", strtotime($seg_Departure)); ?></span> </div>
                          <div class="Flightcode"> <span class="flightnamenew"><?php echo $seg_Carrier->Name; ?></span> <span><?php echo $seg_Carrier->DisplayCode.$seg->FlightNumber; ?></span> </div>
                        </div>
                        <div class="Secondinfolight">
                          <div class="Flightstation">
                            <h4><?php echo '('.$seg_Destination->Code.') ';?></h4>
                            <span><?php echo $Places[$seg_Destination->ParentId]->Name.', '.$seg_Destination->Name; ?></span> </div>
                          <div class="FlightTiming">
                            <h4><?php echo date("H:i", strtotime($seg_Arrival)); ?></h4>
                            <span><?php echo date("D, d M Y", strtotime($seg_Arrival)); ?></span> </div>
                          <div class="Flightcode"> <?php echo $seg_hours; ?>h <?php echo $seg_minutes; ?> </div>
                        </div>
                      </div>
                    </div>
                    <?php
                    if ( count( $seg_OperatingCarrier ) ):
                      ?>
                    <div class="row nomargint">
                      <p>Partly operated by <b><?php echo $seg_OperatingCarrier->Name; ?></b></p>
                    </div>
                    <?php
                    endif;
                    if ( $segCnt < count( $out_Segment ) ):
                      ?>
                    <div class="row nomargin connect-airport">
                      <div class="pull-left">
                        <p>Connect in airport <b><?php echo '('.$seg_Destination->Code.') '.$Places[$seg_Destination->ParentId]->Name; ?></b></p>
                      </div>
                      <div class="pull-right"> <b>Layover</b>&nbsp;&nbsp; <span><?php echo $seg_Con_hours; ?>h <?php echo $seg_Con_minutes; ?></span> </div>
                    </div>
                    <?php
                    endif;
                    if ( count( $out_Segment ) == $segCnt ):
                      ?>
                    <div class="row arrive">
                      <div class="col-xs-6">
                        <ul class="list">
                          <li>Arrives <?php echo date("D, d M Y", strtotime($seg_Arrival)); ?></li>
                        </ul>
                      </div>
                      <div class="col-xs-6 text-right">
                        <ul class="list list-inline">
                          <li>Journey Duration: <?php echo $out_hours; ?>h <?php echo $out_minutes; ?>m</li>
                        </ul>
                      </div>
                    </div>
                    <?php
                    endif;
                    ?>
                    <?php
                    endforeach;
                    endif;
                    ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="pull-left flighrr book-price">
        <div class="agentdetails">
          <h6 class="lowhid"><?php echo $AgentName; ?></h6>
        </div>
        <span class="booking-item-price">$ <?php echo number_format($price,2); ?></span><span class="perpp"><!--/person--></span> 
        <!--<p class="booking-item-flight-class"><span class="lowhid">Class: </span>Economy</p>--> 
        <a class="btn btn-primary"  href="javascript:void(0);">Select</a> </div>
      <div class="clearfix"></div>
    </div>
  </div>
</li>
<?php
endforeach;
else :
  echo 'noresults';
endif;

}
else {

  if ( $Itineraries ):
    foreach ( $Itineraries as $key => $list ):

      ++$Itiner_Cnt;

  if ( !$_GET[ 'loadmore' ] ) {
    if ( $key > 20 ):
      break;
    endif;
  }

  $Outbound = array(); //Variable
  $Inbound = array();

  $Outbound = $Legs[ ( string )$list->OutboundLegId ]; //Set Values
  $Inbound = $Legs[ ( string )$list->InboundLegId ];

  $out_Origin = array();
  $out_Destination = array();
  $out_Segment = array();
  $out_Departure = '';
  $out_Arrival = '';
  $out_Duration = '';

  $in_Origin = array();
  $in_Destination = array();
  $in_Segment = array();
  $in_Departure = '';
  $in_Arrival = '';
  $in_Duration = '';

  $out_Origin = $Places[ ( string )$Outbound->OriginStation ];
  $out_Destination = $Places[ ( string )$Outbound->DestinationStation ];
  $out_Departure = $Outbound->Departure;
  $out_Arrival = $Outbound->Arrival;
  $out_Duration = $Outbound->Duration;
  if ( isset( $Outbound->SegmentIds ) && count( $Outbound->SegmentIds ) > 0 ):
    foreach ( $Outbound->SegmentIds as $Ids ):
      $out_Segment[ $Ids ] = $Segments[ $Ids ];
  endforeach;
  endif;

  $in_Origin = $Places[ ( string )$Inbound->OriginStation ];
  $in_Destination = $Places[ ( string )$Inbound->DestinationStation ];
  $in_Departure = $Inbound->Departure;
  $in_Arrival = $Inbound->Arrival;
  $in_Duration = $Inbound->Duration;
  if ( isset( $Inbound->SegmentIds ) && count( $Inbound->SegmentIds ) > 0 ):
    foreach ( $Inbound->SegmentIds as $Ids ):
      $in_Segment[ $Ids ] = $Segments[ $Ids ];
  endforeach;
  endif;

  $out_Stop = 0; //Stop
  $out_Stop_Text = 'Direct';

  $in_Stop = 0;
  $in_Stop_Text = 'Direct';

  if ( isset( $Outbound->Stops ) && count( $Outbound->Stops ) > 0 ):
    $out_Stop = count( $Outbound->Stops );
  endif;

  if ( $out_Stop > 0 ):
    $out_Stop_Text = $out_Stop . ' Stop';
  endif;

  if ( isset( $Inbound->Stops ) && count( $Inbound->Stops ) > 0 ):
    $in_Stop = count( $Inbound->Stops );
  endif;

  if ( $in_Stop > 0 ):
    $in_Stop_Text = $in_Stop . ' Stop';
  endif;

  $out_hours = 0; //Hours and Minutes
  $out_minutes = 0;

  $in_hours = 0;
  $in_minutes = 0;

  $out_hours = intval( $out_Duration / $MINUTES_PER_HOUR );
  $out_minutes = $out_Duration % $MINUTES_PER_HOUR;

  $out_hours = ( strlen( $out_hours ) >= 2 ? $out_hours : '0' . $out_hours );
  $out_minutes = ( strlen( $out_minutes ) >= 2 ? $out_minutes : '0' . $out_minutes );

  $in_hours = intval( $in_Duration / $MINUTES_PER_HOUR );
  $in_minutes = $in_Duration % $MINUTES_PER_HOUR;

  $in_hours = ( strlen( $in_hours ) >= 2 ? $in_hours : '0' . $in_hours );
  $in_minutes = ( strlen( $in_minutes ) >= 2 ? $in_minutes : '0' . $in_minutes );

  $out_Airline = array(); //Airline
  $in_Airline = array();

  if ( isset( $Outbound->Carriers ) && count( $Outbound->Carriers ) > 0 ):
    $out_Airline = $Carriers[ $Outbound->Carriers[ 0 ] ];
  endif;

  if ( isset( $Inbound->Carriers ) && count( $Inbound->Carriers ) > 0 ):
    $in_Airline = $Carriers[ $Inbound->Carriers[ 0 ] ];
  endif;

  $price = 0;
  $AgentName = '';
  $Agent_DeeplinkUrl = '';

  if ( isset( $list->PricingOptions[ 0 ]->Price ) && $list->PricingOptions[ 0 ]->Price ):

    $price = $list->PricingOptions[ 0 ]->Price;
  $AgentName = $Agents[ $list->PricingOptions[ 0 ]->Agents[ 0 ] ]->Name;
  $Agent_DeeplinkUrl = $list->PricingOptions[ 0 ]->DeeplinkUrl;

  endif;

  $filter_stops = array(); //Filter Data
  $filter_stops[] = $out_Stop;
  $filter_stops[] = $in_Stop

    ?>
<li class="FlightList" <?php echo ($Itiner_Cnt > 20 ? 'style="display:none;"': ''); ?>>
  <div class="EachFlightRow" data-stop="<?php echo max($filter_stops); ?>" data-order="<?php echo $Itiner_Cnt; ?>" data-name="<?php echo $out_Airline->Name; ?>" data-price="<?php echo str_replace(',','.',number_format($price,2)); ?>" data-from="<?php echo strtotime($out_Departure); ?>" data-to="<?php echo strtotime($out_Arrival); ?>" data-duration="<?php echo $out_Duration; ?>" data-obid="<?=$list->OutboundLegId?>" data-ibid="<?=$list->InboundLegId?>">
    <div class="flightrow moreresult-dialog" data-rel="<?php echo $Itiner_Cnt;?>"> 
      <!-- New Style Start -->
      <div class="newview<?php echo $Itiner_Cnt;?> Flightsearchnew" style="display:none;">
        <div class="NewHead">
          <div class="Newbacktoresult closenew" data-rel="<?php echo $Itiner_Cnt;?>">
            <h4>Back to Results</h4>
          </div>
          <div class="Flightheaddetails">
            <h3>Flight Details</h3>
          </div>
          <div class="Newclose closenew" data-rel="<?php echo $Itiner_Cnt;?>"> <a href="javascript:void(0);"  class="closenew"><img src="images/csclose.svg" width="22" height="22" alt="close icons"></a> </div>
        </div>
        <div class="flightrownew">
          <div class="flightll pull-left colleft" style="padding:0">
            <div class="booking-item-container active">
              <div class="booking-item popup-text pull-left itinerarypop">
								<div class="border">
									<div class="Newpoup">
										<div class="itinerarywrapper">
											<h5><?php echo $in_Destination->Name; ?>&mdash;<?php echo $in_Origin->Name; ?></h5>
										</div>
									</div>
								</div>
							</div>
							<div class="booking-item popup-text pull-left">
                <div class="border">
                  <div class="Newpoup">
                    <div class="Firstadvs">
                      <div class="agentdetails">
                        <h6 class="lowhid">Book it on <?php echo $AgentName; ?></h6>
                      </div>
                      <div class="pull-left flighrr book-price"> <span class="booking-item-price">$ <?php echo number_format($price,2); ?></span><span class="perpp"><!--/person--> 
                        <!--<p class="booking-item-flight-class"><span class="lowhid">Class: </span>Economy</p>-->
                        <?php
                        /*nikolay: add condition to designate the flight as one with multiple booking agents*/
                        if ( is_array( $list->PricingOptions[ 0 ]->Agents ) && count( $list->PricingOptions[ 0 ]->Agents ) < 2 ) {
                          ?>
                        <a data-alength="<?=count($list->PricingOptions[0]->Agents)?>" class="btn btn-primary alldeal moreresult-dialog" data-rel="<?php echo $Itiner_Cnt;?>" target="_blank" href="<?php echo $Agent_DeeplinkUrl; ?>" >Book</a>
                        <?php } else { ?>
                        <a data-alength="<?=count($list->PricingOptions[0]->Agents)?>" class="btn btn-primary alldeal loadlinks" href="javascript:void(0);" data-obid="<?=$list->OutboundLegId?>" data-ibid="<?=$list->InboundLegId?>" data-sid="<?=$_SESSION[$unique_id]['sessionkey']?>">Details</a> <a href="javascript:void(0);" class="alldeal mobile-show loadlinks" data-obid="<?=$list->OutboundLegId?>" data-ibid="<?=$list->InboundLegId?>" data-sid="<?=$_SESSION[$unique_id]['sessionkey']?>">2 Bookings Required</a>
                        <?php } ?>
                        </span>
											</div>
											<div class="mobileonly">Lowest price on <strong><?php echo $AgentName; ?></strong></div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <?php
                  if ( isset( $Outbound->SegmentIds ) && count( $Outbound->SegmentIds ) > 0 ):
                    if ( $Segments[ $Outbound->SegmentIds[ 0 ] ]->Carrier != $Segments[ $Outbound->SegmentIds[ 0 ] ]->OperatingCarrier ):
                      ?>
                  <!--p><b>Depart:</b> Partly operated by <b><?php echo $Carriers[$Segments[$Outbound->SegmentIds[0]]->OperatingCarrier]->Name; ?></b></p-->
                  <?php
                  endif;
                  endif;
                  ?>
                  <?php
                  if ( isset( $Inbound->SegmentIds ) && count( $Inbound->SegmentIds ) > 0 ):
                    if ( $Segments[ $Inbound->SegmentIds[ 0 ] ]->Carrier != $Segments[ $Inbound->SegmentIds[ 0 ] ]->OperatingCarrier ):
                      ?>
                  <!--p><b>Return:</b> Partly operated by <b><?php echo $Carriers[$Segments[$Inbound->SegmentIds[0]]->OperatingCarrier]->Name; ?></b></p-->
                  <?php
                  endif;
                  endif;
                  ?>
                  <div class="booking-item-details">
                    <div class="infopad">
                      <div class="dlbox">
                        <?php

                        $segCnt = 0;
                        $Segment_keys = array();
                        $Segment_keys = array_keys( $out_Segment );

                        if ( isset( $out_Segment ) && count( $out_Segment ) ):
                          foreach ( $out_Segment as $seg_key => $seg ):

                            ++$segCnt;

                        $seg_Origin = array();
                        $seg_Destination = array();
                        $seg_Departure = '';
                        $seg_Arrival = '';
                        $seg_Duration = '';
                        $seg_Carrier = array();
                        $seg_OperatingCarrier = array();
                        $seg_hours = 0; //Hours and Minutes
                        $seg_minutes = 0;

                        $seg_Origin = $Places[ ( string )$seg->OriginStation ];
                        $seg_Destination = $Places[ ( string )$seg->DestinationStation ];
                        $seg_Departure = $seg->DepartureDateTime;
                        $seg_Arrival = $seg->ArrivalDateTime;
                        $seg_Duration = $seg->Duration;
                        $seg_Carrier = $Carriers[ ( string )$seg->Carrier ];
                        if ( ( string )$seg->Carrier != $seg->OperatingCarrier ):
                          $seg_OperatingCarrier = $Carriers[ ( string )$seg->OperatingCarrier ];
                        endif;
                        $seg_hours = intval( $seg_Duration / $MINUTES_PER_HOUR );
                        $seg_minutes = $seg_Duration % $MINUTES_PER_HOUR;

                        $seg_hours = ( strlen( $seg_hours ) >= 2 ? $seg_hours : '0' . $seg_hours );
                        $seg_minutes = ( strlen( $seg_minutes ) >= 2 ? $seg_minutes : '0' . $seg_minutes );

                        $Con_Airport[ $segCnt ][ 'Departure' ] = $seg_Departure;
                        $Con_Airport[ $segCnt ][ 'Arrival' ] = $seg_Arrival;

                        $seg_Con_hours = 0; //Connecting Hours and Minutes
                        $seg_Con_minutes = 0;

                        //Connecting an Airport Hours and Minutes
                        $key = array_search( $seg_key, $Segment_keys );

                        if ( false !== $key ):
                          unset( $Segment_keys[ $key ] );
                        endif;

                        if ( isset( $out_Segment[ current( $Segment_keys ) ]->Id ) && $out_Segment[ current( $Segment_keys ) ]->Id ):
                          $start = '';
                        $end = '';
                        $diff = '';

                        $start = date_create( $out_Segment[ current( $Segment_keys ) ]->DepartureDateTime );
                        $end = date_create( $seg_Arrival );

                        $diff = date_diff( $end, $start );

                        $seg_Con_hours = ( strlen( $diff->h ) >= 2 ? $diff->h : '0' . $diff->h );
                        $seg_Con_minutes = ( strlen( $diff->i ) >= 2 ? $diff->i : '0' . $diff->i );

                        endif;

                        if ( $segCnt == 1 ):
                          ?>
                        <div class="row nomargin depart">
                          <h5><b>DEPART</b> <?php echo date("D, d M Y", strtotime($seg_Departure)); ?></h5>
                        </div>
                        <?php
                        endif;
                        ?>
                        <div class="row nomargin">
                          <div class="flightnewinfo">
                            <div class="Firstinfolight">
                              <div class="Flightstation">
                                <h4><?php echo '('.$seg_Origin->Code.') ';?></h4>
                                <span><?php echo $Places[$seg_Origin->ParentId]->Name.', '.$seg_Origin->Name; ?></span> </div>
                              <div class="FlightTiming">
                                <h4><?php echo date("H:i", strtotime($seg_Departure)); ?></h4>
                                <span><?php echo date("D, d M Y", strtotime($seg_Departure)); ?></span> </div>
                              <div class="Flightcode"> <span class="flightnamenew"><?php echo $seg_Carrier->Name; ?></span> <img src="<?php echo str_replace('http://','//',$seg_Carrier->ImageUrl); ?>" alt="<?php echo $out_Airline->Name; ?>" title="<?php echo $out_Airline->Name; ?>" class="lufthansa" /> <span><?php echo $seg_Carrier->DisplayCode.$seg->FlightNumber; ?></span> </div>
                            </div>
                            <div class="Secondinfolight">
                              <div class="Flightstation">
                                <h4><?php echo '('.$seg_Destination->Code.') ';?></h4>
                                <span><?php echo $Places[$seg_Destination->ParentId]->Name.', '.$seg_Destination->Name; ?></span> </div>
                              <div class="FlightTiming">
                                <h4><?php echo date("H:i", strtotime($seg_Arrival)); ?></h4>
                                <span><?php echo date("D, d M Y", strtotime($seg_Arrival)); ?></span> </div>
                              <div class="Flightcode">
                                <h4>Duration</h4>
                                <span><?php echo $seg_hours; ?>h <?php echo $seg_minutes; ?></span> </div>
                            </div>
                          </div>
                        </div>
                        <?php
                        if ( count( $seg_OperatingCarrier ) ):
                          ?>
                        <div class="row nomargin">
                          <p>Partly operated by <b><?php echo $seg_OperatingCarrier->Name; ?></b></p>
                        </div>
                        <?php
                        endif;
                        if ( $segCnt < count( $out_Segment ) ):
                          ?>
                        <div class="row nomargin connect-airport">
                          <div class="pull-left">
                            <p>Connect in airport <b><?php echo '('.$seg_Destination->Code.') '.$Places[$seg_Destination->ParentId]->Name; ?></b></p>
                          </div>
                          <div class="pull-right"> <b>Layover</b>&nbsp;&nbsp; <span><?php echo $seg_Con_hours; ?>h <?php echo $seg_Con_minutes; ?></span> </div>
                        </div>
                        <?php
                        endif;
                        if ( count( $out_Segment ) == $segCnt ):
                          ?>
                        <div class="row arrive">
                          <div class="col-xs-6">
                            <ul class="list">
                              <li>Arrives <?php echo date("D, d M Y", strtotime($seg_Arrival)); ?></li>
                            </ul>
                          </div>
                          <div class="col-xs-6 text-right">
                            <ul class="list list-inline">
                              <li>Journey Duration: <?php echo $out_hours; ?>h <?php echo $out_minutes; ?>m</li>
                            </ul>
                          </div>
                        </div>
                        <?php
                        endif;
                        ?>
                        <?php
                        endforeach;
                        endif;
                        ?>
                      </div>
                      <div class="dlbox">
                        <?php

                        $segCnt = 0;

                        $Segment_keys = array();
                        $Segment_keys = array_keys( $in_Segment );

                        if ( isset( $in_Segment ) && count( $in_Segment ) ):
                          foreach ( $in_Segment as $seg_key => $seg ):

                            ++$segCnt;

                        $seg_Origin = array();
                        $seg_Destination = array();
                        $seg_Departure = '';
                        $seg_Arrival = '';
                        $seg_Duration = '';
                        $seg_Carrier = array();
                        $seg_OperatingCarrier = array();
                        $seg_hours = 0; //Hours and Minutes
                        $seg_minutes = 0;

                        $seg_Origin = $Places[ ( string )$seg->OriginStation ];
                        $seg_Destination = $Places[ ( string )$seg->DestinationStation ];
                        $seg_Departure = $seg->DepartureDateTime;
                        $seg_Arrival = $seg->ArrivalDateTime;
                        $seg_Duration = $seg->Duration;
                        $seg_Carrier = $Carriers[ ( string )$seg->Carrier ];
                        if ( ( string )$seg->Carrier != $seg->OperatingCarrier ):
                          $seg_OperatingCarrier = $Carriers[ ( string )$seg->OperatingCarrier ];
                        endif;
                        $seg_hours = intval( $seg_Duration / $MINUTES_PER_HOUR );
                        $seg_minutes = $seg_Duration % $MINUTES_PER_HOUR;

                        $seg_hours = ( strlen( $seg_hours ) >= 2 ? $seg_hours : '0' . $seg_hours );
                        $seg_minutes = ( strlen( $seg_minutes ) >= 2 ? $seg_minutes : '0' . $seg_minutes );

                        $seg_Con_hours = 0; //Connecting Hours and Minutes
                        $seg_Con_minutes = 0;

                        //Connecting an Airport Hours and Minutes
                        $key = array_search( $seg_key, $Segment_keys );

                        if ( false !== $key ):
                          unset( $Segment_keys[ $key ] );
                        endif;

                        if ( isset( $in_Segment[ current( $Segment_keys ) ]->Id ) && $in_Segment[ current( $Segment_keys ) ]->Id ):
                          $start = '';
                        $end = '';
                        $diff = '';

                        $start = date_create( $in_Segment[ current( $Segment_keys ) ]->DepartureDateTime );
                        $end = date_create( $seg_Arrival );

                        $diff = date_diff( $end, $start );

                        $seg_Con_hours = ( strlen( $diff->h ) >= 2 ? $diff->h : '0' . $diff->h );
                        $seg_Con_minutes = ( strlen( $diff->i ) >= 2 ? $diff->i : '0' . $diff->i );

                        endif;

                        if ( $segCnt == 1 ):
                          ?>
                        <div class="row nomargin depart">
                          <h5><b>RETURN</b> <?php echo date("D, d M Y", strtotime($seg_Departure)); ?></h5>
                        </div>
                        <?php
                        endif;
                        ?>
                        <div class="row nomargin">
                          <div class="flightnewinfo">
                            <div class="Firstinfolight">
                              <div class="Flightstation">
                                <h4><?php echo '('.$seg_Origin->Code.') ';?></h4>
                                <span><?php echo $Places[$seg_Origin->ParentId]->Name.', '.$seg_Origin->Name; ?></span> </div>
                              <div class="FlightTiming">
                                <h4><?php echo date("H:i", strtotime($seg_Departure)); ?></h4>
                                <span><?php echo date("D, d M Y", strtotime($seg_Departure)); ?></span> </div>
                              <div class="Flightcode"> <span class="flightnamenew"><?php echo $seg_Carrier->Name; ?></span> <img src="<?php echo str_replace('http://','//',$seg_Carrier->ImageUrl); ?>" alt="<?php echo $in_Airline->Name; ?>" title="<?php echo $in_Airline->Name; ?>" class="lufthansa" /> <span><?php echo $seg_Carrier->DisplayCode.$seg->FlightNumber; ?></span> </div>
                            </div>
                            <div class="Secondinfolight">
                              <div class="Flightstation">
                                <h4><?php echo '('.$seg_Destination->Code.') ';?></h4>
                                <span><?php echo $Places[$seg_Destination->ParentId]->Name.', '.$seg_Destination->Name; ?></span> </div>
                              <div class="FlightTiming">
                                <h4><?php echo date("H:i", strtotime($seg_Arrival)); ?></h4>
                                <span><?php echo date("D, d M Y", strtotime($seg_Arrival)); ?></span> </div>
                              <div class="Flightcode">
                                <h4>Duration</h4>
                                <span><?php echo $seg_hours; ?>h <?php echo $seg_minutes; ?></span> </div>
                            </div>
                          </div>
                        </div>
                        <?php
                        if ( count( $seg_OperatingCarrier ) ):
                          ?>
                        <div class="row nomargin">
                          <p>Partly operated by <b><?php echo $seg_OperatingCarrier->Name; ?></b></p>
                        </div>
                        <?php
                        endif;
                        if ( $segCnt < count( $in_Segment ) ):
                          ?>
                        <div class="row nomargin connect-airport">
                          <div class="pull-left">
                            <p>Connect in airport <b><?php echo '('.$seg_Destination->Code.') '.$Places[$seg_Destination->ParentId]->Name; ?></b></p>
                          </div>
                          <div class="pull-right"> <b>Layover</b>&nbsp;&nbsp; <span><?php echo $seg_Con_hours; ?>h <?php echo $seg_Con_minutes; ?></span> </div>
                        </div>
                        <?php
                        endif;
                        if ( count( $in_Segment ) == $segCnt ):
                          ?>
                        <div class="row arrive">
                          <div class="col-xs-6">
                            <ul class="list">
                              <li>Arrives <?php echo date("D, d M Y", strtotime($seg_Arrival)); ?></li>
                            </ul>
                          </div>
                          <div class="col-xs-6 text-right">
                            <ul class="list list-inline">
                              <li>Journey Duration: <?php echo $in_hours; ?>h <?php echo $in_minutes; ?>m</li>
                            </ul>
                          </div>
                        </div>
                        <?php
                        endif;
                        ?>
                        <?php
                        endforeach;
                        endif;
                        ?>
                      </div>
                    </div>
                    <?php
                    if ( count( $list->PricingOptions ) ):
                      ?>
                    <div class="agent-deals">
                      <h4>More Booking Options</h4>
                      <ul>
                        <?php
                        foreach ( $list->PricingOptions as $AList ):
                          /*nikolay: add condition to designate the flight as one with multiple booking agents*/
                          if ( is_array( $list->PricingOptions[ 0 ]->Agents ) && count( $list->PricingOptions[ 0 ]->Agents ) < 2 ) {
                            ?>
                        <li><span class="agentimage"><img style="width:auto; height:55px;" src="<?php echo str_replace('http://','//',$Agents[$AList->Agents[0]]->ImageUrl); ?>" alt="<?php echo $Agents[$AList->Agents[0]]->Name; ?>"></span><span class="agentname"><?php echo $Agents[$AList->Agents[0]]->Name;; ?></span><span class="agentprice">$ <?php echo number_format($AList->Price,2); ?></span><a data-test="booksingle" target="_blank" class="btn bookbt btn-primary" href="<?php echo $AList->DeeplinkUrl; ?>">Book</a></li>
                        <?php } else { ?>
                        <li style="border-bottom: 0;">
                          <?php
                          /*
										<span class="agentimage"><img style="width:auto; height:55px;" src="<?php echo $Agents[$AList->Agents[0]]->ImageUrl; ?>" alt="<?php echo $Agents[$AList->Agents[0]]->Name; ?>"></span><span class="agentname"><?php echo $Agents[$AList->Agents[0]]->Name;; ?></span><span class="agentprice">$ <?php echo number_format($AList->Price,2); ?></span><a target="_blank" class="btn bookbt btn-primary loadlinks" href="<?php echo $AList->DeeplinkUrl; ?>" data-obid="<?=$list->OutboundLegId?>" data-ibid="<?=$list->InboundLegId?>">Book</a>
										*/
                          ?>
                          <h3>Score!</h3>
                          <p>In order to get this great deal, you must book each leg of this flight separately. <strong style="font-weight: bold;">Be sure to book both your inbound and outbound flights!</strong></p>
                          <p class="fetchingadls-<?=$list->OutboundLegId?>-<?=$list->InboundLegId?> initial"> <br>
                            <span class="adlsspinner"> <img src="//www.discountflights.com/images/dlloader.gif" width="16" height="16" alt="loading spinner" /> </span> <span class="adlsspinnermessage">Hang tight, we're fetching your booking links.</span> </p>
                        </li>
                        <?php
                        }
                        endforeach;
                        ?>
                      </ul>
                      <span class="addbags"><a href="https://www.discountflights.com/airline-baggage-fees.php" target="_blank">Additional bag fees may apply</a></span> 
										</div>
                    <?php
                    endif;
                    ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
      
      <!-- New Style End  -->
      
      <div class="flightll pull-left colleft" style="padding:0">
        <div class="booking-item-container">
          <div class="booking-item popup-text pull-left">
            <div class="border">
              <div class="airlinelogos pull-left">
                <div class="alogo">
                  <div class="booking-item-airline-logo"> <img src="<?php echo str_replace('http://','//',$out_Airline->ImageUrl); ?>" alt="<?php echo $out_Airline->Name; ?>" title="<?php echo $out_Airline->Name; ?>" class="lufthansa" /> <img src="<?php echo str_replace('http://','//',$in_Airline->ImageUrl); ?>" alt="<?php echo $in_Airline->Name; ?>" title="<?php echo $in_Airline->Name; ?>" class="american-airline" /> 
                    
                    <!--<p>Lufthansa</p>--> 
                  </div>
                </div>
              </div>
              <div class="flightintro pull-left">
                <div class="row">
                  <div class="fileft pull-left">
                    <div class="booking-item-flight-details" id="arrow">
                      <div class="booking-item-departure"><!-- <i class="fa fa-plane"></i> --> 
                        <span class="booking-item-date"><?php echo date("H:i", strtotime($out_Departure)); ?></span> <span class="jfk">(<?php echo $out_Origin->Code; ?>)</span> </div>
                      <div class="arrowctrl">
                        <div class="linedot <?php echo ($out_Stop > 1 ? 'twostop' : 'onestop'); ?> pull-left"> <span class="arrowline"></span>
                          <?php
                          for ( $i = 1; $i <= $out_Stop; $i++ ):
                            echo '<div class="stopcircle stop' . $i . '"> </div>';
                          endfor;
                          ?>
                        </div>
                        <img src="images/arrow-end.png" class="arrow-small">
                        <div class="clearfix"></div>
                      </div>
                      <div class="booking-item-arrival arrive"><!-- <i class="fa fa-plane fa-flip-vertical"></i> --> 
                        <span class="booking-item-date"><?php echo date("H:i", strtotime($out_Arrival)); ?></span> <span class="jfk">(<?php echo $out_Destination->Code; ?>)</span> </div>
                    </div>
                  </div>
                  <div class="firight pull-left"> <span><?php echo $out_hours; ?>h <?php echo $out_minutes; ?>m</span> <span class="stops"><?php echo $out_Stop_Text; ?></span> </div>
                </div>
                <div class="row">
                  <div class="fileft pull-left">
                    <div class="booking-item-flight-details" id="arrow">
                      <div class="booking-item-departure"><!-- <i class="fa fa-plane"></i> --> 
                        <span class="booking-item-date"><?php echo date("H:i", strtotime($in_Departure)); ?></span> <span class="jfk">(<?php echo $in_Origin->Code; ?>)</span></div>
                      <div class="arrowctrl">
                        <div class="linedot <?php echo ($in_Stop > 1 ? 'twostop' : 'onestop'); ?> pull-left"> <span class="arrowline"></span>
                          <?php
                          for ( $i = 1; $i <= $in_Stop; $i++ ):
                            echo '<div class="stopcircle stop' . $i . '"> </div>';
                          endfor;
                          ?>
                        </div>
                        <img src="images/arrow-end.png" class="arrow-small">
                        <div class="clearfix"></div>
                      </div>
                      <div class="booking-item-arrival arrive"><!-- <i class="fa fa-plane fa-flip-vertical"></i> --> 
                        <span class="booking-item-date"><?php echo date("H:i", strtotime($in_Arrival)); ?></span> <span class="jfk">(<?php echo $in_Destination->Code; ?>)</span></div>
                    </div>
                  </div>
                  <div class="firight pull-left"> <span><?php echo $in_hours; ?>h <?php echo $in_minutes; ?>m</span> <span class="spangreen stops"><?php echo $in_Stop_Text; ?></span> </div>
                </div>
              </div>
              <div class="clearfix"></div>
              <?php
              if ( isset( $Outbound->SegmentIds ) && count( $Outbound->SegmentIds ) > 0 ):
                if ( $Segments[ $Outbound->SegmentIds[ 0 ] ]->Carrier != $Segments[ $Outbound->SegmentIds[ 0 ] ]->OperatingCarrier ):
                  ?>
              <p><b>Depart:</b> Partly operated by <b><?php echo $Carriers[$Segments[$Outbound->SegmentIds[0]]->OperatingCarrier]->Name; ?></b></p>
              <?php
              endif;
              endif;
              ?>
              <?php
              if ( isset( $Inbound->SegmentIds ) && count( $Inbound->SegmentIds ) > 0 ):
                if ( $Segments[ $Inbound->SegmentIds[ 0 ] ]->Carrier != $Segments[ $Inbound->SegmentIds[ 0 ] ]->OperatingCarrier ):
                  ?>
              <p><b>Return:</b> Partly operated by <b><?php echo $Carriers[$Segments[$Inbound->SegmentIds[0]]->OperatingCarrier]->Name; ?></b></p>
              <?php
              endif;
              endif;
              ?>
            </div>
          </div>
        </div>
      </div>
      <div class="pull-left flighrr book-price">
        <div class="agentdetails">
          <h6 class="lowhid"><?php echo $AgentName; ?></h6>
        </div>
        <span class="booking-item-price">$ <?php echo number_format($price,2); ?></span><span class="perpp"><!--/person--></span> 
        <!--<p class="booking-item-flight-class"><span class="lowhid">Class: </span>Economy</p>-->
        <?php
        /*nikolay: add condition to designate the flight as one with multiple booking agents*/
        if ( is_array( $list->PricingOptions[ 0 ]->Agents ) && count( $list->PricingOptions[ 0 ]->Agents ) < 2 ) {
          ?>
        <a data-alength="<?=count($list->PricingOptions[0]->Agents)?>" class="btn btn-primary alldeal moreresult-dialog" data-rel="<?php echo $Itiner_Cnt;?>" href="javascript:void(0);" >Select</a>
        <?php } else { ?>
        <a data-alength="<?=count($list->PricingOptions[0]->Agents)?>" class="btn btn-primary alldeal loadlinks" href="javascript::void(0);" data-obid="<?=$list->OutboundLegId?>" data-ibid="<?=$list->InboundLegId?>" data-sid="<?=$_SESSION[$unique_id]['sessionkey']?>">Details</a> <a href="javascript:void(0);" class="alldeal mobile-show loadlinks" data-obid="<?=$list->OutboundLegId?>" data-ibid="<?=$list->InboundLegId?>" data-sid="<?=$_SESSION[$unique_id]['sessionkey']?>">2 Bookings Required</a>
        <?php } ?>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</li>
<?php
endforeach;
else :
  echo 'noresults';
endif;

}

echo '<span style="display:none;">' . $ResStatus . '</span>';

else :

  if ( !$ResStatus ):
    $ResStatus = 'noresults';
endif;

echo '<span style="display:none;">' . $ResStatus . '</span>';

endif;

else :
  echo 'noresults';
endif;
?>
<input type="hidden" class="flightcount" data-name="/new" value="<?php echo ($Itineraries ? count($Itineraries) : '0'); ?>" />
<?php
if ( isset( $Carriers ) && count( $Carriers ) > 0 ):
  ?>
<div class="AjaxAirlines" style="display:none;">
  <?php
  foreach ( $Carriers as $ca ):
    ?>
  <div class="checkbox">
    <input id="<?=str_replace(' ','',$ca->Code)?>" class="myinput large AirlinesName" value="<?php echo $ca->Code; ?>" type="checkbox" style="position: absolute;left: 50px;">
    <label for="<?=str_replace(' ','',$ca->Code)?>"><?php echo $ca->Name; ?></label>
  </div>
  <?php
  endforeach;
  ?>
</div>
<?php
endif;
?>
<?php // echo "http://partners.api.skyscanner.net/apiservices/pricing/v1.0/".$_SESSION[$unique_id]['sessionkey']."?apikey=".$apikey.$sorting.$filter; ?>
