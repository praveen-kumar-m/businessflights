<?php require('header.php');
?>
<div class="footer">
<div class="iwrapper" id="extcontent"> 
  <h2>Help Center</h2>
  <h4>Who should I contact about my booking?</h4>
  <p>If you need to contact someone about your booking, it&rsquo;s best to speak to the flight provider directly. As a travel search engine DiscountFlights.com helps you plan your trip, but we don't take bookings or payments and don&rsquo;t have access to your booking information; instead you&rsquo;re transferred to an airline or travel agent, and your booking is made directly with them.</p>
  <p> For this reason, we recommend you get in touch with the flight provider directly with questions about completing a booking, making changes to a booking, payment issues, cancellations, refunds or questions about luggage, charges, or what you can (or can&rsquo;t) take on the plane. You should have received a confirmation when you made your booking which will contain the flight provider&rsquo;s contact details. </p>
  <p> To help you find their contact details, below is a list of some of the most popular travel providers who appear on DiscountFlights.com</p>
  <p> If you&rsquo;re struggling to reach them, or you need a hand, please get in touch using the contact us link below, and please remember to include as many details as possible about the flights you booked – we&rsquo;ll try our best to help out.</p>

  <h4>Where is my booking confirmation?</h4>
  <p>As a travel search engine, DiscountFlights.com is a tool to help you plan your trip but we don't take bookings or payments of any kind. This is because when you have found what you are looking for, we transfer you to the airline or travel agent to complete your booking. So your booking is with them, rather than with DiscountFlights.com.</p>
  <p> Once you&rsquo;ve made a booking, the provider will send you a confirmation email. If you haven't received that email, we recommend checking your spam or trash email folders, as sometimes emails from unrecognized sources can end up there.</p>
  <p> If you still can't find the email, we recommend that you contact the airline or travel agent directly. If you can't remember who you booked with, you can usually check your credit card statement for a company name.&nbsp;Tickets are usually issued on the same day, but some travel agents may take longer so it will be worth checking with them directly.&nbsp; </p>
	
	<h4>How can I pay?</h4>
  <p>DiscountFlights.com is a travel search engine, and we don't handle bookings or payments at all. When you find the travel arrangements that suit you, you will be transferred to the service provider&rsquo;s site to complete your booking. Once there, you&rsquo;ll be able to see all the different payment options they offer.</p>
  <p>For this reason, we recommend that you get in touch with the airline, travel agent or travel provider directly if you have any questions about how to pay. You can usually find their contact details on their website.</p>
  <p>For security reasons, when you buy a flight ticket you may sometimes be asked for further identification or documents before your booking can be processed. If you have any concerns about this, please contact your provider.</p>
	
	<h4>How do I change or cancel my booking?</h4>
  <p>As a travel search engine, DiscountFlights.com is a tool to help you plan your trip but we don&rsquo;t take bookings or payments of any kind. Once you have found what you are looking for, we simply transfer you to the airline, travel agent or travel provider to complete your booking. This means your booking is with them, not with DiscountFlights.com, and we don&rsquo;t hold any of your details connected with any bookings you may have made.</p>
  <p>For this reason, we recommend you get in touch with the service provider directly with questions about changing or cancelling a booking. Depending on which company you have booked through, there will be different options for cancellation and refunds and these can usually be found on the company&rsquo;s website.</p>
	
	<h4>I booked the wrong dates / times</h4>
  <p>You can search flexible or specific dates on DiscountFlights.com to find your preferred flight, and when you select a flight on DiscountFlights.com you are transferred to the website where you will make and pay for your booking. Once you are redirected to the airline or travel agent website, you might be required to select dates again, depending on the website. In all cases, you will be shown the flight details of your selection before you confirm your booking. We strongly recommend that you always check this information carefully, as travel information can be subject to change.</p>

  <h4>My booking has been canceled</h4>
  <p>We’re sorry to hear that! Cancellations occasionally occur, usually when the airline of travel agent is unable to fulfill a ticket at their advertised price, or if the airline makes a decision to cancel that flight or route. If this happens, the airline or travel agent will give you the option to purchase a ticket at the new price or refund your money in full. Unfortunately, cancelled bookings are beyond DiscountFlights.com control because we are a travel search engine, rather than a booking service. We would recommend speaking to your travel provider directly with regards to the options that might be available.</p>
  <h4>I think I have been overcharged</h4>
  <p>Prices on DiscountFlights.com always include an estimate of all taxes and charges that must be paid.</p>
  <p>When we transfer you to the airline or travel agent website to complete your booking, all flight details including prices will be updated and displayed. At this stage, we recommend and you check this information carefully before you confirm and pay for your booking. The final price you pay for your tickets may change because of additional options offered by the provider, such as luggage or travel insurance (although these should always be optional).</p>
  <p>A note about card fees. Airlines and travel agents offer a free payment option (usually it&rsquo;s the most popular card in your country) but they might charge fees for other payment methods. Because you have at least one option to avoid these fees, we don't include them in our prices. If you feel like you have been overcharged, we recommend that you contact the provider you booked with – they are the best people to help you, and DiscountFlights.com doesn&rsquo;t have any access to your booking information.</p>
  <p>If you have already contacted them and you still need our help, please get in touch via the contact us link below.</p>
  <h4>I entered the wrong email address</h4>
  <p>Please contact the airline or travel agent you booked with as DiscountFlights.com does not have access to bookings made with airlines/travel agents. If you can't remember who you booked with, you can check your credit card statement for a company name.</p>
</div>
</div>
<?php require('footer.php');
?>
