<?php

session_start();

$Is_Page = 'PageIdlist';

$unique_id = uniqid(); // Generate for ajax values

$_SESSION[$unique_id] = array();

$_SESSION[$unique_id]['get'] = $_GET;

//Create Session Key
$out_ex = explode('/',$_GET['outdate']);
if(isset($_GET['indate']) && $_GET['indate']):
 $in_ex = explode('/',$_GET['indate']);
endif;

$out = strtotime($out_ex[1].'/'.$out_ex[0].'/'.$out_ex[2]);
if(isset($_GET['indate']) && $_GET['indate']):
 $in = strtotime($in_ex[1].'/'.$in_ex[0].'/'.$in_ex[2]);
endif;

$apikey = 'as901892827258298792949665487652';

$from 		= (isset($_GET['from']) && $_GET['from'] ? $_GET['from'] : '');
if ($from != '') $fromdisplay = '<span class="mh">'.str_replace('(','(</span><span class="ms">',str_replace(')','</span><span class="mh">)',$from)).'</span>';
$to 		= (isset($_GET['to']) && $_GET['to'] ? $_GET['to'] : '');
if ($to != '') $todisplay = '<span class="mh">'.str_replace('(','(</span><span class="ms">',str_replace(')','</span><span class="mh">)',$to)).'</span>';
$oricode 	= (isset($_GET['oricode']) && $_GET['oricode'] ? $_GET['oricode'] : '');
$descode 	= (isset($_GET['descode']) && $_GET['descode'] ? $_GET['descode'] : '');
$outdate 	= (isset($_GET['outdate']) && $_GET['outdate'] ? date("Y-m-d", strtotime($_GET['outdate'])) : '');
$indate 	= (isset($_GET['indate']) && $_GET['indate'] ? date("Y-m-d", strtotime($_GET['indate'])) : '');
$adults 	= (isset($_GET['adults']) && $_GET['adults'] ? $_GET['adults'] : '1');
$children 	= (isset($_GET['children']) && $_GET['children'] ? $_GET['children'] : '0');
$infants 	= (isset($_GET['infants']) && $_GET['infants'] ? $_GET['infants'] : '0');
$cabinclass 	= (isset($_GET['cabinclass']) && $_GET['cabinclass'] ? $_GET['cabinclass'] : 'Economy');
$trip = (isset($_GET['trip']) && $_GET['trip'] ? $_GET['trip'] : '2');

$param = 'country=US&currency='.$_GET['rs_currency'].'&locale='.$_GET['rs_language'].'&originplace='.$oricode.'&destinationplace='.$descode.'&outbounddate='.$outdate.'&adults='.$adults.'&children='.$children.'&infants='.$infants.'&locationschema=iata&cabinclass='.$cabinclass.'&groupPricing=true';

$_SESSION[$unique_id]['way'] = '1';

if($trip == 2):
	if($indate):
		$param = 'country=US&currency='.$_GET['rs_currency'].'&locale='.$_GET['rs_language'].'&originplace='.$oricode.'&destinationplace='.$descode.'&outbounddate='.$outdate.'&inbounddate='.$indate.'&adults='.$adults.'&children='.$children.'&infants='.$infants.'&locationschema=iata&cabinclass='.$cabinclass.'&groupPricing=true';
		$_SESSION[$unique_id]['way'] = '2';
	endif;
endif;

$ch = curl_init();

curl_setopt($ch, CURLOPT_URL,"http://partners.api.skyscanner.net/apiservices/pricing/v1.0?apikey=".$apikey);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_HEADER, true);
curl_setopt($ch, CURLOPT_VERBOSE, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
curl_setopt($ch, CURLOPT_HTTPHEADER, 
    array(
        'Content-Type: application/x-www-form-urlencoded', 
        'Accept: application/json'
    )
);
$result = curl_exec($ch);

$st=explode('/',$result);

//print_r($st);
//die($st);
//exit;

$sessionkey = substr($st[9],0,42);

//$_SESSION[$unique_id]['sessionkey'] = $sessionkey;
$_SESSION[$unique_id]['sessionkey'] = substr($sessionkey,0,-6);

global $mobileview;
function isMobile() {
    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
}

$mobileview = isMobile();

?>
<?php require 'header.php'; ?>
<div class="global-wrap">
    <div class="container">
    <ul class="breadcrumb">
      <li><a href="index.html">Home</a> </li>
      <li><a href="#">United States</a> </li>
      <li><a href="#">New York (NY)</a> </li>
      <li><a href="#">New York City</a> </li>
      <li class="active">New York City Flights</li>
    </ul>
    <div class="mfp-with-anim mfp-hide mfp-dialog mfp-search-dialog" id="search-dialog">
      <div class="containertp">
        <div class="searchWrapper">
          <div id="rs_multi" class="rs_searchbox">
            <form name="air" class="air rs_air_form" id = "rs_air_forms" action="dynamic-flight-result.php" method="GET">
              <div class="rs_air_options"> <span class="rs_air_option rs_air_highlight">
                <input type="radio" class="round-trip" name="air-radio" id="round-trip" checked>
                <label for="round-trip">Round Trip</label>
                </span> <span class="rs_air_option">
                <input type="radio" class="one-way" name="air-radio" id="one-way">
                <label for="one-way" class="ftripers">One Way</label>
                </span> 
                <!--span class="rs_air_option"><input type="radio" class="multi-city" name="air-radio" id="multi-city"><label for="multi-city">Multi City</label></span--> 
              </div>
              <div class="clear"></div>
              <div id="air_round_trip">
                <div class="row">
                  <div class="col-sm-6 input-daterange">
                    <input name="from" class="from autosuggest rs_from" value="<?php echo $from; ?>" id = "rs_from" placeholder = 'From' onClick='$(this).val("");' autocomplete="off">
                    <input type="hidden" name="oricode" value="<?php echo $oricode; ?>" id = "rs_o_code">
                    <div id="suggesstion-box"></div>
                  </div>
                  <div class="col-sm-6">
                    <input name="to" class="to autosuggest rs_to" value="<?php echo $to; ?>" placeholder ='To' id = "rs_to" onClick='$(this).val("");' autocomplete="off">
                    <input type="hidden" name="descode" value="<?php echo $descode; ?>" id ="rs_d_code">
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-4 DepartBlk">
                    <input name="outdate" class="rs_chk_in tb-input" value="<?php echo $_GET['outdate']; ?>" id="rs_chk_in" placeholder ="Depart" readonly>
                  </div>
                  <div class="col-sm-4 DateOutBlock">
                    <input name="indate" class="rs_chk_out tb-input" value="<?php echo $_GET['indate']; ?>" id = "rs_chk_out" placeholder = "Return" readonly>
                  </div>
                  <div class="col-sm-4 travelersBlk">
                    <div class="travelers"> <span>Travelers <?php echo $_GET['adults'] + $_GET['children'] + $_GET['infants'] ?>, <?php echo $_GET['cabinclass']; ?></span> <img src="images/down-arrow.png"> </div>
                  </div>
                </div>
                <div class="row PaxBlock" style="display:none;">
                  <div class="col-md-3 col-sm-6">
                    <label>Adults (16+) </label>
                    <select name="adults" class="rs_adults_input pax paxcal">
                      <option value="1" <?php echo ($adults == '1' ? 'selected' : ''); ?>>1 Adult</option>
                      <option value="2" <?php echo ($adults == '2' ? 'selected' : ''); ?>>2 Adults</option>
     	              <option value="3" <?php echo ($adults == '3' ? 'selected' : ''); ?>>3 Adults</option>
                      <option value="4" <?php echo ($adults == '4' ? 'selected' : ''); ?>>4 Adults</option>
                      <option value="5" <?php echo ($adults == '5' ? 'selected' : ''); ?>>5 Adults</option>
                      <option value="6" <?php echo ($adults == '6' ? 'selected' : ''); ?>>6 Adults</option>
                    </select>
                  </div>
                  <div class="col-md-3 col-sm-6">
                    <label>Children (2-16)</label>
                    <select name="children" class="rs_child_input pax paxcal">
                      <option selected="" value="0">0 Children</option>
                      <option value="1" <?php echo ($children == '1' ? 'selected' : ''); ?>>1 Children</option>
                      <option value="2" <?php echo ($children == '2' ? 'selected' : ''); ?>>2 Children</option>
                      <option value="3" <?php echo ($children == '3' ? 'selected' : ''); ?>>3 Children</option>
                      <option value="4" <?php echo ($children == '4' ? 'selected' : ''); ?>>4 Children</option>
                      <option value="5" <?php echo ($children == '5' ? 'selected' : ''); ?>>5 Children</option>
                    </select>
                  </div>
                  <div class="col-md-3 col-sm-6">
                    <label>Infants (0-2)</label>
                    <select name="infants" class="rs_infant_input pax paxcal">
                      <option selected="" value="0">0 Infants</option>
                      <option value="1" <?php echo ($infants == '1' ? 'selected' : ''); ?>>1 Infants</option>
                      <option value="2" <?php echo ($infants == '2' ? 'selected' : ''); ?>>2 Infants</option>
                      <option value="3" <?php echo ($infants == '3' ? 'selected' : ''); ?>>3 Infants</option>
                      <option value="4" <?php echo ($infants == '4' ? 'selected' : ''); ?>>4 Infants</option>
                      <option value="5" <?php echo ($infants == '5' ? 'selected' : ''); ?>>5 Infants</option>
                    </select>
                  </div>
                  <div class="col-md-3 col-sm-6">
                    <label>Cabin Class</label>
                    <select name="cabinclass" class="rs_select_skin_activated rs_select_box rs_cabin_box paxcal">
                      <option selected="" value="Economy">Economy</option>
                      <option value="PremiumEconomy">Premium Economy</option>
                      <option value="Business">Business</option>
                      <option value="First">First</option>
                    </select>
                  </div>
                </div>
                <div class="rs_button_row newbtn">
          <input type="hidden" name="rs_language"  value="en-US">
            <input type="hidden" name="rs_currency"  value="USD">
            <div class="rs_search">
              <span>Search</span>
              <div class="lds-ring"><div></div><div></div><div></div><div></div></div>
            </div> 
             <i class="fa fa-search plf-search" aria-hidden="true"  ></i>
            <i class="fa-li fa plf-load fa-spinner fa-spin" style="display:none"></i>
            </input>
          </div>
                <div class="clear"></div>
              </div>
              <?php
     if(isset($_GET['test']) && $_GET['test']) {
      echo '<input type="hidden" name="test" id="testvalues" value="1">';
     }
     ?>
              <input type="hidden" name="trip" id="trip" value="<?php echo $trip; ?>">
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="booking-title">
    <div class="row">
   <div class="col-md-12">
      <div class="trinfo pull-left">
        <h5><strong><span class="TotalFlightCount"><img style="width: 20px;" src="images/loading.gif"></span> Flights</strong> from <strong><?php echo $fromdisplay; ?></strong> to <strong><?php echo $todisplay; ?></strong></h5>
				<h6><?php echo $_GET['outdate']; ?> <?php echo (isset($_GET['indate']) && $trip == 2 ? ' - '.$_GET['indate'] : ''); ?> <span><?php echo ($_GET['adults'] + $_GET['children'] + $_GET['infants']); ?> Travellers<span class="mh"> (Adults <?php echo $_GET['adults']; ?>: Children <?php echo $_GET['children']; ?>: Infants: <?php echo $_GET['infants']; ?>)</span>, <?php echo $_GET['cabinclass']; ?></span></h6>
      </div>
      <a class="popup-text pull-left changesrc min" href="#search-dialog" data-effect="mfp-zoom-out">Change <span class="lowhid">search</span></a>
      </div>
   <div class="col-md-3">
   <div class="loading-image text-center" style="display:none">
        <p class="load-text">Loading more flights...</p>
	      <?php /*<p><img src="images/loading_bar_animated.gif" alt="loader" class="img-responsive"> */ ?>
				<p><span class="loaderwrapper"><i class="loader"></i></span></p>
  </div>
   </div>
       <div class="clearfix"></div>
    </div>
 </div>
    <div class="row ListPageResults">
      <div class="mobilecollapse visible-xs visible-sm">
        <div class="nav-drop booking-sort mobilesort">
          <h5 class="booking-sort-title"><a href="javascript:void(0);">Sort:<i class="fa fa-angle-down"></i><i class="fa fa-angle-up"></i></a></h5>
        </div>
        <a href="javascript:void(0);" class="collapsefilter"><i class="fa fa-filter" aria-hidden="true"></i></a> </div>
      <div class="col-md-3 fiterbox <?php echo ($mobileview ? 'mobileview' : ''); ?>">
        <div class="closebox visible-xs visible-sm">
          <h3>Filter Result</h3>
          <span class="mbe-filter-close1 filterclose">Close</span> </div>
          
          
          <aside class="aside">
          <h3 class="filter-by">Filter By:</h3>
          <ul class="">
            <li>
              <h5 class="">Stops <!--<small>Price from</small>--></h5>
              <div class="checkbox">
                
                  <input class="myinput large stop" value="0" type="checkbox" style="position: absolute;left: 50px;">
                  <label>Non-stop<!--<span class="pull-right">$215</span>--> </label>
              </div>
              <div class="checkbox">
                
                  <input class="myinput large stop" value="1" type="checkbox" style="position: absolute;left: 50px;">
                  <label>1 Stop<!--<span class="pull-right">$154</span>--> </label>
              </div>
              <div class="checkbox">
                
                  <input class="myinput large stop" value="2" type="checkbox" style="position: absolute;left: 50px;">
                  <label>2 Stops<!--<span class="pull-right">$197</span>--> </label>
              </div>
              <div class="checkbox">
                
                  <input class="myinput large stop" value="3" type="checkbox" style="position: absolute;left: 50px;">
                  <label>3 Stops<!--<span class="pull-right">$197</span>--> </label>
              </div>
            </li>
            
           
            </li>
            <li>
              <h5 class="">Airlines <!--<small>Price from</small>--></h5>
              <div class="FilterAirlines">
              		<img style="width: 20px;" src="images/loading.gif">
              </div>
            </li>
            <li>
             <h5 class="">Departure Time</h5>
              <div class="checkbox">
                
                  <input class="myinput large timefilter" value="M" type="checkbox" style="position: absolute;left: 50px;">
                  <label>Morning (5:00 - 11:59)</label>
              </div>
              <div class="checkbox">
                  <input class="myinput large timefilter" value="A" type="checkbox" style="position: absolute;left: 50px;">
                   <label class="">Afternoon (12:00 - 17:59)</label>
              </div>
              <div class="checkbox">
                
                  <input class="myinput large timefilter" value="E" type="checkbox" style="position: absolute;left: 50px;">
                  <label class="">Evening (18:00p - 23:59)</label>
              </div>
            </li>
          </ul>
        </aside>
       
      </div>
      <div class="col-md-9 sucrth">
        <ul class="nav-drop-menu listtitle">
          <li class="allg"><a href="javascript:void(0);" class="sortName">Airline <!--<i class="fa fa-sort-desc" aria-hidden="true"></i>--></a> </li>
          <li class="alfr" <?php if($mobileview): echo 'style="display:none;"'; endif; ?>><a href="javascript:void(0);" class="sortFrom">from <!--<i class="fa fa-sort-desc" aria-hidden="true"></i>--></a></li>
          <li class="empty"></li>
          <li class="afto" <?php if($mobileview): echo 'style="display:none;"'; endif; ?>><a href="javascript:void(0);" class="sortTo">To <!--<i class="fa fa-sort-desc" aria-hidden="true"></i>--></a></li>
          <li class="aldr"><a href="javascript:void(0);" class="sortDuration">Duration <!--<i class="fa fa-sort-desc" aria-hidden="true"></i>--></a></li>
          <!--<li class="alst"><a href="javascript:void(0);" class="sortStop">Stop <i class="fa fa-sort-desc" aria-hidden="true"></i></a></li>-->
          <li class="alst" <?php if($mobileview): echo 'style="display:none;"'; endif; ?>>&nbsp;</li>
          <li class="alpr"><a href="javascript:void(0);" class="sortPrice">Price</a></li>
        </ul>
        <ul class="booking-list">
        </ul>
        <div class="ListLoading" style="text-align:center; margin:100px 0px;">
        	<p><b>Flights are loading..</b></p>
        	<?php /*<img src="images/1.gif" class="LadGif" style="display:none;">*/ ?>
					<div class="circlespinner"></div>
        </div>
        <div class="alert alert-info NoResulstAlert" align="center" style="display:none;">
         <strong>Oops! No More Results Found.</strong>
        </div>
        <div id="page-selection" style="cursor:pointer; display:none;">Load More</div>
        <div style="margin:30px 0px;"></div>
      </div>
    </div>
    <!-- <div class="AdsenseBlock">
      <div class="ads1">
  	    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <ins class="adsbygoogle"
             style="display:inline-block;width:160px;height:600px"
             data-ad-client="ca-pub-8581489459044288"
             data-ad-slot="8623519628"></ins>
        <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
      </div>
      <div class="ads1">
        	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
      <ins class="adsbygoogle"
           style="display:inline-block;width:160px;height:600px"
           data-ad-client="ca-pub-8581489459044288"
           data-ad-slot="8623519628"></ins>
      <script>
      (adsbygoogle = window.adsbygoogle || []).push({});
      </script>
      </div>
      <div class="ads1">
        	<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
      <ins class="adsbygoogle"
           style="display:inline-block;width:160px;height:600px"
           data-ad-client="ca-pub-8581489459044288"
           data-ad-slot="8623519628"></ins>
      <script>
      (adsbygoogle = window.adsbygoogle || []).push({});
      </script>
      </div>
    </div> -->
  </div>
</div>
<input type="hidden" id="uniqueId" value="<?php echo $unique_id; ?>">
<script src="js/jquery.js"></script> 
<script type="text/javascript" src="js/jquery-ui.js"></script> 
<script src="js/bootstrap.js"></script> 
<script src="js/slimmenu.js"></script>
<script src="js/bootstrap-datepicker.js"></script> 
<script src="js/bootstrap-timepicker.js"></script> 
<script src="js/nicescroll.js"></script> 
<script src="js/typeahead.js"></script>
<script src="js/magnific.js"></script>
<script src="js/df.js"></script> 
<script src="js/custom.js"></script>

<style>
.search-form-flights, .search-form-hotels, .search-form-cars {
    min-height: 231px;
    border-bottom: 1px solid #E4EFF7;
}
.fly-padding-bottom-10 {
    padding-bottom: 10px;
}
.fly-push-bottom-10 {
    margin-bottom: 10px !important;
}
.fly-push-bottom-10 {
    margin-bottom: 10px !important;
}
.fly-clearfix, .fly-row {
    display: block;
}
.fly-lineheight-25 {
    line-height: 25px;
}
.fly-inline-block {
    display: inline-block;
}
.fly-lineheight-16 {
    line-height: 16px;
}
.fly-ellipsis {
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
}
.fly-button.green {
    color: #FFF;
    text-shadow: #4aa200 0px 1px 1px;
    border: 1px solid #86cd00;
    border-bottom: 1px solid #5b8a00;
    background: #c8e22d;
    background: -moz-linear-gradient(top, #c8e22d 0%, #b3dc01 1%, #9dcc01 31%, #92c700 64%, #80bc01 99%);
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #c8e22d), color-stop(1%, #b3dc01), color-stop(31%, #9dcc01), color-stop(64%, #92c700), color-stop(99%, #80bc01));
    background: -webkit-linear-gradient(top, #c8e22d 0%, #b3dc01 1%, #9dcc01 31%, #92c700 64%, #80bc01 99%);
    background: -o-linear-gradient(top, #c8e22d 0%, #b3dc01 1%, #9dcc01 31%, #92c700 64%, #80bc01 99%);
    background: -ms-linear-gradient(top, #c8e22d 0%, #b3dc01 1%, #9dcc01 31%, #92c700 64%, #80bc01 99%);
    background: linear-gradient(to bottom, #c8e22d 0%, #b3dc01 1%, #9dcc01 31%, #92c700 64%, #80bc01 99%);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#c8e22d', endColorstr='#80bc01', GradientType=0);
}
.fly-button.large-size {
    height: 45px;
    font-size: 18px;
    padding-top: 10px;
    border-bottom: 3px solid #5b8a00;
}
.fly-full-width {
    width: 100%;
}
.fly-full-width, .large .fly-full-width-lg, .medium .fly-full-width-md, .small .fly-full-width-sm, .mini .fly-full-width-mi {
    width: 100% !important;
}
.fly-button {
    position: relative;
    display: block;
    text-align: center;
    padding: 7px 10px;
    cursor: pointer;
    border: 1px solid #CCC;
}
.fly-rounded-6 {
    -webkit-border-radius: 6px;
    -moz-border-radius: 6px;
    border-radius: 6px;
}
.fly-group-button label {
    float: left;
    height: 22px;
    line-height: 22px;
    cursor: pointer;
    font-weight: 700;
    padding: 0 9px;
}

.fly-group-button li{
	border-right: 1px solid #BBBBBB;
}
.fly-group-button ul{
	margin-bottom: 0;
}
.fly-padding-left-40 {
    padding-left: 40px;
}
.clearable {
	width: 100%;
}
.form-horizontal label{
	padding-right: 70px;
}
.ronud-trip{
	padding-left: 85px;
}
.form-horizontal {
	padding: 20px 0;
}
.pick-date i{
	position: absolute;
    top: 10px;
    right: 13px;
    z-index: 999;
}
.mfp-dialog label {
	color: #000;
}
.fly-push-right-15 {
	color: #000 !important;
}
#air_one_way input {
	height:40px;
	width:100%;
}
#air_one_way .row {
	margin-bottom:10px;
}
.ui-autocomplete {
	overflow-y: auto;
	position:absolute;
}
.ui-helper-hidden-accessible {
    border: 0;
    clip: rect(0 0 0 0);
    height: 1px;
    margin: -1px;
    overflow: hidden;
    padding: 0;
    position: absolute;
    width: 1px;
}
</style>
<script src="js/autocomp.js?s=b1"></script>
</body>
</html>
